import React,{useState,createRef, useEffect} from 'react'
import { useLocation, useNavigate } from 'react-router-dom'
import { useScreenshot } from 'use-react-screenshot';
import { saveAs } from 'file-saver';
import arrow from "../../assets/icons/profil_arrow.svg";
import badge2 from "../../assets/icons/badge3.png";

import { BackDiv, BackImg, BackText, BacktoDashboard, BadgeIcon, BTNcontainer, Container, GetButton, ImgContainer, InnerTitle, ProgressBarHere, ResultWord, TestContainer, TimeTitle, Wrapper } from './style';
import Swal from 'sweetalert2';
import axios from 'axios';


const Results = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const ref = createRef(null)
  const {id,score,start_date,end_date,quiz,user} = location.state;
  let Auth_Data =
  JSON.parse(localStorage.getItem("access_key")) !== null
  ? JSON.parse(localStorage.getItem("access_key"))
  : { user: { full_name: "Unknown" } };
  const [image, takeScreenshot] = useScreenshot()


  useEffect(() => {
    window.scrollTo({
        top: 0,
        behavior: 'auto'
    })
  }, []);


  const getImage = () =>{
     takeScreenshot(ref.current)
     Swal.fire({
        icon: 'info',
        title: 'Natija chop etildi!',
        text: 'Natijalarni yuklab oling!',
        confirmButtonText:'Yuklab olish',
        cancelButtonText:'Bekor qilish'
     }).then((res)=>{
        Download()
     })
  }

  const NextAttempt = () => {
    navigate('/dashboard/allcourses')
  }

  const Download = () => {
    let img = document.getElementById('getimg');
    img.click()
  }

  


  return (
         <Container>
            <Wrapper>
                <BacktoDashboard>
                <BackDiv onClick={()=>navigate('/dashboard/allcourses')}>
                    <BackImg src={arrow} />
                    <BackText>Kabinetga qaytish</BackText>
                </BackDiv>
                </BacktoDashboard>
                <TestContainer >
                    <ProgressBarHere>
                        <InnerTitle>
                            Test natijalari
                        </InnerTitle>
                        <TimeTitle>{start_date} / {end_date}</TimeTitle>

                    </ProgressBarHere>
                    <ImgContainer >
                        <BadgeIcon src={badge2} />
                        <ResultWord ref={ref}>
                            Sizning natijangiz <span>{score}</span>/<span>5.00</span> ni qayd etdi
                        </ResultWord>
                    </ImgContainer>
                    <BTNcontainer>
                        <a style={{display:'none'}} id='getimg' href={image} download>Download</a>
                        <GetButton style={{ marginBottom: '10px' }} onClick={getImage}>
                            Yuklab olish
                        </GetButton>
                        <GetButton style={{ marginBottom: '10px' }} onClick={NextAttempt}>
                           Ortga qaytish
                        </GetButton>
                    </BTNcontainer>
                </TestContainer>
            </Wrapper>
        </Container>
  )
}

export default Results