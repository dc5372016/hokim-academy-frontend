import React, { Component } from 'react';
import NumberFormat from 'react-number-format';

// import Images
import DC from "../assets/icons/DC.png"
import Facebook from "../assets/icons/facebook.svg"
import Telegram from "../assets/icons/telegram.svg"
import Instagram from "../assets/icons/instagram.svg"
import Youtube from "../assets/icons/youtube.svg"
import { FooterContainer, LogoFooter } from './style';

class Footer extends Component {
  state = {}
  render() {
    return (
      <FooterContainer>
      <footer className='footer'>
        <div className="footer__left">
          <LogoFooter/>
          <p>O'qituvchilar va talabalar uchun onlayn platforma</p>
        </div>
        <div className="footer__center">
          <a href="#home">Bosh sahifa</a>
          <a href="#curse">Kurslar</a>
          <a href="#info">Loyiha haqida</a>
          <a href="#litsenzya">Litsenziya</a>
          <a href="#user">Shaxsiy malumotlarni qayta ishlash</a>
          <a href="#faq">F.A.Q</a>
        </div>
        <div className="footer__right">
          <div className="dc__logo">
            <img src={DC} alt="" />
            <div>
              <p>DIGITAL</p>
              <p>CITY</p>
            </div>
          </div>
          <form action="">
            <NumberFormat
              placeholder="+998 _ _  _ _"
              format="+998 ## ### ## ##"
              mask="_"
            />
            <button>Ro'yhatdan o'tish</button>
          </form>
          <div className="social__sets">
            <a href="https://facebook.com"><img src={Facebook} alt="" /></a>
            <a href="https://telegram.com"><img src={Telegram} alt="" /></a>
            <a href="https://instagram.com"><img src={Instagram} alt="" /></a>
            <a href="https://youtube.com"><img src={Youtube} alt="" /></a>
          </div>
        </div>
      </footer>
      </FooterContainer>
    );
  }
}

export default Footer;