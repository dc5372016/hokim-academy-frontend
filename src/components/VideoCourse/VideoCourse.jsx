import React from 'react'
import ReactPlayer from 'react-player'

// Image import
import play from "../../assets/icons/play.png";
import video from "../../assets/video/htmlcss.mp4"
import html from "../../assets/images/html.png"
import php from "../../assets/images/php.png"
import doc from "../../assets/images/doc.png"
import css from "../../assets/images/css.png"
import youtube from "../../assets/images/youtubeplay.png"
import handsfree from "../../assets/images/handsfree.png"
import video_bg from "../../assets/images/video_bg.webp"

const VideoCourse = () => {
  return (
    <div className='video__cource'>
      <h1>Kurslar katalogi Sizni kutmoqda! Kirib, tanishib chiqishingiz mumkin.</h1>
      <p>Siz uchun Akademiya bilan ta'lim olish afzalliklarini namoyish etish maqsadida video tayyorladik. Videoni ko'ring va qabulga qanday tayyorlanishga o'zingiz qaror qiling.</p>
      <div className="icon__handsfree"><img src={handsfree} alt="" /></div>
      <div className="icon__youtube"><img src={youtube} alt="" /></div>
      <div className="video__box">
        <div style={{"--i": "0",}} className="icon__html"><img src={html} alt="" /></div>
        <div style={{"--i": "2",}} className="icon__css"><img src={css} alt="" /></div>
        <div style={{"--i": "4",}} className="icon__php"><img src={php} alt="" /></div>
        <div style={{"--i": "6",}} className="icon__doc"><img src={doc} alt="" /></div>
        <div className='video'>
          <ReactPlayer
            url={video}
            light={video_bg}
            style={{objectFit:'cover',backgroundSize:'cover',borderRadius:20}}
            width="100%"
            height="400px"
            className="react__player"
            controls
            playIcon={<button className="play__btn"><img src={play} alt="" /></button>}
          />
        </div>
        
      </div>
    </div>
  )
}

export default VideoCourse