export const options = [
    {
      value: "Male",
      label: "Erkak",
    },
    {
      value: "Female",
      label: "Ayol",
    },
  ];

export const TypesSelect = [
    {
      value:1,
      label:"O'quvchi"
    },
    {
      value:2,
      label:"Talaba"
    },
    {
      value:3,
      label:"O'qituvchi"
    },
    {
      value:4,
      label:"Xodim"
    },
    {
      value:5,
      label:"Boshqa"
    },
  ]