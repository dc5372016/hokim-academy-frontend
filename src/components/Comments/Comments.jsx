import React, { Component } from 'react';
import { Navigation, Autoplay } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";

import user_image from "../../assets/images/user_image.png"
import Messages from "../../assets/images/messages.png"

// import Styles
import "swiper/css";
import "swiper/css/bundle";
import "swiper/css/navigation";


class Comments extends Component {
  state = {  } 
  render() { 
    return (
      <React.Fragment>
        <div className="comments">
          <img className='comment__iconleft' src={Messages} alt="" />
          <img className='comment__iconright' src={Messages} alt="" />
          <p className='comments__title'>
            Talabalar fikri
          </p>
          <div className="slider__container" data-aos="fade-up">
            <div className="slider">
              <Swiper
				        navigation={true}
                autoplay={{
                  delay: 2500,
                  disableOnInteraction: false,
                }}
                breakpoints={{
                  320: {
                    spaceBetween: 10,
                  },
                  375: {
                    spaceBetween: 10,
                  },
                  425: {
                    spaceBetween: 10,
                  },
                  768: {
                    spaceBetween: 10,
                  },
                  900: {
                    spaceBetween: 10,
                  },
                  1024: {
                    spaceBetween: 0,
                  },
                }}
                loop={true}
                modules={[Navigation, Autoplay]}
                className="commentSwiper"
              >
                <SwiperSlide>
                  <div className="comment">
                    <p className="slider__title">
                      Hammasi tushunarli va ortiqcha ma'lumotlarsiz berilgan  
                    </p>
                    <p className="slider__comment">
                      Onlayn kurslarni sinab ko'rish haqida uzoq o'yladim, chunki tushunarli bo'lmasligidan qo'rqardim. Lekin darslar juda lo'nda va tushunarli ekan. Birga tayyorlanayotgan sinifdoshlarimgaham sinab korishni maslahat berdim
                    </p>
                    <div className="slider__user">
                      <p>Rano Aliyeva - </p>
                      <p>Ingliz tili</p>
                      <img src={user_image} alt="" />
                    </div>
                  </div>
                </SwiperSlide>
                <SwiperSlide>
                  <div className="comment">
                    <p className="slider__title">
                      Hammasi tushunarli va ortiqcha ma'lumotlarsiz berilgan
                    </p>
                    <p className="slider__comment">
                      Onlayn kurslarni sinab ko'rish haqida uzoq o'yladim, chunki tushunarli bo'lmasligidan qo'rqardim. Lekin darslar juda lo'nda va tushunarli ekan. Birga tayyorlanayotgan sinifdoshlarimgaham sinab korishni maslahat berdim
                    </p>
                    <div className="slider__user">
                      <p>Rano Aliyeva - </p>
                      <p>Ingliz tili</p>
                      <img src={user_image} alt="" />
                    </div>
                  </div>
                </SwiperSlide>
              </Swiper>
            </div>
          </div>
        </div>
        
      </React.Fragment>
    );
  }
}
 
export default Comments;