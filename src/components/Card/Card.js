import React from 'react'
import { CardContainer, CardTitle, Container, Icon, IconContainer, Card } from './styles'
import {useNavigate} from 'react-router-dom'
import Card1Img from '../../assets/icons/card/card1.png';
import Card2Img from '../../assets/icons/card/card2.png';
import Card3Img from '../../assets/icons/card/card3.png';


const Widget = () => {
    const navigate = useNavigate();




    return (
        <Container>
            <CardContainer>
                <Card  data-aos="fade-up" data-aos-duration="2000" onClick={()=>navigate('/register')}>
                    <IconContainer>
                        <Icon src={Card1Img} />
                    </IconContainer>
                    <CardTitle>Profil yarating</CardTitle>
                </Card>
                <Card data-aos="fade-up" data-aos-duration="2000" onClick={()=>navigate('/courses')}>
                    <IconContainer>
                        <Icon src={Card2Img} />
                    </IconContainer>
                    <CardTitle>Kursni tanlang</CardTitle>
                </Card>
                <Card data-aos="fade-up" data-aos-duration="2000">
                    <IconContainer>
                        <Icon src={Card3Img} />
                    </IconContainer>
                    <CardTitle>O’qishni boshlang</CardTitle>
                </Card>
            </CardContainer>
        </Container>
    )
}

export default Widget;