import styled from "styled-components";
import BgCourse from "../../assets/images/course_bg.png";



export const LeftFilterContainer = styled.div`
    display: flex;
    flex-direction: column;
`;

export const CourseContainer = styled.div`
    display: flex;
    flex-direction: column;
    
`;

export const TopFilterContainer = styled.div`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    margin: 15px 0px;
    padding: 20px;
    grid-gap: 25px;
    width: 100%;
    max-width: 1024px;
    overflow-x: auto;
    &::-webkit-scrollbar-track{
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.1);
        background-color: #F5F5F5;
        border-radius: 10px;
    }
    &::-webkit-scrollbar
    {
        width: 4px;
        height: 4px;
        border-radius: 10px;
        background: linear-gradient(180deg, #03F5FF 0%, #02B6EB 100%);
    }
    &::-webkit-scrollbar-thumb
    {
        border-radius: 4px;
        background: linear-gradient(180deg, #03F5FF 0%, #02B6EB 100%);
    }
    
`;

export const TopFilterBtn = styled.button`
    font-family: 'Lexend';
    font-style: normal;
    font-weight: 500;
    font-size: 23px;
    line-height: 32px;
    color: #333333;
    flex: none;
    /* order: 0; */
    /* flex-grow: 0; */
    /* margin: 0px 50px; */
    border: 2px solid white;
    outline: none;
    text-align: center;
    padding: 5px;
    background-color: transparent;
    cursor: pointer;
    transition: .3s;
    &.active{
        border-bottom: 2px solid #03F5FF;
        font-weight: 600;
    }
    &:hover{
        color : #02C0EE;
    }
`;


export const CardContainer = styled.div`
    display: ${props=>props.loading ? 'grid' : 'flex'};
    margin-top: 15px;
    grid-template-columns: auto auto auto auto;
    grid-gap: 20px;
    justify-content:  ${props=>props.loading ? '' : 'center'};
    align-items:  ${props=>props.loading ? '' : 'center'};
    margin-top:  ${props=>props.loading ? '' : '200px'};
    
`;
export const SliderContainer = styled.div`
    display: none;
    position: relative;
    
`;

export const CustomRightArrowBtn = styled.button`
    background: linear-gradient(180deg, #03F5FF 0%, #02B6EB 100%);
    transform: matrix(-1, 0, 0, 1, 0, 0);
    position: absolute;
    outline: 0;
    transition: all .5s;
    border-radius: 8px;
    border: 0;
    background-color: rgba(50, 57, 61, 0.8);
    min-width: 36px;
    min-height: 36px;
    opacity: 1;
    cursor: pointer;
    display: flex;
    align-items: center;
    justify-content: center;
    color: white;
    font-size: 20px;
    right: 35%;
    top: 80px;
`;

export const CustomLeftArrowBtn = styled.button`
    background: linear-gradient(180deg, #03F5FF 0%, #02B6EB 100%);
    transform: matrix(-1, 0, 0, 1, 0, 0);
    position: absolute;
    outline: 0;
    transition: all .5s;
    border-radius: 8px;
    border: 0;
    min-width: 36px;
    min-height: 36px;
    opacity: 1;
    cursor: pointer;
    display: flex;
    align-items: center;
    justify-content: center;
    color: white;
    font-size: 20px;
    top: 80px;
    left: 35%;
`;

export const FilterSlideContainer = styled.div`
    display: none;
`;

export const AllBtnContainer = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    margin-top: 20px;
`;

export const AllBtn = styled.button`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 237px;
    max-height: 77px;
    min-height: 77px;
    background: white;
    border-radius: 15px;
    cursor: pointer;
    font-family: 'Lexend';
    font-style: normal;
    font-weight: 400;
    font-size: 28px;
    line-height: 35px;
    text-align: center;
    color: #03F5FF;
    outline: none;
    border: 1px solid #03F5FF;
    background: linear-gradient(180deg, #03F5FF 0%, #02B6EB 100%);
        color: #FFFFFF;
`;

export const MaxContainer = styled.div`
    display: flex;
    justify-content: center;
`
export const Container = styled.div`
    display: grid;
    grid-template-columns: 25% 73%;
    background-color: white;
    padding: 50px 100px;
    background-image: url(${BgCourse});
    background-position: center;
    background-size: 100%;
    background-repeat: no-repeat;
    position: relative;
    max-width: 1900px;
    @media (max-width: 1550px){
        grid-template-columns: 30% 70%;
        ${CardContainer}{
            grid-template-columns: auto auto auto;
        }
        ${TopFilterBtn}{
            font-size: 22px;
            line-height: 28px; 
        }
    }
    @media (max-width: 1300px){
        grid-template-columns: 25% 75%;
        ${TopFilterContainer}{
            max-width: 800px;
        }
    }
    @media (max-width: 1200px){
        grid-template-columns: 30% 70%;
        ${TopFilterContainer}{
            max-width: 100%;
        }
    }
    @media (max-width: 1100px){
        grid-template-columns: 25% 75%;
    }
    @media (max-width: 1025px){
        grid-template-columns: 100%;
    }
    @media (max-width: 860px){
        ${CardContainer}{
            grid-template-columns: auto auto;
        }
    }
    @media (max-width: 700px){
        ${CardContainer}{
            display: none;
        };
        padding-left: 40px;
        padding-right: 40px;
        ${SliderContainer}{
            display: block;
        };
    }
    max-width: 100%;
    margin: auto;
    box-sizing: border-box;
`;

