import styled from 'styled-components';
import SearchIcon from '@mui/icons-material/Search';
import Select from 'react-select';

export const SearchBanner = styled.div`
    width: 100%;
    height: 76px;
    border-radius: 6px;
    box-shadow: 0px 4px 10px 0px #9CA1A440;
    margin-bottom: 30px;
    margin: auto;
    background-color: #EBFEFF;
    /* background-color: red; */
    display: flex;
    align-items: center;
    justify-content: space-around;
    max-width: 1550px;
    @media (max-width:1111px){
        flex-direction: column;
        height: auto;
        padding: 20px;
        padding-top: 40px;
        padding-bottom: 40px;
        box-sizing: border-box;
    }
`
export const Space = styled.div`
    height: 50px;
    width: 100%;
    /* border: 1px solid red; */
`

export const SelectView = styled(Select)`
    height: 50px !important;
    width: 225px !important;
    border-radius: 10px;
    background-color: white;
    border: 1px solid red;
    font-size: 18px;
    padding: 10px !important;
    scroll-behavior: smooth;
    border: none !important;
    @media (max-width:1111px){
        width: 100% !important;
        margin-top: 10px;
    }
`

export const Option = styled.option`
    border: none !important;
    height: 40px !important;
    &:hover{
        background-color: red !important;
    }
`
export const ReusableTitle = styled.p`
    font-family: 'Lexend';
    font-size: 28px;
    font-weight: 500;
    line-height: 35px;
    letter-spacing: 0em;
    text-align: left;
    color: rgba(0, 0, 0, 1);
    border-bottom: 2px solid rgba(3, 245, 255, 1);
    padding-bottom: 15px;
    @media (max-width:1110px){
        width:100%;
        margin: auto;
    }
    @media (max-width:650px){
        width:100%;
        margin: auto;
    }
`

export const SearchInpContainer = styled.div`
    width: 230px;
    height: 40px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 5px;
    padding-right: 10px;
    background-color: white;
    border-radius: 10px;
    @media (max-width:1111px){
        width: 100% ;
        margin-top: 10px;
    }
`
export const SearchRasm = styled(SearchIcon)`
   
`

export const SearchInp = styled.input`
    width: 90%;
    border: none;
    outline: none;
    height: 40px;
    font-size: 18px;
    @media (max-width:1111px){
       padding-left: 20px;
    }
`

export const AllCoursesContainer = styled.div`
    width: 100%;
    height: auto;
    min-height: 350px;
    margin-top: 30px;
    display: flex;
    justify-content:space-between;
    flex-wrap: wrap;
    &::after{
        content: "";
        flex: 180px;
    }
    @media (max-width:880px){
       justify-content: space-around;
    }
`









export const LessonCount = styled.span`
    background: linear-gradient(180deg, #03F5FF 0%, #02B6EB 100%);
    border-radius: 7px;
    padding: 7px;
    display: flex;
    align-items: center;
    justify-content: center;
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 500;
    font-size: 10px;
    line-height: 10px;
    position: absolute;
    top: 10px;
    right: 10px;
    /* identical to box height, or 100% */

    text-align: center;
    letter-spacing: 0.5px;

    color: #FFFFFF;
`;

export const Image = styled.img`
    border-top-left-radius: 20px;
    border-top-right-radius: 20px;
    height: 48%;
    object-fit: cover;
`;
export const Image1 = styled.img`
    border-top-left-radius: 20px;
    border-top-right-radius: 20px;
    width: 32px;
    height: 32px;
    object-fit: cover;

`;
export const Content = styled.div`
    padding: 7px 15px;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    height: 52%;
    box-sizing: border-box;
    position: relative;
`;

export const AuthorContainer = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
`;

export const AvatarContainer = styled.div`
    width: 35px;
    height: 35px;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 3px;
    background-color: transparent;
    border-radius: 28px;
    position: relative;
    border: 1px solid white;
    position: absolute;
    top: -20px;
    right: 10px;
`;

export const Name = styled.span`
   font-family: 'Poppins';
    font-style: normal;
    font-weight: 400;
    font-size: 11px;
    line-height: 13px;
    margin-bottom: 15px;
    /* identical to box height, or 100% */

    letter-spacing: 0.5px;

    color: #B7B9D2;

    mix-blend-mode: normal; 
`;

export const Category = styled.span`
font-family: 'Poppins';
font-style: normal;
font-weight: 500;
font-size: 12px;
line-height: 20px;

letter-spacing: 0.3px;

color: #FFFFFF;
`;

export const Text1 = styled.span`
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 400;
    font-size: 9px;
    line-height: 12px;
    /* identical to box height, or 100% */
    letter-spacing: 0.5px;
    color: #808191;
    mix-blend-mode: normal;
`;

export const Button1 = styled.button`
    background: linear-gradient(180deg, #03F5FF 0%, #02B6EB 100%);
    box-shadow: 0px 4px 8px rgba(93, 95, 239, 0.2);
    border-radius: 10px;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 142px;
    height: 26px;
    border: none;
    cursor: pointer;
    position: relative;
    font-family: 'Lexend';
    font-style: normal;
    font-weight: 400;
    font-size: 10px;
    line-height: 12px;
    /* identical to box height, or 80% */

    text-align: center;
    letter-spacing: 0.5px;

    color: #FFFFFF;
    transition: .3s;
    &:hover{
    background: linear-gradient(180deg, #03F5FF 0%, #1acaff 100%);

    }
`;

export const Container1 = styled.div`
    display: flex;
    flex-direction: column;
    background: #252836;
    border-radius: 20px;
    width: 180px;
    height: 239px;
    position: relative;
    margin: 10px;
    margin-top: 10px;
    margin-left: 10px;
    font-family: 'Poppins';
`;

export const VidosWrap = styled.div`
    display: flex;
    margin-top: 70px;
    justify-content: flex-start;
    align-items: center;
    flex-wrap: wrap;
    @media (max-width:1310px){
        justify-content: flex-start;
    }
    @media (max-width:1210px){
        justify-content: flex-start;
    }
    @media (max-width:920px){
        justify-content: flex-start;

    }
    @media (max-width:666px){
        justify-content: center;

    }
`

export const LoadingWrapper = styled.div`
    width: 100%;
    height: 400px;
    display: flex;
    justify-content: center;
    align-items: center;
`

export const PaginationWrapper = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    height: 100px;
    padding-bottom: 100px;
`
export const InnerPaginationWrap = styled.div`
    width: auto;
    height: auto;
    padding: 10px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    box-shadow: 0px 4.558139324188232px 11.39534854888916px 0px #00000033;
    border-radius: 5px;

`

export const PaginationInputWrap = styled.div`
    display: flex;
    width: 150px;
    justify-content: center;
    align-items: center;
    @media (max-width:600px ){
        display: none;
    }

`

export const PaginationInput = styled.input`
    border: 2.28px solid #03F5FF;
    outline :none;
    width: 47px;
    height: 26px;
    background: none;

`

export const PaginationTextButton = styled.button`
    font-family: 'Lexend';
    font-size: 21px;
    font-weight: 300;
    line-height: 26px;
    letter-spacing: 0em;
    text-align: center;
    background: none;
    border: none;
    margin-left: 10px;
    &:hover{
        cursor: pointer;
        color: #23D5F3;
    }
`