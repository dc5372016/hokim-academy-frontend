import React, { useEffect, useState } from 'react'
import LeftFilter from './LeftFilter/LeftFilter';
import { CardContainer, Container, CourseContainer, CustomLeftArrowBtn, CustomRightArrowBtn, LeftFilterContainer, SliderContainer, TopFilterBtn, TopFilterContainer, FilterSlideContainer, AllBtnContainer, AllBtn, MaxContainer } from './styles';
import Loading from '../Info_Courses_Next/Loading/Loading'
import { Courses } from '../../constants/courses';
import Course from './Course/Course';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import { FilterBtn, SlideFilter } from './LeftFilter/styles';
import { useNavigate } from 'react-router-dom';
import 'sweetalert2/src/sweetalert2.scss'
import Swal from 'sweetalert2'
import axios from 'axios';
import { baseURL } from '../../api/baseUrl';

const responsive = {
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 3,
        slidesToSlide: 3 // optional, default to 1.
    },
    tablet: {
        breakpoint: { max: 700, min: 608 },
        items: 2,
        slidesToSlide: 2 // optional, default to 1.
    },
    mobile: {
        breakpoint: { max: 608, min: 0 },
        items: 1,
        slidesToSlide: 1 // optional, default to 1.
    }
};
const Filterresponsive = {
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 3,
        slidesToSlide: 3 // optional, default to 1.
    },
    tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 2,
        slidesToSlide: 2 // optional, default to 1.
    },
    mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 1,
        slidesToSlide: 1 // optional, default to 1.
    }
};
const CustomRightArrow = ({ onClick, ...rest }) => {
    const {
        onMove,
        carouselState: { currentSlide, deviceType }
    } = rest;
    // onMove means if dragging or swiping in progress.
    return <CustomRightArrowBtn onClick={() => onClick()} > {"<"} </CustomRightArrowBtn>;
};

const CustomLeftArrow = ({ onClick, ...rest }) => {
    const {
        onMove,
        carouselState: { currentSlide, deviceType }
    } = rest;
    // onMove means if dragging or swiping in progress.
    return <CustomLeftArrowBtn onClick={() => onClick()} > {">"}</CustomLeftArrowBtn>;
};




const HomeCourse = () => {
    const navigate = useNavigate();
    const [category, setcategory] = useState('Aniq Fanlar');
    const [data, setData] = useState([]);
    const [data1, setData1] = useState([]);
    const [loading, setloading] = useState(false);
    let token = localStorage.getItem('access_key');

   
    const ToCourses = () => {
        if(token===null){
            Swal.fire({
                title: 'Eslatma !',
                icon: 'info',
                text: 'Videolarni ko`rish uchun iltimos ro`yhatdan o`ting',
                imageWidth: 400,
                imageHeight: 200,
                imageAlt: 'Custom image',
                confirmButtonColor: '#3085d6',
                confirmButtonText:'Ro`yhatdan o`tish',
                showCloseButton: true,
                footer: '<a className="alertlogin" href="/login">Yoki Login orqali kiring</a>'
            })
            .then((result) => {
                /* Read more about handling dismissals below */
                if (result.isConfirmed) {
                  navigate('/register')
                }
              })
        }else{
            navigate('/courses');
        }
        
    }

    const Main_Filter = (key_word) => {
        setcategory(key_word)
    }
    const Sub_Filtering = (thisname) => {
        let filtered = data1?.filter((item)=>{
            return item.name ===thisname
        })

        setData(filtered)
    }

    useEffect(() => {
        async function fetchData() {
            try {
                const res = await axios.get(`${baseURL}/api/courses-with-category/`)
                if (res) {
                    let filtered_Course = res.data.data.filter((item) => {
                        if (item.category.name === category) {
                            return item
                        }
                    })
                    console.log("homecourse", filtered_Course[0].courses);
                    setData(filtered_Course[0].courses)
                    setData1(filtered_Course[0].courses)
                    setloading(true)
                } else {
                    console.log('error');
                }
            } catch (error) {
                console.log(error.message);
            }
        }
        fetchData()
    }, [category, setcategory]);


    return (
        <>
            <Container>
                <LeftFilterContainer>
                    <LeftFilter category={category} Main_Filter={Main_Filter} />
                </LeftFilterContainer>
                <CourseContainer>
                    <TopFilterContainer>
                        {data1?.map((item,index)=>{
                            return(
                                <TopFilterBtn key={index} onClick={()=>Sub_Filtering(item.name)} >
                                    {item.name}
                                </TopFilterBtn>
                            )
                        })}
                    </TopFilterContainer>
                    <CardContainer loading={loading}>
                        {loading ?  data?.map((item, index) => (
                            <Course item={item} key={index} />
                        )) : <Loading />}
                    </CardContainer>
                    <SliderContainer>
                        {
                            loading ? <Carousel
                            responsive={responsive}
                            keyBoardControl={true}
                            customTransition="all .5"
                            transitionDuration={500}
                            renderArrowsWhenDisabled={true}
                            containerClass="carousel-container"
                            itemClass="carousel-item-padding-40-px"
                        >
                            {data?.map((item, index) => (
                                <Course item={item} key={index} />
                            ))}
                        </Carousel> : <Loading />
                        }
                        
                    </SliderContainer>
                    {loading ? <AllBtnContainer>
                        <AllBtn onClick={ToCourses} data-aos="zoom-in" data-aos-duration="2000" >Barchasi</AllBtn>
                    </AllBtnContainer> : ''}
                </CourseContainer>
            </Container>
        </>
    )
}

export default HomeCourse;