import styled from 'styled-components';
import TextField from '@mui/material/TextField';

import NumberFormat from 'react-number-format';
export const InputNumber = styled(NumberFormat)`
    height: 60.274658203125px;
    width: 360.6963195800781px;
    border-radius: 128.8201141357422px;
    border: 1.6px solid rgba(3, 215, 246, 1);
    outline:none;
    background: none;
    text-align: center;
    &::-webkit-input-placeholder {
        color: rgba(201, 201, 201, 1);
    }
    &:-ms-input-placeholder {
    color: rgba(201, 201, 201, 1);
    }
    &::placeholder {
        color: rgba(201, 201, 201, 1);
    }
    @media (max-width:785px){
       width: 90%;
       padding-left: 15px;
       padding-right: 15px;
       box-sizing: border-box;
    }
    @media (max-width:400px){
       width: 300px;
       height: 50px;
    }
`
export const Container = styled.div`
    width: 100%;
    box-sizing: border-box;
    display: flex;
    justify-content: center;
    align-items: flex-start;
    background-image: url(${props=>props.bg});
    background-repeat: no-repeat;
    background-size: contain;
    background-position: center center;
    
`
export const Wrapper = styled.div`
    width: 100%;
    box-sizing: border-box;
    max-width: 1400px;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: flex-start;
`

export const RegisterContainer = styled.div`
    max-width: 800px;
    width: 100%;
    margin-top: 170px;
    display: flex;
    justify-content:space-around;
    align-items: center;
    flex-direction: column;
    @media (max-width:500px){
        margin-top: 100px;
    }
`

export const Box = styled.div`
    @media (max-width:785px){
       width: 100%;
       display: flex;
       justify-content: center;
       align-items: center;
       flex-direction: column;
    }
`
export const Label = styled.div`
    font-family: 'Lexend';
    font-size: 18px;
    font-weight: 400;
    line-height: 23px;
    letter-spacing: 0em;
    text-align: left;
    margin: 15px 10px;
    @media (max-width:785px){
       width: 88%;
    }
    @media (max-width:400px){
        font-size: 16px;
        width: 300px;
        font-weight: 400;
        line-height: 18px;
        letter-spacing: 0em;
        text-align: left;
        margin: 10px 5px;
    }
`
export const Input = styled.input`
    height: 60.274658203125px;
    width: 360.6963195800781px;
    border-radius: 128.8201141357422px;
    border: 1.6px solid rgba(3, 215, 246, 1);
    outline:none;
    background: none;
    text-align: center;
    &::-webkit-input-placeholder {
        color: rgba(201, 201, 201, 1);
    }
    &:-ms-input-placeholder {
    color: rgba(201, 201, 201, 1);
    }
    &::placeholder {
        color: rgba(201, 201, 201, 1);
    }
    @media (max-width:785px){
       width: 90%;
       padding-left: 15px;
       padding-right: 15px;
       box-sizing: border-box;
    }
    @media (max-width:400px){
       width: 300px;
       height: 50px;
    }
`
export const Button = styled.button`
    height: 68.274658203125px;
    width: 360.6963195800781px;
    border-radius: 128.8201141357422px;
    background: linear-gradient(180deg, 
        #03F5FF 0%, #02B6EB 100%);
    font-family: 'Lexend';
    font-size: 22px;
    font-weight: 500;
    line-height: 28px;
    letter-spacing: 0em;
    text-align: center;
    color: white;
    border: none;
    display: flex;
    justify-content: center;
    align-items: center;
    &:hover{
        background: linear-gradient(180deg, 
        #03F5FF 0%, #3a7efc 100%);
        cursor: pointer;
    }
    @media (max-width:785px){
       width: 90%;
       padding-left: 15px;
       padding-right: 15px;
       box-sizing: border-box;
    }
    @media (max-width:400px){
       width: 300px;
       height: 50px;
       font-size: 16px;
    }

`
export const Title = styled.div`
    width: 100%;
    height: 70px;
    font-family: 'Lexend';
    font-size: 30px;
    font-weight: 400;
    line-height: 45px;
    letter-spacing: 0em;
    text-align: center;
    display: flex;
    justify-content: center;
    align-items: center;
    @media (max-width:500px){
        font-size: 23px;
    }
`