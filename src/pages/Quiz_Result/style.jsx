import styled from "styled-components"

export const Container = styled.div`
    width: 100%;
    height: auto;
    display: flex;
    justify-content: center;
    box-sizing: border-box;
`
export const Wrapper = styled.div`
    width: 97%;
    max-width: 1150px;
`

export const BacktoDashboard = styled.div`
    width: 100%;
    height: 120px;
    display: flex;
    align-items: center;
    margin-top: 200px;
    @media (max-width:600px){
       margin-top: 100px;
    }
`
export const BackDiv = styled.div`
    display: flex;
    width: auto;
    align-items: center;
    transition: 0.3s;
    &:hover{
        cursor: pointer;
    }
`
export const BackImg = styled.img`
    
`
export const BackText = styled.div`
    color: rgba(2, 182, 235, 1);
    font-family: 'Lexend';
    font-size: 22px;
    font-weight: 400;
    line-height: 28px;
    letter-spacing: 0px;
    text-align: left;
    margin-left: 25px;

`

export const TestContainer = styled.div`
    width: 100%;
    height: auto;
    min-height: 600px;
    box-sizing: border-box;
    background-color: white;
    box-shadow: 0px 4px 20px 0px rgba(0, 0, 0, 0.25);
    @media (max-width:700px){
        padding: 10px;
        box-sizing: border-box;
        padding-left: 15px;
    }
`
export const TestContainer2 = styled.div`
    width: 100%;
    height: auto;
    padding: 50px;
    padding-top: 20px;
    box-sizing: border-box;
    box-shadow: 0px 4px 20px 0px rgba(0, 0, 0, 0.25);
    @media (max-width:700px){
        padding: 10px;
        box-sizing: border-box;
        padding-left: 15px;
    }
`

export const ProgressBarHere = styled.div`
    width: 100%;
    height: 80px;
    padding-top: 30px;
    padding-left: 50px;
    padding-right: 50px;
    box-sizing: border-box;
    display: flex ;
    justify-content: space-between;
    align-items: center;
`
export const ImgContainer = styled.div`
    width: 100%;
    padding-left: 50px;
    padding-right: 50px;
    height: 300px;
    margin-top: 30px;
    display: flex;
    justify-content: space-around;
    align-items: center;
    flex-direction: column;
    box-sizing: border-box;
    @media (max-width:700px){
        
        width: 100%;
        box-sizing: border-box;
        padding: 0px;
    }
`
export const BadgeIcon = styled.img`
    height: 100%;
`
export const ResultWord = styled.h1`
    font-family: 'Lexend';
    font-size: 25px;
    font-weight: 500;
    line-height: 40px;
    letter-spacing: 0px;
    margin-top: 20px;
    color: #5f5f5f;
    span{
        color: black;
        font-weight:600;
    }
`

export const InnerTitle = styled.div`
    font-family: 'Lexend';
    font-size: 32px;
    font-weight: 500;
    line-height: 40px;
    letter-spacing: 0px;
    text-align: left;
    

`

export const TimeTitle = styled.div`
    font-family: 'Lexend';
    font-style: italic;
    color: #454545;
`

export const BTNcontainer = styled.div`
    width: 100%;
    height: 50px;
    display: flex;
    justify-content: center;
    align-items: center;
    margin-top: 77px;
`

export const GetButton = styled.button`
    background: linear-gradient(180deg, #03F5FF 0%, #3479f8 100%);
    box-shadow: 0px 5.937713623046875px 11.87542724609375px 0px #5D5FEF33;
    height: 56px;
    width: auto;
    border-radius: 5.937713623046875px;
    border: none;
    color: white;
    padding: 0px 20px;
    font-family: Lexend;
    font-size: 19px;
    font-weight: 400;
    line-height: 24px;
    letter-spacing: 0em;
    text-align: center;
    transition: .3s;
    &:hover{
        cursor: pointer;
        background: linear-gradient(180deg, #03F5FF 0%, #486fff 100%);
    }
    margin: 20px;


`