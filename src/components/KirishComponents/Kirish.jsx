import React, { useEffect, useState } from 'react';
import { styled } from '@mui/material/styles';
import { Box, Button, Container, Input, InputNumber, Label, RegisterContainer, Title, Wrapper } from './style';
import Bg from '../../assets/images/loginbg.svg';
import TextField from '@mui/material/TextField';
import axios from 'axios';
import CircularIndeterminate from '../RegisterComponent/Circular/Progress';
import CustomizedSnackbars from './Snackbar';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import { baseURL } from '../../api/baseUrl';
const CssTextField = styled(TextField)({
  '& label.Mui-focused': {
    color: '#03D7F6',
  },
  '& .MuiInput-underline:after': {
    borderBottomColor: '#03D7F6',
  },
  '& .MuiOutlinedInput-root': {
    '& fieldset': {
      borderColor: '#03D7F6',
    },
    '&:hover fieldset': {
      borderColor: '#03D7F6',
    },
    '&.Mui-focused fieldset': {
      borderColor: '#03D7F6',
    },
  },
});

const Kirish = () => {
  const [phone, setphone] = useState('');
  const [password, setpassword] = useState('');
  const [btnword, setbtnword] = useState('Kirish')
  const [open, setOpen] = useState(false);
  const [alertword, setalertword] = useState('Error');
  const navigate = useNavigate()
  useEffect(() => {
    window.scrollTo({
      top:0,
      behavior: 'auto'
    })
  }, [])


  const Login = () => {
      setbtnword('Loading')
      console.log('====================================');
      console.log({phone:phone,password:password});
      console.log('====================================');
      if(password.length < 8){
        Swal.fire({
          icon: 'warning',
          title: 'Eslatma!',
          text: 'Parol 8ta belgidan ko`p bo`lishi kerak!',
        }).then((res)=>{
          setbtnword('Kirish')
        })
      }else{
        axios.get(`${baseURL}/api/login/?phone=${phone}&password=${password}`)
        .then((res)=>{
          console.log(res);
          if(res){
            setbtnword('Kirish')
          }
          if(res.data.success){
            setphone('')
            setpassword('')
            localStorage.setItem('access_key',JSON.stringify(res.data.data))
            
            window.location.href = '/'
          }
          if(res.data.success === false){
            setOpen(true)
            setalertword(res.data.error)
          }
        })
        .catch((err)=>{
          console.log("erwrerwerwerwerwerwerwer",err);
          setalertword(err.message)
          setOpen(true)
        })
      }
  }

  return (
    <Container bg={Bg}>
      <Wrapper>
        <RegisterContainer>
           <CustomizedSnackbars open={open} setOpen={setOpen} alertword={alertword}  />
          <Title>Login</Title>
            <Box>
                <Label>Telefon Nomer</Label>
                <InputNumber
                  placeholder="+998 _ _  _ _"
                  format="998#########"
                  mask="_"
                  
                  variant="outlined" 
                  value={phone}
                  onChange={(e)=>{
                    setphone(e.target.value)
                    setbtnword('Kirish')
                  }}
                  />
            </Box>
            <Box>
                <Label>Parol</Label>
                <Input
                 id="outlined-basic" 
                 className='mui__textfield'
                  placeholder="8 xonadan dan iborat bo’lish kerak" 
                  variant="outlined"
                  value={password}
                  onChange={(e)=>{
                    setpassword(e.target.value)
                    setbtnword('Kirish')
                  }}
                   />
            </Box>
            <Box>
                 <Label>&nbsp;</Label>
                <Button onClick={Login}>
                  {
                    btnword === 'Kirish' ? btnword : <CircularIndeterminate />
                  }
                </Button>
            </Box>
        </RegisterContainer>
      </Wrapper>
    </Container>
  )
}

export default Kirish