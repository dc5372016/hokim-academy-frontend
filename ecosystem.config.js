module.exports = {
    apps: [
      {
        name: 'react-app',
        script: 'serve',
        args: '-s build -l 3500', // Change 3000 to your desired port
        env: {
          "PORT": 3500 // Ensure this matches your desired port
        }
      }
    ]
  };
  