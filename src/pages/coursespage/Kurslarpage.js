import { IconButton, MenuItem } from "@mui/material";
import React, { useState } from "react";
import AniqFanlar from "../../components/CourseComponents/AniqFanlar/AniqFanlar";
import ChetTili from "../../components/CourseComponents/ChetTili/ChetTili";
import Dasturlash from "../../components/CourseComponents/Dasturlash/Dasturlash";
import MainBg from "../../components/CourseComponents/MainBg/MainBg";
import {
  SearchBanner,
  SearchInp,
  SearchInpContainer,
  SearchRasm,
  Option,
  SelectView,
  Space,
} from "./style";

const Kurslar = () => {
  const [selectedOption, setSelectedOption] = useState(null);

  const options = [
    { value: "Dasturlash", label: "Dasturlash" },
    { value: "Frontend", label: "Frontend" },
    { value: "Matematika", label: "Matematika" },
  ];

  return (
    <>
      <MainBg />
      <SearchBanner>
        <SelectView
          placeholder="Soha"
          className="Myinp"
          defaultValue={selectedOption}
          onChange={setSelectedOption}
          options={options}
        />
        <SelectView
          placeholder="Yo’nalish"
          className="Myinp"
          defaultValue={selectedOption}
          onChange={setSelectedOption}
          options={options}
        />
        <SelectView
          placeholder="Yoshingiz"
          className="Myinp"
          defaultValue={selectedOption}
          onChange={setSelectedOption}
          options={options}
        />
        <SelectView
          placeholder="Fanlar"
          className="Myinp"
          defaultValue={selectedOption}
          onChange={setSelectedOption}
          options={options}
        />

        <SearchInpContainer>
          <SearchInp placeholder="Qidiruv" />
          <IconButton>
            <SearchRasm />
          </IconButton>
        </SearchInpContainer>
      </SearchBanner>
      <Space></Space>
      <AniqFanlar />
      <Dasturlash />
      <ChetTili />
    </>
  );
};

export default Kurslar;
