import React, { Component } from 'react';

import Learning1 from "../../assets/images/learning1.png"
import Learning2 from "../../assets/images/learning2.png"

import { Container } from './style';

class OnlineLearning extends Component {
  state = {  } 
  render() { 
    return (
      <React.Fragment>
        <Container>
        <div className="online__learning" data-aos="fade-up">
          <div className='learn1'><img src={Learning1} alt="" /></div>
          <div className='online__form'>
            <p className='online__title'>Onlayn Ta'lim Oling</p>
            <p className='online__description'>Platformamiz orqali istalgan yerda va istalgan vaqtda doimiy ravishda bilim oling!</p>
            <div className="">
              <input type="number" placeholder='Telefon raqam' />
              <input type="text" placeholder='Ismingiz' />
              <a href="#online">Ro'yhatdan o'tish</a>
            </div>
          </div>
          <div className='learn2'><img src={Learning2} alt="" /></div>
        </div>
        </Container>
      </React.Fragment>
    );
  }
}
 
export default OnlineLearning;