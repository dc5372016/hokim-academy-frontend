import Atropos from 'atropos/react';
import "atropos/atropos.css";
import { CircularProgress, IconButton } from "@mui/material";
import {
  AllCoursesContainer,
  InnerPaginationWrap,
  LoadingWrapper,
  PaginationInput,
  PaginationInputWrap,
  PaginationTextButton,
  PaginationWrapper,
  ReusableTitle,
  SearchBanner,
  SearchInp,
  SearchInpContainer,
  SearchRasm,
  SelectView,
  Space,
  AuthorContainer,
  AvatarContainer,
  Button1,
  Category,
  Container,
  Container1,
  Content,
  Image,
  Image1,
  LessonCount,
  Name,
  Text1,
  VidosWrap,
  Wrapper,
} from "./style";


import React, { useEffect, useState } from "react";
import { Courses } from "../../constants/courses";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import axios from "axios";
import CircularIndeterminate from "../RegisterComponent/Circular/Progress";
import Pagination from "@mui/material/Pagination";
import Stack from "@mui/material/Stack";
import Skeleton1 from './skeleton';
import { baseURL } from '../../api/baseUrl'


const AllCourses = () => {
  const [selectedOption, setSelectedOption] = useState(null);
  const location = useLocation();
  const [data, setdata] = useState([]);
  const [loading, setloading] = useState(true);
  const navigate = useNavigate();
  const [rasm, setrasm] = useState("");
  const [sub_category, setsub_category] = useState('');
  const [main_category, setmain_category] = useState('');


  const [subcatData, setsubcatData] = useState([{ value:1 , label: "Matematika" }]);
  const [catData, setcatData] = useState([{value:1,label:'Aniq Fanlar'}])
  const [ageoption, setageoption] = useState([{value:10,label:'7 va 15 orasi'},{value:15,label:'15 va 20 orasi'},{value:20,label:'20 va 30 orasi'},{value:30,label:'30 va yuqori'}]);
  const [page, setpage] = useState(1);
  const [pages_count, setpages_count] = useState(3);
  const [Govalue, setGovalue] = useState('');
  let Auth_Data =
  JSON.parse(localStorage.getItem("access_key")) !== null
    ? JSON.parse(localStorage.getItem("access_key"))
    : { user: { full_name: "Unknown" } };



  const Ishla = (item) => {
    navigate(`/course/${item.id}`, {
      state: { ...item,  },
    });
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };
  const options = [
    { value: "Dasturlash", label: "New" },
    { value: "Frontend", label: "Top" },
    { value: "Matematika", label: "Old" },
  ];

  useEffect(() => {
    async function GetAllCoursesPagination() {
      try {
        const res = await axios.get(`${baseURL}/api/courses/`,
        {
          params:{page:page},
          headers:{
            'Authorization' : `token ${Auth_Data.token}`
          }
        }
        );
        if (res) {
          setloading(false);
          console.log("res courses pagination ",res);
          setpages_count(res.data.data.pagination.pages_count)
          setdata(res.data.data.result);
        }
      } catch (err) {
        console.log(err.message);
      }
    }
    GetAllCoursesPagination();
  }, [setpage,page]);




  useEffect(() => {
  
    async function GetAllcategories() {
      try {
        const res = await axios.get(`${baseURL}/api/category/`);
        if (res) {
          let catarr = []
          res.data.data.forEach((item)=>{
            catarr.push({value:item.id,label:item.name})
          })
          setcatData(catarr);
        }
      } catch (err) {
        console.log(err.message);
      }
    }
    GetAllcategories();
    async function GetAllSubcategories() {
      try {
        const res = await axios.get(`${baseURL}/api/sub-category/`);
        if (res) {
          let subarr = []
          let filtered = res.data.data.filter((item)=>{
            return item.category===main_category.value
          })  
          console.log("filter",filtered)
          filtered.forEach((item)=>{
            subarr.push({value:item.id,label:item.name})
          })
          setsubcatData(subarr);
        }
      } catch (err) {
        console.log(err.message);
      }
    }
    GetAllSubcategories();
    
  }, [sub_category,main_category,setsub_category,setmain_category]);


  const GetSearchCourses = async (searchword) => {
    try {
      const res = await axios.get(
        `${baseURL}/api/courses/?search=${searchword}&category=${main_category.value}&suv_cat=${sub_category.value}`,
        {headers:{'Authorization':`token ${Auth_Data.token}`}}
      );
      if (res) {
       console.log("searched data",res);
       setdata(res.data.data.result)
      }
    } catch (err) {
      console.log(err.message);
      alert('error while data is recieving')
    }
  }

  const HandleChange = (e) => {
    console.log('e',e.target.textContent);
    setpage(e.target.textContent)
  }

  const GoButton = () => {
    setpage(Govalue)
    setGovalue('')
  }
  


  return (
    <div>
      <SearchBanner>
        <SelectView
          placeholder="Soha"
          className="Myinp"
          defaultValue={main_category}
          onChange={setmain_category}
          options={catData}
        />
        <SelectView
          placeholder="Yo’nalish"
          className="Myinp"
          defaultValue={sub_category}
          onChange={setsub_category}
          options={subcatData}
        />
        <SelectView
          placeholder="Yoshingiz"
          className="Myinp"
          defaultValue={selectedOption}
          onChange={setSelectedOption}
          options={ageoption}
        />
        <SelectView
          placeholder="Saralash"
          className="Myinp"
          defaultValue={selectedOption}
          onChange={setSelectedOption}
          options={options}
        />

        <SearchInpContainer>
          <SearchInp onChange={(e)=>GetSearchCourses(e.target.value)} placeholder="Qidiruv" />
          <IconButton>
            <SearchRasm />
          </IconButton>
        </SearchInpContainer>
      </SearchBanner>
      <Space></Space>
      <ReusableTitle>Barcha Kurslar</ReusableTitle>

      <AllCoursesContainer>
        {loading === false ? (
          data?.map((item, index) => {
            return (
              <Container1
                key={index}
              >
                <Image  src={`${baseURL}${item.image}`} />
                <LessonCount>{item.lesson_count}ta dars</LessonCount>
                <Content>
                  <AuthorContainer>
                    <Name>{item.author.name}</Name>
                    <AvatarContainer>
                      <Image1
                        src={baseURL+item.author.image}
                        style={{ borderRadius: "25px" }}
                      />
                    </AvatarContainer>
                  </AuthorContainer>
                  <Category>{item.name}</Category>
                  <Text1>
                    {item.views} ko'rildi - {item.day} kun oldin
                  </Text1>
                  <Button1 onClick={() => Ishla(item)}>Boshlash</Button1>
                </Content>
              </Container1>
            );
          })
        ) : 
           Array.apply(null, Array(12)).map((item)=>(
            <Skeleton1 />
           ))
        }
      </AllCoursesContainer>
      {
        loading ? '' : 
      <PaginationWrapper>
        <InnerPaginationWrap>
          <Stack spacing={2}>
            <Pagination count={pages_count} color="secondary" onChange={(e)=>HandleChange(e)}    />
          </Stack>
          <PaginationInputWrap>
            <PaginationInput value={Govalue} onChange={(e)=>setGovalue(e.target.value)} />
            <PaginationTextButton onClick={GoButton}>Go</PaginationTextButton>
          </PaginationInputWrap>
        </InnerPaginationWrap>
      </PaginationWrapper>
      }
    </div>
  );
};

export default AllCourses;
