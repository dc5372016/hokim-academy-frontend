import styled from 'styled-components';
import Heart from "react-animated-heart";

export const Container = styled.div`
    width: 100%;
    /* border: 1px solid red; */
    display: flex;
    justify-content: center;
    margin-top: 80px;
    align-items: center;
`
export const Wrapper = styled.div`
    width: 100%;
    max-width: 1550px;
    padding: 15px;
    box-sizing: border-box;
    /* border: 1px solid red; */
`
export const MyHeart = styled(Heart)`
    position: absolute;
    right: 0px;
    width: 50px !important;
    height: 50px !important;
`
export const HeartWrapper = styled.div`
    width: 100px;
    height: 40px;
    display: flex;
    justify-content: center;
    align-items: center;
`
export const ReusableTitle = styled.p`
    font-family: 'Lexend';
    font-size: 28px;
    font-weight: 500;
    line-height: 35px;
    letter-spacing: 0em;
    text-align: left;
    color: rgba(0, 0, 0, 1);
    border-bottom: 2px solid rgba(3, 245, 255, 1);
    padding-bottom: 15px;
    @media (max-width:1110px){
        width:80%;
        margin: auto;
    }
    @media (max-width:650px){
        width:95%;
        margin: auto;
    }
`

export const BigContainer = styled.div`
    width: 100%;
    height: 424px;
    display: flex;
    justify-content: flex-start;
    align-items: center;
    margin-top: 50px;
    @media (max-width:1110px){
        height: auto;
        flex-direction: column;
    }
    
`
export const VideoContainer = styled.div`
    height: 424px;
    width: 649px;
    width: 40%;
    border-radius: 20px;
    background: ${props=>props.show ? 'black' : `url(${props.bg})`};
    background-repeat: no-repeat;
    background-position: center center;
    background-size: cover;
    display: flex;
    justify-content: center;
    align-items: center;
    @media (max-width:1450px){
        width:50%;
    }
    @media (max-width:1110px){
        width:80%;
        background-size: cover;
    }
    @media (max-width:650px){
        width:95%;
        height: 300px;
    }
    @media (max-width:500px){
        width:99%;
        height: 250px;
    }
`
export const TextContainer = styled.div`
    display: flex;
    align-items: flex-start;
    justify-content: space-between;
    flex-direction: column;
    padding-left:40px;
    box-sizing: border-box;
    width: 40%;
    height: 100%;
    @media (max-width:1450px){
        width:50%;
    }
    @media (max-width:1110px){
        width:80%;
        padding-left: 0px;
    }
    @media (max-width:650px){
        width:95%;
    }
`
export const Yozuvlar = styled.div`
    
`

export const Title = styled.div`
    font-family: Poppins;
    font-size: 36px;
    font-weight: 500;
    line-height: 54px;
    letter-spacing: 0.30000001192092896px;
    text-align: left;
    color: rgba(51, 51, 51, 1);
    margin-top: 20px;
    @media (max-width:650px){
        font-size: 30px;
        margin-top: 10px;
    }
    @media (max-width:500px){
        margin-top: 5px;
        font-size: 25px;
    }

`
export const Text = styled.div`
    font-family: 'Lexend';
    font-size: 15px;
    font-weight: 300;
    line-height: 19px;
    letter-spacing: 0px;
    text-align: left;
    margin-top: 20px;
    @media (max-width:500px){
        font-size: 13px;
        margin-top: 5px;
    }

`
export const ButtonWrapper = styled.div`
    display: flex;
    position: relative;
    align-items: center;
    @media (max-width:1110px){
        margin-top: 20px;
    }

`
export const Button = styled.button`
    border: none;
    background: linear-gradient(180deg, #03F5FF 0%, #02B6EB 100%);
    height: 38px;
    width: 205px;
    border-radius: 10px;
    box-shadow: 0px 4px 8px 0px rgba(93, 95, 239, 0.2);
    font-family: 'Lexend';
    font-size: 15px;
    font-weight: 400;
    line-height: 12px;
    letter-spacing: 0.5px;
    text-align: center;
    color: white;
    transition: .3s;
    &:hover{
        box-shadow: 0px 4px 8px 0px rgba(93, 95, 239, 0.5);
        cursor: pointer;
    }
`
export const Img = styled.img`
    margin-left: 40px;
`
export const Img1 = styled.img`
    margin-left: 10px;
    @media (max-width:1110px){
        width: 45px;
        height: 60px;
    }
    @media (max-width:650px){
        width: 31px;
        height: 40px;
    }
`

export const PlayBtn = styled.div`
    height: 145px;
    width: 145px;
    border-radius: 100%;
    background: rgba(255, 255, 255, 0.2);
    display: ${props=>props.show ? 'none' : 'flex'};
    justify-content: center;
    align-items: center;
    transition: .3s;
    &:hover{
        transform: scale(1.1);
        cursor: pointer;
    }
    @media (max-width:1110px){
        width: 110px;
        height: 110px;
    }
    @media (max-width:650px){
        width: 80px;
        height: 80px;
    }
`



export const LessonCount = styled.span`
    background: linear-gradient(180deg, #03F5FF 0%, #02B6EB 100%);
    border-radius: 7px;
    padding: 7px;
    display: flex;
    align-items: center;
    justify-content: center;
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 500;
    font-size: 10px;
    line-height: 10px;
    position: absolute;
    top: 10px;
    right: 10px;
    /* identical to box height, or 100% */

    text-align: center;
    letter-spacing: 0.5px;

    color: #FFFFFF;
`;

export const Image = styled.img`
    border-top-left-radius: 20px;
    border-top-right-radius: 20px;
    width: 100%;
    height: 48%;
    object-fit: cover;
`;
export const Image1 = styled.img`
    width: 46px;
    height:46px;
    border-radius: 100%;
    object-fit: cover;
`;

export const Content = styled.div`
    padding: 15px 27px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    height: 52%;
    position: relative;
    box-sizing: border-box;
`;

export const AuthorContainer = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
`;

export const AvatarContainer = styled.div`
    width: 50px;
    height: 50px;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 3px;
    background-color: transparent;
    border-radius: 28px;
    position: relative;
    border: 1px solid white;
    position: absolute;
    top: -20px;
    right: 20px;
`;

export const Name = styled.span`
   font-family: 'Poppins';
    font-style: normal;
    font-weight: 400;
    font-size: 13px;
    line-height: 13px;
    margin-bottom: 15px;
    /* identical to box height, or 100% */

    letter-spacing: 0.5px;

    color: #B7B9D2;

    mix-blend-mode: normal; 
`;

export const Category = styled.span`
font-family: 'Poppins';
font-style: normal;
font-weight: 500;
font-size: 16px;
line-height: 26px;

letter-spacing: 0.3px;

color: #FFFFFF;
`;

export const Text1 = styled.span`
font-family: 'Poppins';
font-style: normal;
font-weight: 400;
font-size: 12px;
line-height: 12px;
/* identical to box height, or 100% */

letter-spacing: 0.5px;

color: #808191;

mix-blend-mode: normal;
`;

export const Button1 = styled.button`
    background: linear-gradient(180deg, #03F5FF 0%, #02B6EB 100%);
    box-shadow: 0px 4px 8px rgba(93, 95, 239, 0.2);
    border-radius: 10px;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 205px;
    height: 38px;
    border: none;
    cursor: pointer;
    position: relative;
    font-family: 'Lexend';
    font-style: normal;
    font-weight: 400;
    font-size: 15px;
    line-height: 12px;
    /* identical to box height, or 80% */

    text-align: center;
    letter-spacing: 0.5px;

    color: #FFFFFF;
`;

export const Container1 = styled.div`
    display: flex;
    flex-direction: column;
    background: #252836;
    border-radius: 20px;
    width: 259px;
    height: 343px;
    position: relative;
    margin: 15px;
    margin-top: 20px;
    font-family: 'Poppins';
   
    /* @media (max-width: 1200px){
        height: 300px;
        width: 220px;
        ${Button}{
            width: 160px;
            height: 30px; 
        }
    }
    @media (max-width: 860px){
        height: 343px;
        width: 260px;
        margin: auto;
        ${Button}{
            width: 205px;
            height: 38px;
        }
    } */
`;

export const VidosWrap = styled.div`
    display: flex;
    margin-top: 70px;
    justify-content: flex-start;
    align-items: center;
    flex-wrap: wrap;
    @media (max-width:1210px){
        justify-content: flex-start;
    }
    @media (max-width:920px){
        justify-content: center;
    }
`


export const LoadMoreView = styled.div`
    width: 100%;
    height: 150px;
    display: flex;
    justify-content: center;
    align-items: center;
`

export const MoreButton = styled.button`
    height: 64px;
    width: 230px;
    border-radius: 8px;
    padding: 10px, 42px, 10px, 42px;
    background: linear-gradient(180deg, #03F5FF 0%, #02B6EB 100%);
    box-shadow: 0px 8.027523040771484px 16.05504608154297px 0px #5D5FEF33;
    font-family: Lexend;
    font-size: 26px;
    font-weight: 400;
    line-height: 32px;
    letter-spacing: 0em;
    text-align: center;
    margin-left: -60px;
    color: white;
    border: none;
    cursor: pointer;
    display: flex;
    justify-content: center;
    align-items: center;
    &:active{
         background: linear-gradient(180deg, #03F5FF 0%, #0179d4 100%);
         box-shadow: none;
    }
    @media (max-width:970px){
        margin-left: 0px;
    }


`