import {Link} from 'react-router-dom'
import React from 'react'
import { Block, Container, FilterButton, Name, Number, ProfileImg } from './style'
import Profile_Img from '../../assets/images/prfile_im.png'
function Profile(props) {
    return (
        <Container >
            <ProfileImg src={Profile_Img} alt="" />
            <Name>{'Dostonbek'}</Name>
            <Number>{'+998991170407'}</Number>
            <Block>
                <FilterButton >Barcha kurslar</FilterButton>
                <FilterButton >Mening kurslarim</FilterButton>
                <FilterButton >Testlar</FilterButton>
                <FilterButton >Tahrirlash</FilterButton>
                <FilterButton >Chiqish</FilterButton>
            </Block>
        </Container>
    )
}

export default Profile
