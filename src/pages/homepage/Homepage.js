import React from 'react'
import { Comments, Header, HomeCourse, Platform, Widget, DarkAbout } from '../../components'
import CourseIntro from '../../components/CourseIntro/CourseIntro';
import VideoCourse from '../../components/VideoCourse/VideoCourse';
import Gifts from '../../components/Gifts/Gifts';
import Giftsinfo from '../../components/Gifts/Gifts__info';
import OnlineLearning from '../../components/OnlineLearning/OnlineLearning';
import { Container } from './styles';
import { useEffect } from "react";
// import Airplane1 from '../Airplane/airplane1';


const HomePage = () => {
    
    return (
        <Container>
            <Header />
            {/* <Airplane1 /> */}
            <Widget />
            <Platform />
            <HomeCourse />
            <DarkAbout />
            <CourseIntro />
            <VideoCourse />
            <OnlineLearning />
            <Giftsinfo />
            <Gifts />
            <Comments />
        </Container>
    )
}

export default HomePage;