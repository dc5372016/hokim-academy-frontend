import React from 'react'
import { Box, Box1, Button, Container, Input, Input1, Label, MainBox, Option, Rows, Rows1, Rows2, Select } from './style'

const Oqituvchi = () => {
  const [age, setAge] = React.useState('Erkak');
  const [mutahasis, setmutahasis] = React.useState('Dasturchi')

  const handleChange = (event) => {
    setAge(event.target.value);
  };
  const handleChange1 = (event) => {
    setmutahasis(event.target.value);
  };
  return (
    <Container>
        <Rows>
            <Box>
                <Label>Username</Label>
                <Input placeholder='Foydalanuvchi nomi' />
            </Box>
            <Box>
                <Label>Parol</Label>
                <Input placeholder='8 xonadan , @ $ & dan iborat bo’lish kerak' />
            </Box>
        </Rows>
        <Rows>
            <Box>
                <Label>F.I.Sh</Label>
                <Input placeholder='Familya , Ism, ...' />
            </Box>
            <Box>
                <Label>Jinsi</Label>
                <Select
                  value={age}
                  onChange={handleChange}
                >
                  <Option  value={'Erkak'}>Erkak</Option>
                  <Option  value={'Ayol'}>Ayol</Option>
                </Select>
            </Box>
        </Rows>
        <Rows>
            <Box>
                <Label>Tugʻilgan sana</Label>
                <Input  placeholder='Oy-Kun-Yil' />
            </Box>
            <Box>
                <Label>Telefon raqami</Label>
                <Input placeholder='+___ __ ___ __ __' />
            </Box>
        </Rows>
        <Rows1>
            <MainBox>
            <Box>
                <Label>Mutahasisligi</Label>
                <Select
                  value={mutahasis}
                  onChange={handleChange1}
                >
                  <Option  value={'Dasturchi'}>Dasturchi</Option>
                  <Option  value={'Matematik'}>Matematik</Option>
                  <Option  value={'Kimyogar'}>Kimyogar</Option>
                  <Option  value={'Fizik'}>Fizik</Option>
                </Select>
            </Box>
            <Box1>
                <Label> </Label>
                <Button>Akkaunt yaratish</Button>
            </Box1>
            </MainBox>
            <Box>
                <Label>Yashash joyi</Label>
                <Input1  placeholder='' />
            </Box>
        </Rows1>
        <Rows2>
             <Box>
                <Button>Akkaunt yaratish</Button>
            </Box>
        </Rows2>
    </Container>
  )
}

export default Oqituvchi