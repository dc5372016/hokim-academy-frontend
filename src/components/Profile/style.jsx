import styled from 'styled-components';

export const Container = styled.div`
    width: 70%;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;

`
export const ProfileImg = styled.img`
    width: 110px !important;
    height: 110px !important;
`
export const FilterButton = styled.span`
    margin-top: 15px;
    font-family: 'Lexend';
    font-size: 20px;
    font-weight: 300;
    line-height: 28px;
    letter-spacing: 0em;
    text-align: left;
    &:hover{
        cursor: pointer;
        color: blue;
        font-weight: 500;
    }

`
export const Block = styled.div`
    display: flex;
    flex-direction: column;
    width: 200px;
`
export const Number = styled.div`
    font-family: 'Lexend';
    font-size: 20px;
    font-weight: 300;
    line-height: 28px;
    letter-spacing: 0em;
    text-align: left;
    color: #8E8E8E;

`
export const Name = styled.div`
    font-family: 'Lexend';
    font-size: 33px;
    font-weight: 600;
    line-height: 41px;
    letter-spacing: 0em;
    text-align: left;

`