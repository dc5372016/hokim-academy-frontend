import * as React from 'react';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';

export default function CircularIndeterminate() {
  return (
    <Box sx={{ display: 'flex',marginLeft:3,fontSize:14 }}>
      <CircularProgress style={{color:'white',fontSize:10}} />
    </Box>
  );
}