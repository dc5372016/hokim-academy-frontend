import axios from 'axios';
import React, { Component, useEffect } from 'react';
import { useState } from 'react';
import { ProgressBar } from 'react-bootstrap';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import './pro.css'
// import Images
import arrow from "../../assets/icons/profil_arrow.svg"
import { AnswerWrapper, BackDiv, BackImg, BackText, BacktoDashboard, Block, CancelButton, CheckBox, ChoicesWrapper, Container, NextButton, NoTEST, Options, ProgressBarHere, QuestionWrapper, SubmitContainer, TestContainer, TestsHere, TestsWrapper, TestTitle, TimeTable, Wrapper,NewRadio } from './style';
import { Skeleton } from 'react-skeleton-generator';

import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import Swal from 'sweetalert2';
import Loading from '../Info_Courses_Next/Loading/Loading'
import Skeleton1 from './Skeleton/Skeleton';
import Buttonskeleton from './Skeleton/buttonskeleton'
import TitleSkeleton from './Skeleton/title'
import  BpRadio from './radioascheck'
import { baseURL } from '../../api/baseUrl';





const Testlar = () => {
    const location = useLocation();
    const navigate = useNavigate()
    const [tests, settests] = useState([]);
    console.log("categorydan kelgan location",location);
    let Auth_Data =
    JSON.parse(localStorage.getItem("access_key")) !== null
      ? JSON.parse(localStorage.getItem("access_key"))
      : { user: { full_name: "Unknown" } };
    const [render, setrender] = useState(false);
    const [next, setnext] = useState(0);
    const [quiz_length, setQuiz_length] = useState(0);
    let Quiz_id = location.state[0].id;
    let Course_ID = location.state[0].course;
    const [NEXTBUTTON, setNEXTBUTTON] = useState('Keyingisi');
    const [linear_value, setlinear_value] = useState(0)

    console.log('====================================');
    console.log('Testlar pppppppppppp',location.state[0]);
    console.log('====================================');


    useEffect(() => {
      window.scrollTo({
        top:0,
        behavior: 'auto'
      })
    }, [setnext,next]);

    useEffect(() => {
      if(quiz_length-next<6){
        setNEXTBUTTON('Tasdiqlash')
      }else{
        setNEXTBUTTON('Keyingi')
      }
    }, [setQuiz_length,quiz_length]);

    


    useEffect(() => {
      
      async function GetQuizes(){
        try {
          const res = await axios.get(`${baseURL}/api/quiz/question/${location.state[0].id}/`,{
            headers:{
              Authorization: `token ${Auth_Data.token}`,
            }
          })
          if(res){
            console.log("testlar = ",res);
            settests(res.data.data)
            setQuiz_length(res.data.data.length)
            let filtered = res.data.data.filter((item)=>{
              return item.answer !==null
            })
            setlinear_value(filtered.length*(100 / res.data.data.length) === 100 ? 100 : String(filtered.length*(100 / res.data.data.length)).slice(0,2))
            
            console.log('1111111111',linear_value);
          }
          
        } catch (err) {
          
        }
      }
      GetQuizes()
    }, [render,setrender,setnext,next]);

    
    const Post_Answer = async (choice_id,q_id) => {
      console.log('qu id',q_id);
      console.log('qu id',choice_id);
      let AnswerData = new FormData();
      AnswerData.append('question',q_id);
      AnswerData.append('answer',choice_id)

      let config = {
        method: 'post',
        url: `${baseURL}/api/quiz/question/${Quiz_id}/`,
        headers: { 
          Authorization: `token ${Auth_Data.token}`,
        },
        data : AnswerData
      };
      try {
        let res = await axios(config)
        if(res){
          console.log('Answer Posted',res);
          setrender(!render)
        }
      } catch (err) {
        alert(err.message)
      }
    }



   

    const KeyingiTest = () => {
      console.log({quiz_length:quiz_length,next:next})
      if(quiz_length-next<=6){
        setNEXTBUTTON('Tasdiqlanmoqda...')
        let config = {
          method: 'post',
          url: `${baseURL}/api/quiz/result/${Quiz_id}/`,
          headers: { 
            Authorization: `token ${Auth_Data.token}`,
          },
        };
        try {
          axios(config)
          .then((res)=>{
            console.log("Testni tugatish",res);
            if(res.data.success){
              Swal.fire({
                icon: 'success',
                title: 'Test yakunlandi!',
                showDenyButton:true.valueOf,
                text: 'Natijalarni ko`rish uchun natijalar tugmasini bosing!',
                confirmButtonText:'Natijalarni ko`rish',
                denyButtonText: `Xozir emas`,
                reverseButtons:true,

              }).then((result)=>{
                if (result.isConfirmed) {
                  navigate('/results',{state:res.data.data})
                } else if (result.isDenied) {

                }
                
              })
            }
            setNEXTBUTTON('Tasdiqlash')
          })
        } catch (err) {
          
        }
      }else{
        setnext(next+6)
      }
    }



    const CancelTest = () => {
      let config = {
        method: 'delete',
        url: `${baseURL}/api/quiz/quiz/${Quiz_id}/`,
        headers: { 
          Authorization: `token ${Auth_Data.token}`,
        },
      };
      try {
        axios(config).then((res)=>{
          console.log("delete",res);
          if(res.data.success){
            navigate(-1)
          }
        })
      } catch (err) {
        
      }
    }
   

    const GetChoicesList = (choices,question_id,answer) => {
      
      return choices?.map((item,index)=>(
        <ChoicesWrapper>
            {
             <FormControlLabel 
              value={item.id}
              control={<BpRadio  checked={item.id===answer*1 ? true : false }  />}
              onClick={()=>Post_Answer(item.id,question_id)}
              />
            }
            <Options>
                {item.choice}
            </Options>
        </ChoicesWrapper>
      ))
    }


      
      
    return (
      <React.Fragment>
        <Container>
          {/* <TimeTable>
             09:00
          </TimeTable> */}
          <Wrapper>
            <BacktoDashboard>
              <BackDiv onClick={()=>navigate(-1)}>
                <BackImg src={arrow} />
                <BackText>Kabinetga qaytish</BackText>
              </BackDiv>
            </BacktoDashboard>
            <TestContainer>
              <ProgressBarHere>
                  {
                    linear_value === 'Na' ? '' : linear_value ==   0 ? '' :  <ProgressBar  animated now={linear_value} label={`${linear_value}%`} />
                  }
              </ProgressBarHere>
              <TestsWrapper>
                  {
                    // tests.length !==0 ? 
                    <TestTitle>{location.state[0] !==undefined ? location.state[0].title : ''}ga oid savollar</TestTitle> 
                    // <TitleSkeleton />
                  }
                  <TestsHere>
                       {tests.length !== 0 ?
                         tests?.slice(next,next+6).map((item,index)=>(
                          <Block>
                            <QuestionWrapper>
                                {item.question.id}. {item.question.query}
                            </QuestionWrapper>
                                <RadioGroup
                                  aria-labelledby="demo-radio-buttons-group-label"
                                  defaultValue={1}
                                  name={item.id}
                                >
                            <AnswerWrapper>
                                  {GetChoicesList(item.question.choices,item.id,item.answer)}
                            </AnswerWrapper>
                                </RadioGroup>
                          </Block>
                        ))
                        : 
                        <div>
                          <Skeleton1 />
                          <Skeleton1 />
                          <Skeleton1 />
                        </div>
                       }
                  </TestsHere> 
              </TestsWrapper>
              {
                tests.length !== 0 ?
                <SubmitContainer>
                  <CancelButton onClick={CancelTest}>Bekor qilish</CancelButton>
                  <NextButton onClick={KeyingiTest}>{NEXTBUTTON}</NextButton>
                </SubmitContainer> 
                :
                <Buttonskeleton />
              }
            </TestContainer>
          </Wrapper>
        </Container>
      </React.Fragment>
    );
}
 
export default Testlar;