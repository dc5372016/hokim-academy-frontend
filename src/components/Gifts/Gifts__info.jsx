import React, { Component } from 'react';

import gifts from "../../assets/images/gifts.webp"

class Gifts__info extends Component {
  state = {  } 
  render() { 
    return (
      <React.Fragment>
        <div className="gifts__info" data-aos="fade-up">
          <div className="gifts__block">
            <div className="gifts__left" data-aos="fade-up-right">
              <img src={gifts} alt="" />
            </div>
            <div className="gifts__right">
              <p className="gifts__title" data-aos="fade-down">TA'LIM QANDAY OLIB BORILADI?</p>
              <div className="gr_items">
                <p data-aos="fade-left"><span>01</span> Individual ta'lim tezligi</p>
                <p data-aos="fade-left"><span>02</span> Barcha fanlar bir platformada</p>
                <p data-aos="fade-left"><span>03</span> O'rganilgan bilimlar nazorati</p>
                <p data-aos="fade-left"><span>04</span> Tengdoshlar bilan yutuqlarni taqqoslash imkoniyati</p>
                <p data-aos="fade-left"><span>05</span> Aksiya va promokodlar</p>
                <a href='#oqish' data-aos="fade-left">O'qish</a>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
 
export default Gifts__info;