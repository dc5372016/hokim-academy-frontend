import React, { useState } from 'react';
import user from "../assets/icons/user.png"

const UserMenu = (props) => {
  const [open , setOpen] = useState(false)
  const [open2 , setOpen2] = useState(false)
  const onClick = () =>{ 
    setOpen(!open)
    if (open === true) {
      setTimeout(() => {
        setOpen2(false)
      }, 700);
    } else {
      setOpen2(true)
    }
    
  }
  return ( 
    <React.Fragment>
      <div className='user__menu'>
        <button onClick={onClick}>
          User
          <img src={user} alt="" />
        </button>
        <div onClick={onClick} className={open === true ?'user__menu__show':""}></div>
        <div 
          style={open2 === true ? {display: "block"} : {display: "none"}}
          className={open === true ? "menu__items menu__items__show" : "menu__items"}
        >
          <a href="/">home</a>
        </div>
      </div>
    </React.Fragment>
   );
}
 
export default UserMenu;