import styled from 'styled-components';
import BG from '../../../assets/images/infomainbg.png';

export const Container = styled.div`
    width: 100%;
    height: 300px;
    /* background-image: url(${BG}); */
    /* background-color: red; */
    background-size:cover;
    max-width: 1900px;
    margin: auto;
    background-position: center center;
    display: flex;
    justify-content: center;
    align-items: center;
`
export const Button = styled.button`
    height: 60px;
    width: 283px;
    border-radius: 5.213675498962402px;
    background: rgba(3, 186, 237, 1);
    font-family: 'Lexend';
    font-size: 42px;
    font-weight: 500;
    line-height: 52px;
    letter-spacing: 0em;
    text-align: center;
    color: white;
    border: none;
`