import styled from 'styled-components';
export const Container = styled.div`
    width: 100%;
    /* border: 1px solid red; */
    display: flex;
    justify-content: center;
    align-items: center;
    margin-top: 80px;
`

export const Wrapper = styled.div`
    width: 100%;
    max-width: 1550px;
    padding: 15px;
    box-sizing: border-box;
    /* border: 1px solid red; */
`
export const ReusableTitle = styled.p`
    font-family: 'Lexend';
    font-size: 28px;
    font-weight: 500;
    line-height: 35px;
    letter-spacing: 0em;
    text-align: left;
    color: rgba(0, 0, 0, 1);
    border-bottom: 2px solid rgba(3, 245, 255, 1);
    padding-bottom: 15px;
    @media (max-width:1110px){
        width:95%;
        margin: auto;
    }
    @media (max-width:650px){
        width:95%;
        margin: auto;
    }
`


export const LessonCount = styled.span`
    background: linear-gradient(180deg, #03F5FF 0%, #02B6EB 100%);
    border-radius: 7px;
    padding: 7px;
    display: flex;
    align-items: center;
    justify-content: center;
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 500;
    font-size: 10px;
    line-height: 10px;
    position: absolute;
    top: 10px;
    right: 10px;
    /* identical to box height, or 100% */

    text-align: center;
    letter-spacing: 0.5px;

    color: #FFFFFF;
`;

export const Image = styled.img`
    border-top-left-radius: 20px;
    border-top-right-radius: 20px;
    height: 48%;
    object-fit: cover;
`;
export const Image1 = styled.img`
    border-radius: 100%;
    width: 52px;
    height: 52px;
    object-fit: cover;

`;
export const Content = styled.div`
    padding: 7px 25px;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    height: 52%;
    box-sizing: border-box;
    position: relative;
`;

export const AuthorContainer = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
`;

export const AvatarContainer = styled.div`
    width: 55px;
    height: 55px;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 3px;
    background-color: transparent;
    border-radius: 100%;
    position: relative;
    border: 1px solid white;
    position: absolute;
    top: -20px;
    right: 10px;
`;

export const Name = styled.span`
   font-family: 'Poppins';
    font-style: normal;
    font-weight: 400;
    font-size: 17px;
    line-height: 13px;
    margin-bottom: 15px;
    /* identical to box height, or 100% */

    letter-spacing: 0.5px;

    color: #B7B9D2;

    mix-blend-mode: normal; 
`;

export const Category = styled.span`
font-family: 'Poppins';
font-style: normal;
font-weight: 500;
font-size: 19px;
line-height: 40px;

letter-spacing: 0.3px;

color: #FFFFFF;
`;

export const Text1 = styled.span`
font-family: 'Poppins';
font-style: normal;
font-weight: 400;
font-size: 15px;
line-height: 12px;
/* identical to box height, or 100% */
letter-spacing: 0.5px;
color: #808191;
mix-blend-mode: normal;
`;

export const Button1 = styled.button`
    background: linear-gradient(180deg, #03F5FF 0%, #02B6EB 100%);
    box-shadow: 0px 4px 8px rgba(93, 95, 239, 0.2);
    border-radius: 10px;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 99%;
    height: 49px;
    border: none;
    cursor: pointer;
    position: relative;
    font-family: 'Lexend';
    font-style: normal;
    font-weight: 400;
    font-size: 16px;
    line-height: 12px;
    /* identical to box height, or 80% */
    box-sizing: border-box;
    text-align: center;
    letter-spacing: 0.5px;

    color: #FFFFFF;
`;

export const Container1 = styled.div`
    display: flex;
    flex-direction: column;
    background: #252836;
    border-radius: 20px;
    width: 338px;
    height: 447px;
    position: relative;
    margin: 15px;
    margin-top: 20px;
    font-family: 'Poppins';
`;

export const VidosWrap = styled.div`
    display: flex;
    margin-top: 70px;
    justify-content: flex-start;
    align-items: center;
    flex-wrap: wrap;
    max-width: 1800px;
    margin: auto;
    padding-top: 40px;
    @media (max-width:1510px){
        justify-content: center;
    }
    @media (max-width:1310px){
        justify-content: flex-start;
    }
    @media (max-width:1210px){
        justify-content: center;
    }
    @media (max-width:920px){
        justify-content: center;

    }
    @media (max-width:666px){
        justify-content: center;

    }
`