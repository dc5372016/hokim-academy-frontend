import styled from 'styled-components';
import CloseIcon from '@mui/icons-material/Close';
import NavigationIcon from '@mui/icons-material/Navigation';
import { IconButton } from '@mui/material';
export const Container = styled.div`
    width: 100%;
    /* border: 1px solid red; */
    display: flex;
    justify-content: center;
    align-items: center;
    box-sizing: border-box;
    margin-bottom: 500px;
    `
export const Wrapper = styled.div`
    position: relative;
    width: 100%;
    max-width: 1550px;
    padding: 15px;
    box-sizing: border-box;
    margin-top: -50px;
`

export const BacktoDiv = styled.div`
    padding-bottom: 0px;
`
export const BigContainer = styled.div`
    width: 100%;
    height: 648px;
    display: flex;
    justify-content: flex-start;
    align-items: center;
    @media (max-width:1410px){
        height: 550px;
    }
    @media (max-width:1200px){
        height: 500px;
    }
    @media (max-width:1100px){
        height: 450px;
    }
    @media (max-width:1100px){
        height: 400px;
    }
    @media (max-width:910px){
        flex-direction: column;
        margin: auto;
        height: auto;
    }
    
`
export const VideoContainer = styled.div`
    height: 100%;
    width: 60%;
    border-radius: 20px;
    background: ${props => props.show ? 'black' : `url(${props.bg})`};
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center center;
    display: flex;
    justify-content: center;
    align-items: center;
    @media (max-width:910px){
        width: 95%;
        height: 500px;
    }
    @media (max-width:710px){
        width: 95%;
        height: 400px;
    }
    @media (max-width:550px){
        width: 95%;
        height: 300px;
    }
    @media (max-width:440px){
        width: 99%;
        height: 250px;
    }
    @media (max-width:380px){
        width: 99%;
        height: 200px;
    }
`
export const TextContainer = styled.div`
    display: flex;
    align-items: flex-start;
    justify-content: flex-start;
    flex-direction: column;
    padding-left:70px;
    box-sizing: border-box;
    width: 40%;
    height: 100%;
    box-shadow: 0px 4px 10px 0px #00000026;
    margin-left: 30px;
    margin-right: 10px;
    padding-top: 10px;
    border-radius: 10px;
    overflow: scroll;
    overflow-x: hidden;
    overflow-y: hidden;
    @media (max-width:1200px){
        padding-left: 40px;
    }
    @media (max-width:1100px){
        padding-left: 20px;
    }
    @media (max-width:910px){
        display: none;
    }
`


export const Title = styled.div`
    

`

export const NavIcon = styled(NavigationIcon)`
    color: white !important;
    transform: rotate(-90deg);
    font-size: 30px !important;
    @media (max-width:510px){
        font-size: 25px !important;
    }

`
export const MenuButton = styled.div`
    width: 55px;
    height: 55px;
    border-bottom-left-radius: 40px;
    border-top-left-radius: 40px;
    background: linear-gradient(180deg, #03F5FF 0%, #02B6EB 100%);
    position: absolute;
    right: 0px;
    top: 15px;
    text-align: center;
    display: flex;
    justify-content: center;
    align-items: center;
    z-index: 98;

    &:active{
        background: linear-gradient(180deg, #03F5FF 0%, #0449ea 100%);
        box-shadow: 0px 0px 10px 3px aqua;
    }
    display: none !important;
    @media (max-width:910px){
        display: flex !important;
    }
    @media (max-width:510px){
        width: 45px;
        height: 45px;
    }

`
export const ListContainer = styled.div`
    width: 100%;
    position: absolute;
    left: -150%;
    top: 0px;
    background:#f4fbffe3;
    backdrop-filter: blur(5px);
    transition: .5s;
    overflow: scroll;
    overflow-x: hidden;
    overflow-y: hidden;
    &::-webkit-scrollbar {
        width: 5px;
        border-radius: 10px;
    }
    &::-webkit-scrollbar-track {
        box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    }
    &::-webkit-scrollbar-thumb {
        background: linear-gradient(180deg, #39f8ff 0%, #31cfff 100%);
        outline: 1px solid rgb(51, 153, 255);
        border-radius: 10px;
    }
    z-index: 99;
`
export const MyIconButton = styled(IconButton)`
    position: absolute !important;
    top: 7px !important;
    right: 15px !important;
    background: linear-gradient(180deg, #39f8ff 0%, #0ebff5 100%) !important;
    width: 50px !important;
    height: 50px !important;
`

export const XIcon = styled(CloseIcon)`
    color: #ffffff !important;
    font-size: 25px !important;
    
    width: 40px !important;
    height: 40px !important;
    transition: 0.3s !important;
    &:hover{
        cursor: pointer !important;
        transform: rotate(180deg) !important;
    }
`

export const Text = styled.div`
    font-family: 'Lexend';
    font-size: 22px;
    font-weight: 400;
    line-height: 28px;
    letter-spacing: 0px;
    text-align: left;
    color: rgba(0, 0, 0, 1);
    height: 58px;
    width: 338px;
    display: flex;
    align-items: center;
    justify-content: flex-start;
    transition: .1s;
    padding-left: 20px;
    margin: 4px;
    &:hover{
        border-radius: 5px;
        color: #0b24ff;
        cursor: pointer;

    }
    @media (max-width:1100px){
        font-size: 17px;
        width: 90%;
        padding-left: 10px;

    }
    @media (max-width:910px){
        font-size: 15px;
        width: 98%;
    }


`
export const QuizText = styled.p`
    color: #02B6EB;
    font-family: 'Lexend';
    font-size: 22px;
    font-weight: 300;
    line-height: 28px;
    text-align: left;
    padding-left: 20px;
    margin: 4px;
    transition:  .3s;
    &:hover{
        cursor: pointer;
        color: blue;
    }
    margin-bottom: 10px;
    @media (max-width:1100px){
        font-size: 17px;
        width: 90%;
        padding-left: 10px;

    }
    @media (max-width:910px){
        font-size: 15px;
        width: 98%;
    }
`


export const Img1 = styled.img`
    margin-left: 10px;
    @media (max-width:1110px){
        width: 45px;
        height: 60px;
    }
    @media (max-width:650px){
        width: 31px;
        height: 40px;
    }
`

export const PlayBtn = styled.div`
    height: 145px;
    width: 145px;
    border-radius: 100%;
    background: rgba(255, 255, 255, 0.2);
    display: ${props => props.show ? 'none' : 'flex'};
    justify-content: center;
    align-items: center;
    transition: .3s;
    &:hover{
        transform: scale(1.1);
        cursor: pointer;
    }
    @media (max-width:1110px){
        width: 110px;
        height: 110px;
    }
    @media (max-width:650px){
        width: 80px;
        height: 80px;
    }
`

