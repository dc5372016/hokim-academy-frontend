import React, { useEffect, useState } from "react";
import {
  AuthorContainer,
  AvatarContainer,
  Button1,
  Category,
  Container,
  Container1,
  Content,
  Image,
  Image1,
  LessonCount,
  Name,
  ReusableTitle,
  Text1,
  VidosWrap,
  Wrapper,
} from "./style";
import { Courses } from "../../../constants/courses";
import axios from "axios";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import { baseURL } from "../../../api/baseUrl";

const MyCourses = () => {
  const location = useLocation();
  const [data, setdata] = useState([]);
  const {course_id} = useParams()
  const [loading, setloading] = useState(false);
  const navigate = useNavigate();
  const [rasm, setrasm] = useState('');

  useEffect(() => {
    async function GetAllCourses() {
      try {
        const res = await axios.get(`${baseURL}/api/course/`);
        if(res){
          let filtered = res.data.data.filter((item)=>{
            if(item.author.name === 'Dostonbek'){
              return item
            }
          })
          setdata(filtered)
        }
        setloading(true)
      } catch (err) {
        console.log(err.message);
      }
    }
    GetAllCourses()
  }, []);

  const Ishla = () => {
    navigate('/courses')
  }

  return (
    <Container>
      {loading ? <Wrapper>
        <ReusableTitle>Mening kurslarim</ReusableTitle>
        <VidosWrap>
          {data?.map((item, index) => {
            return (
              <Container1
                key={index}>
                <Image src={baseURL+item.image} />
                <LessonCount>{item.lesson_count}ta dars</LessonCount>
                <Content>
                  <AuthorContainer>
                    <Name>{item.author.name}</Name>
                    <AvatarContainer>
                      <Image1
                        src={baseURL+item.author.image}
                        style={{ borderRadius: "25px" }}
                      />
                    </AvatarContainer>
                  </AuthorContainer>
                  <Category>{item.name}</Category>
                  <Text1>
                    {item.views} ko'rildi - {item.day} kun oldin
                  </Text1>
                  <Button1 onClick={Ishla}>Boshlash</Button1>
                </Content>
              </Container1>
            );
          })}
        </VidosWrap>
      </Wrapper>: ''}
    </Container>
  );
};

export default MyCourses;
