import React, { useEffect } from 'react'
import axios from 'axios';
import MyCoursesAnalytic from '../../components/MyCoursesAnalytic/MyCoursesAnalytic'
import { useState } from 'react';
import { LoadingWrapper } from '../../components/AllCourses/style';
import CircularIndeterminate from '../../components/RegisterComponent/Circular/Progress';
import { Box, CircularProgress } from '@mui/material';
import { baseURL } from '../../api/baseUrl';





function MyCourses() {
    const [data, setData] = useState([])
    const [loading, setloading] = useState(true);
    useEffect(()=>{
        const test= async()=>{
            const res =  await axios.get(`${baseURL}/api/course`);
            setData(res.data.data);
            if(res){
                setloading(false)
            }
        }
        test()
    },[])
    console.log(data);
return (
<div className="my-courses">
    <div className="my__courses-product">
        {loading ===false ? <div>
        <h2 className='Mainh2'  >Kurslarim</h2>
        <div className="my__courses-list">
            {data && data.map((e,i)=>(
                <MyCoursesAnalytic key={i} progress={Math.floor(e.lesson_count/30 *100)} style={{padding:'40px 0'}}
                    img={baseURL+e.image}
                    name={e.name} />
            ))}
        </div>
        <h2 >Saqlanganlar</h2>
        <div className="my__courses-list">
        {data && data.map((e,i)=>(
                <MyCoursesAnalytic key={i} progress={Math.floor(e.lesson_count/30 *100)} style={{padding:'40px 0'}}
                    img={baseURL+e.image}
                    name={e.name} />
            ))}
        </div>
        <h2 >Testlar</h2>
        <div className="my__courses-list">
        {data && data.map((e,i)=>(
                <MyCoursesAnalytic key={i} progress={Math.floor(e.lesson_count/30 *100)} style={{padding:'40px 0'}}
                    img={baseURL+e.image}
                    name={e.name} />
            ))}
        </div>
        </div>
        :
        <LoadingWrapper>
            <Box sx={{ display: 'flex',marginLeft:3,fontSize:14 }}>
               <CircularProgress style={{color:'',fontSize:10}} />
            </Box>
     <h3 style={{ marginLeft: 10 }}>Loading...</h3>
        </LoadingWrapper>}
    </div>
</div>
)
}
export default MyCourses;