import React from 'react'

import { Container, FilterBtn, SubContainer } from './styles';

const Data = ['Aniq Fanlar','Dasturlash','Chet Tili','Yangi Kurslar','Graphic Design']

const LeftFilter = ({Main_Filter,category}) => {
    return (
        <Container>
            <SubContainer>
               {Data.map((item,index)=>{
                   return(
                    <FilterBtn style={{
                        background:item===category ?
                         'linear-gradient(180deg, #03F5FF 0%, #02B6EB 100%)' : '',
                         color:item===category ? 'white' : '#03F5FF'
                        }} key={index} onClick={()=>Main_Filter(item)}
                        >{item}</FilterBtn>
                   )
               })}
            </SubContainer>
        </Container>
    )
}

export default LeftFilter;