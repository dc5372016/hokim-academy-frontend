export const HtmlData = [
    {
        id:1,
        title:'Html Intro',
        url:'https://youtu.be/pm5OVxpul48'
    },
    {
        id:2,
        title:'Html Teglar',
        url:'https://youtu.be/UO0ZPL8yMpU'
    },
    {
        id:3,
        title:'Html Atributlar',
        url:'https://youtu.be/qz0aGYrrlhU'
    },
    {
        id:4,
        title:'Html Elementlar',
        url:'https://youtu.be/wvR40su_XBM'
    },
    {
        id:5,
        title:'Html Sarlavhalar',
        url:'https://youtu.be/CQZxeoQeo5c'
    },

    {
        id:6,
        title:'Html Paragraflar',
        url:'https://youtu.be/bCt2FnyY7AE'
    },
    {
        id:7,
        title:'Html Ranglar',
        url:'https://youtu.be/qz0aGYrrlhU'
    },
    {
        id:8,
        title:'Html Css',
        url:'https://youtu.be/s3h5FLBon88'
    },
    {
        id:9,
        title:'Html Havolalar',
        url:'https://youtu.be/rO6_MZLIzCg'
    },
    {
        id:10,
        title:'Html Tasvirlar',
        url:'https://youtu.be/qz0aGYrrlhU'
    },
    {
        id:11,
        title:'Html Jadvallar',
        url:'https://youtu.be/09oErCBjVns'
    },
    
]