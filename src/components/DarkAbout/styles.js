import styled from 'styled-components';
import DarkBg from '../../assets/images/dark_bg.png';

export const Container = styled.div`
    display: flex;
    position: relative;
    justify-content: center;
    background-image: url(${DarkBg});
    width: 100%;
    padding-bottom: 40px;
`;
export const Container1 = styled.div`
    display: flex;
    position: relative;
    background-image: url(${DarkBg});
    width: 100%;
    flex-direction: column;
    max-width: 1920px;
`;
export const Title = styled.span`
    font-family: 'Lexend';
    font-style: normal;
    font-weight: 400;
    font-size: 20px;
    line-height: 25px;
    text-align: center;
    text-transform: uppercase;
    background: linear-gradient(180deg, #03F5FF 0%, #02B6EB 100%);
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
    background-clip: text;
`;

export const SubTitle = styled.span`
    font-family: 'Lexend';
    font-style: normal;
    font-weight: 500;
    font-size: 46px;
    line-height: 58px;
    text-align: center;
    color: #FFFFFF;
`;

export const CardContainer = styled.div`
    display: grid;
    grid-template-columns: auto auto auto auto;
    margin-top: 110px;
    grid-gap: 30px;
    position: relative;
    z-index: 1;
    
`;

export const Card = styled.div`
    position: relative;
    background: linear-gradient(132.43deg, rgba(90, 99, 194, 0.2) 2.98%, rgba(11, 16, 67, 0.2) 100%);
    box-shadow: 0px 8px 20px rgba(9, 237, 245, 0.15);
    backdrop-filter: blur(20px);
    border-radius: 20px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    grid-gap: 20px;
    width: 319px;
    height: 295px;
`;

export const SharImg = styled.img`
    position: absolute;
    z-index: -1;
`;

export const IconContainer = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 111px;
    height: 111px;
    background: #0B1043;
    box-shadow: 0px 0px 15px rgba(9, 237, 245, 0.5);
    border-radius: 20px;
`;

export const CardText = styled.span`
    font-family: 'Lexend';
    font-style: normal;
    font-weight: 500;
    font-size: 28px;
    line-height: 35px;
    /* identical to box height */

    text-align: center;

    color: #FFFFFF;
`;

export const CardTextCount = styled.span`
    font-family: 'Lexend';
    font-style: normal;
    font-weight: 300;
    font-size: 26px;
    line-height: 32px;
    /* identical to box height */

    text-align: center;

    color: #FFFFFF;  
`;

export const CardTextInfo = styled.span`
    font-family: 'Lexend';
    font-style: normal;
    font-weight: 400;
    font-size: 18px;
    line-height: 22px;
    color: #FFFFFF;  
`;

export const Icon = styled.img``;

export const Button = styled.button`
    display : flex;
    justify-content: center;
    align-items: center;
    width: 230px;
    height: 64px;
    background: linear-gradient(180deg, #03F5FF 0%, #02B6EB 100%);
    box-shadow: 0px 8.02752px 16.055px rgba(93, 95, 239, 0.2);
    border-radius: 8.02752px;
    cursor: pointer;
    border: none;
    font-family: 'Lexend';
    font-style: normal;
    font-weight: 400;
    font-size: 25.6881px;
    line-height: 32px;
    transition: all 0.5s;
    color: #FFFFFF;
    &:hover{
        transform: scale(1.03);
    }
`;

export const ButtonContainer = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    margin-top: 100px;
`;

export const CardDemo = styled.div`
    display: flex;
    position: relative;
    justify-content: center;
`;


export const FirstContainer = styled.div`
    display: flex;
    position: relative;
    flex-direction: column;
    justify-content: center;
    margin: 50px;
    @media (max-width:1500px){
        ${Card}{
            width: 280px;
            height: 260px;
        }
        ${IconContainer}{
            width: 95px;
            height: 95px;
        }
        ${CardText}{
            font-size: 24px;
        }
        ${CardTextCount}{
            font-size: 22px;
        }
    }
    @media (max-width:1350px){
        ${Card}{
            width: 250px;
            height: 220px;
        }
        ${IconContainer}{
            width: 80px;
            height: 80px;
        }
        ${Icon}{
            width: 50px;
        }
        ${CardText}{
            font-size: 20px;
        }
        ${CardTextCount}{
            font-size: 18px;
        }
        ${SubTitle}{
            font-size: 34px;
        }
        ${Button}{
            height: 50px;
            width: 210px;
            font-size: 22px;
        }
    }
    @media (max-width:1185px){
        ${Card}{
            width: 215px;
            height: 195px;
        }
        ${IconContainer}{
            width: 70px;
            height: 70px;
        }
        ${Icon}{
            width: 47px;
        }
        ${CardText}{
            font-size: 18px;
        }
        ${CardTextCount}{
            font-size: 16px;
        }
    }
    @media (max-width:1025px){
        ${CardContainer}{
            grid-template-columns: auto auto;
        }
        ${Card}{
            width: 319px;
            height: 295px;
        }
        ${IconContainer}{
            width: 111px;
            height: 111px;
        }
        ${CardText}{
            font-size: 28px;
            line-height: 35px;
        }
        ${CardTextCount}{
            font-size: 26px;
            line-height: 32px;
        }
        ${Button}{
            width: 230px;
            height: 64px;
            font-size: 25px;
        }
    }
    @media (max-width:780px){
        ${Card}{
            width: 250px;
            height: 220px;
        }
        ${IconContainer}{
            width: 80px;
            height: 80px;
        }
        ${Icon}{
            width: 50px;
        }
        ${CardText}{
            font-size: 20px;
        }
        ${CardTextCount}{
            font-size: 18px;
        }
        ${SubTitle}{
            font-size: 34px;
        }
    }
    @media (max-width:610px){
        ${CardContainer}{
            grid-template-columns: auto;
        }
        ${Card}{
            width: 319px;
            height: 295px;
        }
        ${IconContainer}{
            width: 111px;
            height: 111px;
        }
        ${CardText}{
            font-size: 28px;
            line-height: 35px;
        }
        ${CardTextCount}{
            font-size: 26px;
            line-height: 32px;
        }
    }
    @media (max-width:400px){
        ${Card}{
            width: 250px;
            height: 220px;
        }
        ${IconContainer}{
            width: 80px;
            height: 80px;
        }
        ${Icon}{
            width: 50px;
        }
        ${CardText}{
            font-size: 20px;
        }
        ${CardTextCount}{
            font-size: 18px;
        }
    }
`;

export const ContentContainer = styled.div`
    display: flex;
    flex-direction: column;
    margin-top: 80px;
    
`;

export const ContentTitle = styled.span`
font-family: 'Lexend';
font-style: normal;
font-weight: 500;
font-size: 26px;
line-height: 32px;
/* identical to box height */

text-align: center;

color: #FFFFFF;
`;

export const ContentText = styled.span`
font-family: 'Lexend';
font-style: normal;
font-weight: 400;
font-size: 17px;
line-height: 22px;
color: #FFFFFF;
`;

export const TextContainer = styled.div`
    display: flex;
    flex-direction:column;
    align-items: flex-start;
    position: relative;
    top: 40px;
    left: -75px;
    width: fit-content;
`;

export const ContentImage = styled.img`
    width: 600px;
`;

export const Div = styled.div`
    display: flex;
    justify-content: space-between;
    &.br1{
        margin: 0px 40%;
        justify-content: center;
    }
    &.br2{
        margin: 0px 20%;
    }
    &.br3{
        margin: 0px 5%;
    }
`;

export const UstunlikContainer = styled.div`
    display: flex;
    position: relative;
    svg{
        position: relative;
    }
`;

export const SecondContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    margin-top: 100px;
    @media (max-width: 1500px) {
        ${Div}{
            &.br1{
                margin: 0px 37%;
            }
            &.br2{
                margin: 0px 20%;
            }
            &.br3{
                margin: 0px 5%;
            }
        }
        ${ContentImage}{
            width: 500px;
        }
        ${ContentTitle}{
            font-size: 24px;
            line-height: 30px;
        }
        ${ContentText}{
            font-size: 16px;
            line-height: 20px;
        }
    }
    @media (max-width: 1300px) {
        ${Div}{
            &.br1{
                margin: 0px 37%;
            }
            &.br2{
                margin: 0px 20%;
            }
            &.br3{
                margin: 0px 15px;
            }
        }
        ${ContentImage}{
            width: 420px;
        }
        ${ContentTitle}{
            font-size: 23px;
            line-height: 28px;
        }
        ${ContentText}{
            font-size: 15px;
            line-height: 18px;
        }
    }
        @media (max-width: 1108px) {
        ${Div}{
            &.br1{
                margin: 0px 37%;
            }
            &.br2{
                margin: 0px 20%;
            }
            &.br3{
                margin: 0px 15px;
            }
        }
        ${ContentImage}{
            width: 380px;
        }
        ${ContentTitle}{
            font-size: 22px;
            line-height: 26px;
        }
        ${ContentText}{
            font-size: 14px;
            line-height: 17px;
        }
    }
    @media (max-width: 1025px) {
        ${ContentImage}{
            width: 320px;
        }
        ${ContentTitle}{
            font-size: 22px;
            line-height: 26px;
        }
        ${ContentText}{
            font-size: 14px;
            line-height: 17px;
        }
        ${UstunlikContainer}{
            margin: 0px;
        }
    }
    @media (max-width: 900px) {
        ${Div}{
            &.br1{
                margin: 0px 30%;
            }
            &.br2{
                margin: 0px 15%;
            }
            &.br3{
                margin: 0px 15px;
            }
        }
        ${ContentImage}{
            width: 270px;
        }
        ${ContentTitle}{
            font-size: 20px;
            line-height: 24px;
        }
        ${ContentText}{
            font-size: 13px;
            line-height: 16px;
        }
    }
    @media (max-width: 850px) {
        ${Div}{
            padding: 0px 80px;
            &.br1{
                display: flex;
                margin: 0px;
                flex-direction: column;

            }
            &.br2{
                flex-direction: column;
                margin: 0px;
            }
            &.br3{
                flex-direction: column;
                margin: 0px;
            }
        }
        ${UstunlikContainer}{
            &.uz2{
                margin-left: auto;
            }
            &.uz1{
                margin-right: auto;
            }
        }
        ${ContentImage}{
            display: none;
        }
    }
    @media (max-width: 700px) {
        ${Div}{
            padding: 0px 40px;
        }
    }

    @media (max-width: 610px) {
        ${Div}{
            padding: 0px 40px;
        }
    }
`;
