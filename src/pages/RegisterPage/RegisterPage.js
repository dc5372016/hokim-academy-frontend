import React from 'react'
import Register from '../../components/RegisterComponent/Register';

const RegisterPage = ({bigRender,setbigRender}) => {
  return (
    <>
      <Register  setbigRender={setbigRender} bigRender={bigRender} />
    </>
  )
}

export default RegisterPage