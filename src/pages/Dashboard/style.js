import styled from 'styled-components';
import LogoutSharpIcon from '@mui/icons-material/LogoutSharp';
import { Button, MenuItem } from '@mui/material';

export const AllContainer = styled.div`
    width: 100%;
    display: flex;
    justify-content: center;
`

export const Container = styled.div`
    width: 100%;
    /* border: 1px solid red; */
    max-width: 1550px;
    display: flex;
    justify-content: space-between;
    margin-top: 200px;
    height: 900px;
    padding-bottom: 50px;
    /* padding: 20px; */
    box-sizing: border-box;
    @media (max-width:1200px){
        flex-direction: column;
    }
    
`


export const ProfileContainer = styled.div`
    width: 15%;
    /* border: 1px solid yellow; */
    height: 100%;
    position: relative;
    box-sizing: border-box;
    @media (max-width:1200px){
       width: 100%;
    }
`
export const RightContainer = styled.div`
    width: 85%;
    /* border: 1px solid blue; */
    height: 100%;
    padding-left: 15px;
    padding-right: 15px;
    box-sizing: border-box;
    @media (max-width:1200px){
       width: 100%;
    }
`

export const ProfileWrapper = styled.div`
    width: 100%;
    height: 100%;


    position: fixed;
    width: 230px;
    /* border-right: 1px solid aqua; */
    padding-right: 20px;
    padding: 20px;
    padding-top: 0;
    box-sizing: border-box;
    @media (max-width:1500px){
        border: none;
    }
    @media (max-width:1200px){
       position: static;
       width: 100%;
       padding-left: 20px;
       padding-bottom: 20px;
    }
    



`

export const ProfileImg = styled.img`
    width: 110px !important;
    height: 110px !important;
    border-radius:100%;
    @media (max-width:800px){
        width: 80px !important;
        height: 80px !important;
    }
    @media (max-width:650px){
        width: 60px !important;
        height: 60px !important;
    }
`

export const Block = styled.div`
    display: flex;
    flex-direction: column;
    width: 200px;
    
    @media (max-width:1200px){
       flex-direction: row;
       justify-content: space-between;
       align-items: center;
       width: 100%;
    }

    @media (max-width:650px){
        align-items: center;
    }

`
export const Number = styled.div`
    font-family: 'Lexend';
    font-size: 18px;
    font-weight: 300;
    line-height: 10px;
    letter-spacing: 0em;
    text-align: left;
    color: #8E8E8E;
    margin-top: 15px;
    margin-bottom: 20px;
    @media (max-width:1200px){
       display: none;
    }

`

export const ExitIcon = styled(LogoutSharpIcon)`
    margin-left: 55px !important;
    color:  #333333 !important;

    

`
export const ExitIcon1 = styled(LogoutSharpIcon)`
    margin-left: 75px !important;
    color:  #333333 !important;

    

`

export const Name = styled.div`
    font-family: 'Lexend';
    font-size: 29px;
    font-weight: 600;
    line-height: 30px;
    letter-spacing: 0em;
    text-align: left;
    margin-top: 20px;
    @media (max-width:1200px){
       display: none;
    }
`
export const FilterButton = styled.button`
    margin-top: 15px;
    font-family: 'Lexend';
    font-size: 20px;
    font-weight: 300;
    line-height: 20px;
    letter-spacing: 0em;
    text-align: left;
    border: none;
    background: none;
    transition:0.3s;
    @media (max-width:800px){
       font-size: 17px;
    }
    &:hover{
        cursor: pointer;
        color: #7F74FC;
    }
    @media (max-width:650px){
        display: none;
    }

    &:hover .MuiSvgIcon-fontSizeMedium {
        transition: 1s;
        transform: rotateY(360deg) !important;
        color: #7F74FC !important;
    }

`
export const FilterButton2 = styled.button`
    margin-top: 15px;
    font-family: 'Lexend';
    font-size: 20px;
    font-weight: 300;
    line-height: 20px;
    letter-spacing: 0em;
    text-align: left;
    border: none;
    background: none;
    transition:0.3s;
    @media (max-width:800px){
       font-size: 17px;
    }
    &:hover{
        cursor: pointer;
        color: #7F74FC;
    }
    @media (max-width:650px){
        display: none;
    }

    &:hover .MuiSvgIcon-fontSizeMedium {
        transition: 1s;
        transform: rotate(360deg) !important;
        color: #7F74FC !important;

    }

`
export const FilterButton1 = styled.button`
    margin-top: 15px;
    font-family: 'Lexend';
    font-size: 20px;
    font-weight: 300;
    line-height: 28px;
    letter-spacing: 0em;
    text-align: left;
    border: none;
    background: none;
    display: flex;
    transition:0.3s;
    align-items: center;
    &:hover ${ExitIcon1}{
        color: blue !important;
    }
    &:hover ${ExitIcon}{
        color: blue !important;
    }
    &:hover{
        cursor: pointer;
        color: blue !important;
        
    }
    @media (max-width:650px){
        display: none;
    }

`

export const ResponsiveMenu = styled.div`
    display: none;
    @media (max-width:650px){
        display: block;
    }
`

export const MenuItemStyled = styled(MenuItem)`
    font-family: 'Lexend' !important;
    
`


export const MenuButtonCool = styled(Button)`
    display: flex !important;
    justify-content: space-around !important;
    align-items: center !important;
    font-size: 20px;
`