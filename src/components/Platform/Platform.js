import React from 'react';
import Atropos from 'atropos/react';
import { Card, CardContainer, CardText, CardTitle, Container, Icon, IconContainer, Image, LeftContainer, RightContainer, Title } from './styles';
import PlatformImage from '../../assets/images/platform_image.png';
import P_card_1 from '../../assets/images/p_card/p_card_1.png';
import P_card_2 from '../../assets/images/p_card/p_card_2.png';
import P_card_3 from '../../assets/images/p_card/p_card_3.png';
import P_card_4 from '../../assets/images/p_card/p_card_4.png';

const Platform = () => {
    
    return (
        <Container>
            <LeftContainer>
                <Atropos
                    className="atropos-banner"
                    highlight={true}
                    shadow={false}
                >
                     <Image src={PlatformImage} data-aos="flip-left" data-aos-duration="2000" />
                </Atropos>
            </LeftContainer>
            <RightContainer>
                <Title>Platforma qanday tashkil etilgan</Title>
                <CardContainer>
                    <Card data-aos="zoom-in-up" data-aos-duration="2000">
                        <IconContainer>
                            <Icon src={P_card_1} />
                        </IconContainer>
                        <CardTitle>Mashgʻulotlar tuzilishi</CardTitle>
                        <CardText>Alltestʼda taʼlim jarayoni tuzilmasi quyidagicha: boʻlim (Boʻlim) – tagboʻliм (Dars) – bob (dars qismi). Dars va boʻlim tamomlangach, abituriyent materialning qanchalik oʻzlashtirganini baholash uchun fan boʻyicha sinov topshiriqlarini bajaradi;</CardText>
                    </Card>
                    <Card data-aos="zoom-in-up" data-aos-duration="2000">
                        <IconContainer>
                            <Icon src={P_card_2} />
                        </IconContainer>
                        <CardTitle>Sifatli kontent</CardTitle>
                        <CardText>Har bir fan videodarslar va konspektdan iborat;</CardText>
                    </Card>
                    <Card data-aos="zoom-in-up" data-aos-duration="2000">
                        <IconContainer>
                            <Icon src={P_card_3} />
                        </IconContainer>
                        <CardTitle>Taʼlimning izchilligi</CardTitle>
                        <CardText>Taʼlimning izchilligi abituriyent darsni yuqori foizli oʻzlashtirish bilan yakunlamagunicha yangi mashgʻulotlarga kirish huquqi berilmasligi bilan taʼminlanadi.
                        </CardText>
                    </Card>
                    <Card data-aos="zoom-in-up" data-aos-duration="2000">
                        <IconContainer>
                            <Icon src={P_card_4} />
                        </IconContainer>
                        <CardTitle>Takrorlash imkoniyati</CardTitle>
                        <CardText>Har bir foydalanuvchinda oldingi mashgʻulotlarga qaytish va oʻz bilimlarini tekshirish imkoniyati bor.</CardText>
                    </Card>
                </CardContainer>
            </RightContainer>
        </Container>
    )
}

export default Platform;