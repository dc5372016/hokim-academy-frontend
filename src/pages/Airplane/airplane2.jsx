import React, { Component } from 'react';
import Samalyot from "../../assets/icons/samalyot.svg"
import { Controller, Scene } from 'react-scrollmagic-r18';

class Airplane2 extends Component {
  state = { 
    scroll: 0,
    moveY: 0,
    moveX: 0,
    rotate:-25,
   } 
  render() { 
    console.log(this.state.scroll)
    return (
      <React.Fragment>
        {/* <div className='airplane2'>
          <img style={{
              transform: `
              translateX(${this.state.moveX}px) 
              translateY(${this.state.moveY}px) 
              rotate(${this.state.rotate}deg)
              scaleX(-1)
              `
            }} 
          src={Samalyot} alt="" />
        </div> */}
        <div className="airplane2">
          <div id="trigger" />
          <Controller>
            <Scene 
              duration={500} 
              pin={false}
              triggerElement="#trigger" 
              indicators={true}
              
            >
              {(progress, event,) => (
                <div className="test" > 
                  <p>{progress}</p>
                  {progress < 0.5 
                  ? <img style={{
                      transform: `
                        translateX(${this.state.moveX + (progress * -180)}px) 
                        translateY(${this.state.moveY + (progress * 600)}px) 
                        rotate(${this.state.rotate + (progress * -300)}deg)
                        scaleX(1)
                        `
                      }} 
                    src={Samalyot} alt="" />
                    
                  : progress >= 0.6 
                  ? <img style={{
                      transform: `
                        translateX(${this.state.moveX + (progress * -180)}) 
                        translateY(${this.state.moveY + (progress * 600)}) 
                        rotate(${this.state.rotate + (progress * -300)})
                        scaleX(1)
                        `
                      }} 
                    src={Samalyot} alt="" />
                  : progress >= 0.7
                  ? <img style={{
                      transform: `
                        translateX(${this.state.moveX}px) 
                        translateY(${this.state.moveY}px) 
                        rotate(${-180}deg)
                        scaleX(1)
                        `
                      }} 
                    src={Samalyot} alt="" />
                  : progress >= 0.8 
                  ? <img style={{
                      transform: `
                        translateX(${this.state.moveX}px) 
                        translateY(${this.state.moveY}px) 
                        rotate(${-180}deg)
                        scaleX(1)
                        `
                      }} 
                    src={Samalyot} alt="" />
                  : progress >= 0.9 
                  ? <img style={{
                      transform: `
                        translateX(${this.state.moveX}px) 
                        translateY(${this.state.moveY}px) 
                        rotate(${-180}deg)
                        scaleX(1)
                        `
                      }} 
                    src={Samalyot} alt="" />

                  : <img style={{
                    transform: `
                      translateX(${this.state.moveX}px) 
                      translateY(${this.state.moveY}px) 
                      rotate(${-180}deg)
                      scaleX(1)
                      `
                    }} 
                  src={Samalyot} alt="" />
                }
                </div>
              )}
            </Scene>
          </Controller>
        </div>

      </React.Fragment>
    );
  }
}
 
export default Airplane2;