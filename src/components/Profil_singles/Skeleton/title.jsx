import React from "react";
import { Skeleton } from "react-skeleton-generator";

const Skeleton1 = () => {
  return (
    <Skeleton.SkeletonThemeProvider animationDuration={2}>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          marginTop:25,
        }}
      >
            <div style={{margin:10,width:'100%',display:'flex',justifyContent:'center'}}>
                 <Skeleton width="60%" height="35px" borderRadius="10px" />
            </div>
      </div>
    </Skeleton.SkeletonThemeProvider>
  );
};

export default Skeleton1;