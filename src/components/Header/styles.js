import styled from "styled-components";
import BgiHeader from '../../assets/images/bgi_header.png'


export const Text = styled.span`
    font-family: 'Lexend';
    font-style: normal;
    font-weight: 300;
    font-size: 22px;
    line-height: 28px;
    color: #333333;
    width: 100%;
    word-wrap: break-word;
    @media (max-width: 1560px) {
        font-size: 20px;
        line-height: 25px;
    }
    @media (max-width: 400px) {
        width: auto;
    }
`;

export const Button = styled.button`
    display: flex;
    z-index: 1;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    padding: 9.20583px 39.8919px;
    cursor: pointer;
    border: none;
    width: 150px;
    margin-top: 40px;
    outline: none;
    font-family: 'Lexend';
    font-style: normal;
    font-weight: 400;
    font-size: 24.5489px;
    line-height: 31px;

    color: #FFFFFF;
    background: linear-gradient(180deg, #02F5FF 0%, #01B5EA 100%);
    box-shadow: 0px 7.67152px 15.343px rgba(93, 95, 239, 0.2);
    border-radius: 7.67152px;
    transition: all 0.3s;
    &:hover{
        transform: scale(1.02);
    }
`;

export const LightTitle = styled.span`
background: linear-gradient(180deg, #02F5FF 0%, #01B5EA 100%);
box-shadow: 0px 7.67152px 15.343px rgba(93, 95, 239, 0.2);
border-radius: 7.67152px;
font-family: 'Lexend';
    font-style: normal;
    font-weight: 400;
    font-size: 56px;
    line-height: 70px;
    color: white;
    padding: 5px 10px;
    @media (max-width: 1560px) {
        font-size: 50px;
        line-height: 60px;
    }
`;

export const Title = styled.span`
    font-family: 'Lexend';
    font-style: normal;
    font-weight: 400;
    font-size: 56px;
    line-height: 70px;
    color: black;
    @media (max-width: 1560px) {
        font-size: 50px;
        line-height: 60px;
    }
    @media (max-width: 400px) {
        word-wrap: break-word;
    }
`;


export const Container = styled.div`
    display: flex;
    flex-direction: column;
    background-color: white;
    padding: 200px;
    padding-bottom: 160px;
    overflow-x: hidden;
    max-width: 1920px;
    box-sizing: border-box;
    @media (max-width: 1700px) {
        padding-left: 100px;
        padding-right: 100px;
    }
    @media (max-width: 600px) {
        padding: 40px;
        padding-top: 150px;
        padding-bottom: 230px;
    }
    @media (max-width: 400px) {
        padding: 10px;
        padding-top: 150px;
        padding-bottom: 230px;
        
    }
`;

export const Container1 = styled.div`
    display: flex;
    box-sizing: border-box;
    position: relative;
    justify-content: center;
`;

export const LeftContainer = styled.div`
    display: flex;
    flex-direction: column;
    
`;

export const SvgContainer = styled.div`
    display: flex;
    bottom: 0px;
    width: 100%;
    position: absolute;
    left: 0px;
    right: 0px;
`;

export const MainContainer = styled.div`
    display: grid;
    grid-template-columns: 50%  50%;
    height: 100%;
    @media (max-width: 1100px) {
        grid-template-columns: 100%;
        grid-gap: 20px;
    }
    
`;

export const RightGradiant = styled.div`
    position: absolute;
    height: 500px;
    width: 500px;
    top: -100px;
    right: 0px;
    background: radial-gradient(50% 50% at 50% 50%, rgba(64, 123, 255, 0.9) 0%, rgba(79, 64, 255, 0.031) 100%);
    filter: blur(200px);
    @media (max-width: 1600px) {
        height: 500px;
        width: 500px;
        /* right: -250px; */
        top: -200px;
        filter: blur(250px);
    }
    @media (max-width: 1400px) {
        height: 500px;
        width: 500px;
        /* right: -200px; */
        top: -100px;
        filter: blur(200px);
    }
    @media (max-width: 1200px) {
        height: 400px;
        width: 250px;
        /* right: -200px; */
        top: -50px;
        filter: blur(150px);
    }
    @media (max-width: 1000px) {
        height: 400px;
        width: 200px;
        /* right: -250px; */
        top: -50px;
        filter: blur(100px);
    }
    @media (max-width: 800px) {
        height: 400px;
        width: 200px;
        /* right: -300px; */
        top: -100px;
        filter: blur(100px);
    }
`;

export const LeftGradiant = styled.div`
    position: absolute;
    height: 1000px;
    width: 800px;
    top: -500px;
    left: -320px;
    background: radial-gradient(50% 50% at 50% 50%, rgba(195, 26, 255, 0.5) 0%, rgba(195, 26, 255, 0) 100%);
    filter: blur(300px);
    @media (max-width: 1600px) {
        height: 800px;
        width: 800px;
        left: -250px;
        top: -250px;
        filter: blur(250px);
    }
    @media (max-width: 1400px) {
        height: 600px;
        width: 600px;
        left: -200px;
        top: -150px;
        filter: blur(200px);
    }
    @media (max-width: 1200px) {
        height: 600px;
        width: 600px;
        left: -200px;
        top: -100px;
        filter: blur(150px);
    }
    @media (max-width: 1000px) {
        height: 400px;
        width: 400px;
        left: -100px;
        top: -50px;
        filter: blur(100px);
    }
    @media (max-width: 800px) {
        height: 400px;
        width: 400px;
        left: -150px;
        top: -50px;
        filter: blur(100px);
    }
`;

export const RightContainer = styled.div`
    position: relative;
    display: flex;
    width: 100%;
    @media (max-width: 1560px) {
        width: 100% !important;
    }
    
    @media (max-width: 600px) {
     display: none ;
    }
    
`;

export const RightMainContainer = styled.div`
    display: grid;
    position: relative;
    grid-template-columns: auto auto;
    padding: 55px;
    top: -55px;
    background-image: url(${BgiHeader});
    background-position: 10% 20%;
    background-size: 100%;
    background-attachment: initial;
    background-repeat: no-repeat;
`;

export const ImageBg = styled.img`
    z-index: 0;
    position: absolute;
    top: -100px;
`;


export const Image = styled.img`
    width: 100%;
`;

export const ClipContainer = styled.div`
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    overflow: hidden;
    line-height: 0;

`;