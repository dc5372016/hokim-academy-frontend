import React from 'react'
import {useNavigate} from 'react-router-dom'

import { Button, ButtonContainer, Card, CardContainer, CardDemo, CardText, CardTextCount, Container, Container1, ContentContainer, ContentImage, ContentText, ContentTitle, Div, FirstContainer, Icon, IconContainer, SecondContainer, SharImg, SubTitle, TextContainer, Title, UstunlikContainer } from './styles';
import CardICon1 from "../../assets/images/card icon/cardicon1.png";
import CardICon2 from "../../assets/images/card icon/cardicon2.png";
import CardICon3 from "../../assets/images/card icon/cardicon3.png";
import CardICon4 from "../../assets/images/card icon/cardicon4.png";
import Shar1 from "../../assets/images/card icon/shar1.png";
import Shar2 from "../../assets/images/card icon/shar2.png";
import KompImg from "../../assets/images/Komp.png";
const DarkAbout = () => {
    const navigate = useNavigate();

    return (
        <Container>
            <Container1>
            <FirstContainer>
                <Title>KURSLAR</Title>
                <SubTitle>15 dan ortiq soha va<br /> 140+ kurslar</SubTitle>
                <CardContainer>
                    <CardDemo data-aos="zoom-in-up" data-aos-duration="2000">
                        <SharImg src={Shar1} style={{ top: "0px", left: "25px" }} />
                        <Card>
                            <IconContainer>
                                <Icon src={CardICon1} />
                            </IconContainer>
                            <CardText>Dasturlash</CardText>
                            <CardTextCount>4 ta</CardTextCount>
                        </Card>
                        <SharImg src={Shar2} style={{ bottom: "0px", right: "38px" }} />
                    </CardDemo>
                    <CardDemo data-aos="zoom-in-up" data-aos-duration="2000">
                        <SharImg src={Shar1} style={{ top: "50px", left: "20px" }} />
                        <Card>
                            <IconContainer>
                                <Icon src={CardICon2} />
                            </IconContainer>
                            <CardText>Shaxsiy o’sish</CardText>
                            <CardTextCount>7 ta</CardTextCount>
                        </Card>
                        <SharImg src={Shar2} style={{ bottom: "10px", right: "50px" }} />
                    </CardDemo>
                    <CardDemo data-aos="zoom-in-up" data-aos-duration="2000">
                        <SharImg src={Shar1} style={{ bottom: "0px", left: "20px" }} />
                        <Card>
                            <IconContainer>
                                <Icon src={CardICon3} />
                            </IconContainer>
                            <CardText>Marketing</CardText>
                            <CardTextCount>8ta</CardTextCount>
                        </Card>
                        <SharImg src={Shar2} style={{ top: "0px", right: "50px" }} />
                    </CardDemo>
                    <CardDemo data-aos="zoom-in-up" data-aos-duration="2000">
                        <SharImg src={Shar1} style={{ bottom: "20px", left: "10px" }} />
                        <Card>
                            <IconContainer>
                                <Icon src={CardICon4} />
                            </IconContainer>
                            <CardText>Ta’lim va ilmiy fanlar</CardText>
                            <CardTextCount>12 ta</CardTextCount>
                        </Card>
                        <SharImg src={Shar2} style={{ top: "100px", right: "50px" }} />
                    </CardDemo>

                </CardContainer>
                <ButtonContainer>
                    <Button onClick={()=>navigate('/courses')} data-aos="zoom-in-up" data-aos-duration="2000">Barchasi</Button>
                </ButtonContainer>
            </FirstContainer>
            <SecondContainer>
                <Title>USTUNLIK</Title>
                <SubTitle>Nega aynan Biz</SubTitle>
                <ContentContainer>
                    <Div className="br1">
                        <UstunlikContainer className='uz1' data-aos="zoom-in-up" data-aos-duration="2000">
                            <svg width="136" height="162" viewBox="0 0 136 162" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g filter="url(#filter0_d_1_25996)">
                                    <path d="M120.251 28.6917C110.482 21.5216 98.9157 17.2021 86.8381 16.2136C74.7606 15.2252 62.6455 17.6065 51.8407 23.0928C41.0358 28.5792 31.9647 36.9553 25.6364 47.2896C19.3081 57.6239 15.9707 69.5111 15.9955 81.629C16.0202 93.747 19.4061 105.62 25.7766 115.929C32.147 126.237 41.2522 134.576 52.0794 140.018C62.9065 145.461 75.0312 147.792 87.1046 146.755C99.178 145.717 110.727 141.35 120.467 134.14L112.026 122.738C104.396 128.386 95.3481 131.807 85.8897 132.62C76.4313 133.433 66.9327 131.606 58.4507 127.343C49.9686 123.079 42.8355 116.546 37.8449 108.471C32.8542 100.395 30.2017 91.0933 30.1823 81.6001C30.1629 72.1068 32.7775 62.7943 37.7351 54.6983C42.6927 46.6024 49.7991 40.0405 58.2637 35.7424C66.7283 31.4444 76.2193 29.5788 85.6809 30.3532C95.1426 31.1275 104.204 34.5115 111.857 40.1286L120.251 28.6917Z" fill="url(#paint0_linear_1_25996)" />
                                </g>
                                <defs>
                                    <filter id="filter0_d_1_25996" x="0.995117" y="0.9953" width="134.472" height="161" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
                                        <feFlood floodOpacity="0" result="BackgroundImageFix" />
                                        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha" />
                                        <feOffset />
                                        <feGaussianBlur stdDeviation="7.5" />
                                        <feComposite in2="hardAlpha" operator="out" />
                                        <feColorMatrix type="matrix" values="0 0 0 0 0.0352941 0 0 0 0 0.929412 0 0 0 0 0.960784 0 0 0 0.3 0" />
                                        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_1_25996" />
                                        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_1_25996" result="shape" />
                                    </filter>
                                    <linearGradient id="paint0_linear_1_25996" x1="75.1964" y1="16.2989" x2="87.7943" y2="146.692" gradientUnits="userSpaceOnUse">
                                        <stop stopColor="#03F5FF" />
                                        <stop offset="1" stopColor="#02B6EB" />
                                    </linearGradient>
                                </defs>
                            </svg>
                            <TextContainer>
                                <ContentTitle>Qulay vaqt</ContentTitle>
                                <ContentText>Platforma 24/7 soat <br />rejimida ishlaydi</ContentText>
                            </TextContainer>
                        </UstunlikContainer>
                    </Div>
                    <Div className="br2">
                        <UstunlikContainer className='uz2' data-aos="zoom-in-up" data-aos-duration="2000">
                            <svg width="136" height="162" viewBox="0 0 136 162" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g filter="url(#filter0_d_1_25996)">
                                    <path d="M120.251 28.6917C110.482 21.5216 98.9157 17.2021 86.8381 16.2136C74.7606 15.2252 62.6455 17.6065 51.8407 23.0928C41.0358 28.5792 31.9647 36.9553 25.6364 47.2896C19.3081 57.6239 15.9707 69.5111 15.9955 81.629C16.0202 93.747 19.4061 105.62 25.7766 115.929C32.147 126.237 41.2522 134.576 52.0794 140.018C62.9065 145.461 75.0312 147.792 87.1046 146.755C99.178 145.717 110.727 141.35 120.467 134.14L112.026 122.738C104.396 128.386 95.3481 131.807 85.8897 132.62C76.4313 133.433 66.9327 131.606 58.4507 127.343C49.9686 123.079 42.8355 116.546 37.8449 108.471C32.8542 100.395 30.2017 91.0933 30.1823 81.6001C30.1629 72.1068 32.7775 62.7943 37.7351 54.6983C42.6927 46.6024 49.7991 40.0405 58.2637 35.7424C66.7283 31.4444 76.2193 29.5788 85.6809 30.3532C95.1426 31.1275 104.204 34.5115 111.857 40.1286L120.251 28.6917Z" fill="url(#paint0_linear_1_25996)" />
                                </g>
                                <defs>
                                    <filter id="filter0_d_1_25996" x="0.995117" y="0.9953" width="134.472" height="161" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
                                        <feFlood floodOpacity="0" result="BackgroundImageFix" />
                                        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha" />
                                        <feOffset />
                                        <feGaussianBlur stdDeviation="7.5" />
                                        <feComposite in2="hardAlpha" operator="out" />
                                        <feColorMatrix type="matrix" values="0 0 0 0 0.0352941 0 0 0 0 0.929412 0 0 0 0 0.960784 0 0 0 0.3 0" />
                                        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_1_25996" />
                                        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_1_25996" result="shape" />
                                    </filter>
                                    <linearGradient id="paint0_linear_1_25996" x1="75.1964" y1="16.2989" x2="87.7943" y2="146.692" gradientUnits="userSpaceOnUse">
                                        <stop stopColor="#03F5FF" />
                                        <stop offset="1" stopColor="#02B6EB" />
                                    </linearGradient>
                                </defs>
                            </svg>
                            <TextContainer>
                                <ContentTitle>Mutaxassislar</ContentTitle>
                                <ContentText>Darslar mutaxassislar<br />tomonidan o’tiladi.</ContentText>
                            </TextContainer>
                        </UstunlikContainer>
                        <UstunlikContainer className='uz1' data-aos="zoom-in-up" data-aos-duration="2000" >
                            <svg width="136" height="162" viewBox="0 0 136 162" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g filter="url(#filter0_d_1_25996)">
                                    <path d="M120.251 28.6917C110.482 21.5216 98.9157 17.2021 86.8381 16.2136C74.7606 15.2252 62.6455 17.6065 51.8407 23.0928C41.0358 28.5792 31.9647 36.9553 25.6364 47.2896C19.3081 57.6239 15.9707 69.5111 15.9955 81.629C16.0202 93.747 19.4061 105.62 25.7766 115.929C32.147 126.237 41.2522 134.576 52.0794 140.018C62.9065 145.461 75.0312 147.792 87.1046 146.755C99.178 145.717 110.727 141.35 120.467 134.14L112.026 122.738C104.396 128.386 95.3481 131.807 85.8897 132.62C76.4313 133.433 66.9327 131.606 58.4507 127.343C49.9686 123.079 42.8355 116.546 37.8449 108.471C32.8542 100.395 30.2017 91.0933 30.1823 81.6001C30.1629 72.1068 32.7775 62.7943 37.7351 54.6983C42.6927 46.6024 49.7991 40.0405 58.2637 35.7424C66.7283 31.4444 76.2193 29.5788 85.6809 30.3532C95.1426 31.1275 104.204 34.5115 111.857 40.1286L120.251 28.6917Z" fill="url(#paint0_linear_1_25996)" />
                                </g>
                                <defs>
                                    <filter id="filter0_d_1_25996" x="0.995117" y="0.9953" width="134.472" height="161" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
                                        <feFlood floodOpacity="0" result="BackgroundImageFix" />
                                        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha" />
                                        <feOffset />
                                        <feGaussianBlur stdDeviation="7.5" />
                                        <feComposite in2="hardAlpha" operator="out" />
                                        <feColorMatrix type="matrix" values="0 0 0 0 0.0352941 0 0 0 0 0.929412 0 0 0 0 0.960784 0 0 0 0.3 0" />
                                        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_1_25996" />
                                        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_1_25996" result="shape" />
                                    </filter>
                                    <linearGradient id="paint0_linear_1_25996" x1="75.1964" y1="16.2989" x2="87.7943" y2="146.692" gradientUnits="userSpaceOnUse">
                                        <stop stopColor="#03F5FF" />
                                        <stop offset="1" stopColor="#02B6EB" />
                                    </linearGradient>
                                </defs>
                            </svg>
                            <TextContainer>
                                <ContentTitle>Aktual</ContentTitle>
                                <ContentText>Barcha aktual sohalar<br />bo’yicha darslar mavjud.</ContentText>
                            </TextContainer>
                        </UstunlikContainer>
                    </Div>
                    <Div className="br3">
                        <UstunlikContainer className='uz2' data-aos="zoom-in-up" data-aos-duration="2000">
                            <svg width="136" height="162" viewBox="0 0 136 162" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g filter="url(#filter0_d_1_25996)">
                                    <path d="M120.251 28.6917C110.482 21.5216 98.9157 17.2021 86.8381 16.2136C74.7606 15.2252 62.6455 17.6065 51.8407 23.0928C41.0358 28.5792 31.9647 36.9553 25.6364 47.2896C19.3081 57.6239 15.9707 69.5111 15.9955 81.629C16.0202 93.747 19.4061 105.62 25.7766 115.929C32.147 126.237 41.2522 134.576 52.0794 140.018C62.9065 145.461 75.0312 147.792 87.1046 146.755C99.178 145.717 110.727 141.35 120.467 134.14L112.026 122.738C104.396 128.386 95.3481 131.807 85.8897 132.62C76.4313 133.433 66.9327 131.606 58.4507 127.343C49.9686 123.079 42.8355 116.546 37.8449 108.471C32.8542 100.395 30.2017 91.0933 30.1823 81.6001C30.1629 72.1068 32.7775 62.7943 37.7351 54.6983C42.6927 46.6024 49.7991 40.0405 58.2637 35.7424C66.7283 31.4444 76.2193 29.5788 85.6809 30.3532C95.1426 31.1275 104.204 34.5115 111.857 40.1286L120.251 28.6917Z" fill="url(#paint0_linear_1_25996)" />
                                </g>
                                <defs>
                                    <filter id="filter0_d_1_25996" x="0.995117" y="0.9953" width="134.472" height="161" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
                                        <feFlood floodOpacity="0" result="BackgroundImageFix" />
                                        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha" />
                                        <feOffset />
                                        <feGaussianBlur stdDeviation="7.5" />
                                        <feComposite in2="hardAlpha" operator="out" />
                                        <feColorMatrix type="matrix" values="0 0 0 0 0.0352941 0 0 0 0 0.929412 0 0 0 0 0.960784 0 0 0 0.3 0" />
                                        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_1_25996" />
                                        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_1_25996" result="shape" />
                                    </filter>
                                    <linearGradient id="paint0_linear_1_25996" x1="75.1964" y1="16.2989" x2="87.7943" y2="146.692" gradientUnits="userSpaceOnUse">
                                        <stop stopColor="#03F5FF" />
                                        <stop offset="1" stopColor="#02B6EB" />
                                    </linearGradient>
                                </defs>
                            </svg>
                            <TextContainer>
                                <ContentTitle>Istalgan joy</ContentTitle>
                                <ContentText>Internet bo’lsa bas. Hokim <br />akademiyasi har yerda ishlaydi.</ContentText>
                            </TextContainer>
                        </UstunlikContainer>
                        <ContentImage src={KompImg} data-aos="zoom-in-up" data-aos-duration="2000" />
                        <UstunlikContainer className='uz1' data-aos="zoom-in-up" data-aos-duration="2000">
                            <svg width="136" height="162" viewBox="0 0 136 162" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g filter="url(#filter0_d_1_25996)">
                                    <path d="M120.251 28.6917C110.482 21.5216 98.9157 17.2021 86.8381 16.2136C74.7606 15.2252 62.6455 17.6065 51.8407 23.0928C41.0358 28.5792 31.9647 36.9553 25.6364 47.2896C19.3081 57.6239 15.9707 69.5111 15.9955 81.629C16.0202 93.747 19.4061 105.62 25.7766 115.929C32.147 126.237 41.2522 134.576 52.0794 140.018C62.9065 145.461 75.0312 147.792 87.1046 146.755C99.178 145.717 110.727 141.35 120.467 134.14L112.026 122.738C104.396 128.386 95.3481 131.807 85.8897 132.62C76.4313 133.433 66.9327 131.606 58.4507 127.343C49.9686 123.079 42.8355 116.546 37.8449 108.471C32.8542 100.395 30.2017 91.0933 30.1823 81.6001C30.1629 72.1068 32.7775 62.7943 37.7351 54.6983C42.6927 46.6024 49.7991 40.0405 58.2637 35.7424C66.7283 31.4444 76.2193 29.5788 85.6809 30.3532C95.1426 31.1275 104.204 34.5115 111.857 40.1286L120.251 28.6917Z" fill="url(#paint0_linear_1_25996)" />
                                </g>
                                <defs>
                                    <filter id="filter0_d_1_25996" x="0.995117" y="0.9953" width="134.472" height="161" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
                                        <feFlood floodOpacity="0" result="BackgroundImageFix" />
                                        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha" />
                                        <feOffset />
                                        <feGaussianBlur stdDeviation="7.5" />
                                        <feComposite in2="hardAlpha" operator="out" />
                                        <feColorMatrix type="matrix" values="0 0 0 0 0.0352941 0 0 0 0 0.929412 0 0 0 0 0.960784 0 0 0 0.3 0" />
                                        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_1_25996" />
                                        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_1_25996" result="shape" />
                                    </filter>
                                    <linearGradient id="paint0_linear_1_25996" x1="75.1964" y1="16.2989" x2="87.7943" y2="146.692" gradientUnits="userSpaceOnUse">
                                        <stop stopColor="#03F5FF" />
                                        <stop offset="1" stopColor="#02B6EB" />
                                    </linearGradient>
                                </defs>
                            </svg>
                            <TextContainer>
                                <ContentTitle>Oson</ContentTitle>
                                <ContentText>Samarali ilm olish uchun<br />barcha imkoniyatlar..</ContentText>
                            </TextContainer>
                        </UstunlikContainer>
                    </Div>

                </ContentContainer>
            </SecondContainer>
            </Container1>
        </Container >
    )
}

export default DarkAbout;