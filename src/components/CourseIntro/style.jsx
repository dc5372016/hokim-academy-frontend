import styled from 'styled-components';


export const Container = styled.div`
    width: 100%;
    height: auto;
    display: flex;
    justify-content: center;
    position: relative;
    height: 615px;
    margin-top: 30px;
    margin-bottom: 30px;
    @media (max-width:1300px) {
        height: 500px;
    }
    @media (max-width:1100px) {
        height: 400px;
    }
    @media (max-width:900px) {
        height: auto;
    }

`
export const Wrapper = styled.div`
    width: 97%;
    max-width: 1600px;
    display: flex;
    justify-content: space-between;
    @media (max-width:900px) {
        flex-direction: column;
        width: 100%;

    }
    @media (max-width:500px) {
        width: 100%;
    }
`
export const BgImg1 = styled.img`
    position: absolute;
    top: 20px;
    left: 0px;
    height: 205.00555162336997px;
    width: 142.2302533398619px;
    z-index:0;
        
`
export const BgImg2 = styled.img`
    position: absolute;
    right: 0px;
    bottom: 5px;
    height: 217.4408416748047px;
    width: 258.9996643066406px;
    z-index:0;

`
export const LeftWrapper = styled.div`
    width: 60%;
    display: flex;
    justify-content: flex-end;
    align-items: center;
    @media (max-width:900px) {
        width: 100%;
        height: 400px;
    }
    @media (max-width:500px) {
        justify-content: center;
    }

`
export const RightWrapper = styled.div`
    width: 37%;
    margin-left: 40px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    flex-direction: column;
    @media (max-width:900px) {
        width: 100%;
        margin-top: 40px;
        height: 400px;
        margin-left: 0px;

    }
    @media (max-width:500px) {
        margin-left: 0px;

    }
    
`
export const TextContainer = styled.div`
    box-shadow: 0px 0px 25px 0px rgba(3, 245, 255, 0.55);
    height: 435px;
    width: 550px;
    border-radius: 5px;
    background-color: white;
    padding: 50px;
    box-sizing: border-box;
    display: flex;
    justify-content: center;
    align-items: center;
    text-align: center;
    margin-left: -200px;
    font-family: Lexend;
    font-size: 26px;
    font-weight: 300;
    line-height: 35px;
    letter-spacing: 0em;
    text-align: center;
    z-index:999;
    @media (max-width:1300px) {
        height: 395px;
        width: 500px;
        font-size: 23px;
    }
    @media (max-width:1100px) {
        height: 305px;
        width: 400px;
        font-size: 16px;
        margin-left: -140px;

    }
    @media (max-width:900px) {
        height: 255px;
        width: 350px;
        font-size: 14px;
        margin-left: -100px;
    }
    @media (max-width:500px) {
        margin-left: 0px;
        width: 100%;
        height: 50%;
    }


`
export const ImgContainer = styled.div`
    width: 75%;
    height: 100%;
    display: flex;
    justify-content: flex-start;
    align-items: center;
    background-image: url(${props => props.bg});
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center center;
    @media (max-width:500px) {
        width: 100%;
        align-items: flex-end;

    }
`
export const ReusableDiv = styled.div`
    width: 100%;
    height: 30.9%;
    background-image: url(${props => props.bg});
    background-position: center top;
    background-size: cover;
`
export const InnerDiv = styled.div`
    width: 100%;
    height: 100%;
    background:  rgba(0,0,0,0.5);
    display: flex;
    justify-content: center;
    align-items: center;
    position: relative;
    backdrop-filter: blur(3px);
    transition: .3s;
    &:hover {
        box-shadow: 0px 0px 10px 5px #cad8ff ;
        backdrop-filter: blur(0px);
        cursor: pointer;
        background:  rgba(0,0,0,0.1);
    }

`
export const IconImg = styled.img`
    position: absolute;
    top: 20px;
    right: 20px;
    @media (max-width:1300px) {
        top: 5px;
        right: 5px;
        width:60px ;
    }
    @media (max-width:1100px) {
        top: 5px;
        right: 5px;
        width:40px ;
    }
`
export const Text = styled.p`
    font-family: Lexend;
    font-size: 46px;
    font-weight: 400;
    line-height: 63px;
    letter-spacing: 0em;
    text-align: center;
    color: #ffffff;
    @media (max-width:1300px) {
        font-size: 32px;
    }
    @media (max-width:1100px) {
        font-size: 28px;
    }
    @media (max-width:900px) {
        font-size: 22px;
    }
`