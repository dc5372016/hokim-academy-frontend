import styled from 'styled-components';




export const LessonCount = styled.span`
    background: linear-gradient(180deg, #03F5FF 0%, #02B6EB 100%);
    border-radius: 7px;
    padding: 7px;
    display: flex;
    align-items: center;
    justify-content: center;
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 500;
    font-size: 10px;
    line-height: 10px;
    position: absolute;
    top: 10px;
    right: 10px;
    /* identical to box height, or 100% */

    text-align: center;
    letter-spacing: 0.5px;

    color: #FFFFFF;
`;

export const Image = styled.img`
    border-top-left-radius: 20px;
    border-top-right-radius: 20px;
    width: 100%;
    height: 48%;
    object-fit: cover;
`;
export const Image1 = styled.img`
    width: 46px;
    height:46px;
    border-radius: 100%;
    object-fit: cover;
`;

export const Content = styled.div`
    padding: 15px 27px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    height: 52%;
    position: relative;
    box-sizing: border-box;
`;

export const AuthorContainer = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
`;

export const AvatarContainer = styled.div`
    width: 50px;
    height: 50px;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 3px;
    background-color: transparent;
    border-radius: 28px;
    position: relative;
    border: 1px solid white;
    position: absolute;
    top: -20px;
    right: 10px;
`;

export const Name = styled.span`
   font-family: 'Poppins';
    font-style: normal;
    font-weight: 400;
    font-size: 11px;
    line-height: 13px;
    margin-bottom: 15px;
    /* identical to box height, or 100% */

    letter-spacing: 0.5px;

    color: #B7B9D2;

    mix-blend-mode: normal; 
`;

export const Category = styled.span`
font-family: 'Poppins';
font-style: normal;
font-weight: 500;
font-size: 16px;
line-height: 26px;

letter-spacing: 0.3px;

color: #FFFFFF;
`;

export const Text = styled.span`
font-family: 'Poppins';
font-style: normal;
font-weight: 400;
font-size: 12px;
line-height: 12px;
/* identical to box height, or 100% */

letter-spacing: 0.5px;

color: #808191;

mix-blend-mode: normal;
`;

export const Button = styled.button`
    background: linear-gradient(180deg, #03F5FF 0%, #02B6EB 100%);
    box-shadow: 0px 4px 8px rgba(93, 95, 239, 0.2);
    border-radius: 10px;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 205px;
    height: 38px;
    border: none;
    cursor: pointer;
    position: relative;
    font-family: 'Lexend';
    font-style: normal;
    font-weight: 400;
    font-size: 15px;
    line-height: 12px;
    /* identical to box height, or 80% */

    text-align: center;
    letter-spacing: 0.5px;

    color: #FFFFFF;
`;

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    background: #252836;
    border-radius: 20px;
    width: 259px;
    height: 343px;
    position: relative;
    @media (max-width: 1200px){
        height: 300px;
        width: 220px;
        ${Button}{
            width: 160px;
            height: 30px; 
        }
    }
    @media (max-width: 860px){
        height: 343px;
        width: 260px;
        margin: auto;
        ${Button}{
            width: 205px;
            height: 38px;
        }
    }
`;