import styled from 'styled-components';
import BG from '../../../assets/images/kurslarbg.svg';

export const Container = styled.div`
    width: 100%;
    height: 270px;
    /* background-image: url(${BG}); */
    background-size:cover;
    max-width: 1900px;
    margin: auto;
    background-position: center center;
    display: flex;
    justify-content: center;
    align-items: center;
    /* margin-top: 45px; */
    @media (max-width:1025px){
        height: 100px;
    }
`
export const Button = styled.button`
    height: 60px;
    width: 233px;
    border-radius: 5.213675498962402px;
    background: rgba(3, 186, 237, 1);
    font-family: 'Lexend';
    font-size: 42px;
    font-weight: 500;
    line-height: 52px;
    letter-spacing: 0em;
    text-align: center;
    color: white;
    border: none;
`