import React, { useEffect, useState } from "react";
import { Routes, Route, useLocation } from "react-router-dom";
import AOS from "aos";
import "aos/dist/aos.css";
import "./style/css/_import.css"

//views
import Navbar from "./views/Navbar";
import Footer from "./views/Footer";

//pages 
import HomePage from "./pages/homepage/Homepage";
import Kurslar from "./pages/coursespage/Kurslarpage";
// import AboutUsPage from './pages/aboutuspage/AboutUsPage';
// import ContactPage from './pages/ContactPage/ContactPage';
import KirishPage from './pages/KirishPage/KirishPage';
import RegisterPage from './pages/RegisterPage/RegisterPage';
import InfoCourse from "./pages/info-courses/InfoCourse";
import NoMatch from "./views/NoMatch"
import MyCourses from "./pages/myCourses/MyCourses";
import Taxrirlash from "./components/Profil_singles/Taxrirlash"
import Testlar from "./components/Profil_singles/Testlar";
import Dashboard from "./pages/Dashboard/Dashboard";
import { useDispatch } from "react-redux";
import AllCourses from "./components/AllCourses/AllCourses";
import Results from "./pages/Quiz_Result/Results";


AOS.init();

const App = () => {
  const location = useLocation();
  const dispatch = useDispatch();
  const [bigRender, setbigRender] = useState(false)
  let [auth_data, setAuth_data] = useState(undefined);

  useEffect(() => {
    auth_data = JSON.parse(localStorage.getItem('access_key')) !==null ? JSON.parse(localStorage.getItem('access_key')) : undefined
    setAuth_data(JSON.parse(localStorage.getItem('access_key')) !==null ? JSON.parse(localStorage.getItem('access_key')) : undefined)
    // dispatch({type:'AUTH_SUCCESS',payload:{token:auth_data.token !==undefined ? auth_data.token : null ,user:auth_data !==undefined ? auth_data.user : {}}})
  }, [bigRender,setbigRender])
  
  return (
    <>
      <React.Fragment>
        <Navbar auth_data={auth_data}/>
        <Routes>
          <Route path="/"         element={<HomePage />}/>
          <Route path="/courses"  element={<Kurslar />}/>
          {/* <Route path="/aboutus"  element={<AboutUsPage />}/>
          <Route path="/contact"  element={<ContactPage />}/> */}
          <Route path="/login"    element={<KirishPage />}/>
          <Route path="/results"    element={<Results />}/>
          <Route path="/register" element={<RegisterPage setbigRender={setbigRender} bigRender={bigRender}/>}/>
          <Route path="/course/:course_id" element={<InfoCourse />}/>
          <Route path="*" element={<NoMatch />} />
          <Route path="/taxtitlash" element={<Taxrirlash />}/>
          <Route path="/dashboard" element={<Dashboard />}>
            <Route path="allcourses" element={<AllCourses />}/>
            <Route path="mycourses" element={<MyCourses />}/>
          </Route>
            <Route path="/tests" element={<Testlar />}/>

        </Routes>
        {location.pathname === '/dashboard/allcourses' ?
        ''  : location.pathname === '/dashboard/mycourses' ?
        '' :  <Footer />}
      </React.Fragment>
    </>
  );
};

export default App;
