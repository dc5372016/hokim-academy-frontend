const initialState = {
    token: '',
    user: {}
}

const authReducer = (state = initialState, action) => {
    switch(action.type) {
        case 'AUTH_SUCCESS':
            const { token, user,  } = action.payload;
            return { ...state, token, user}
        case 'LOGOUT_SUCCESS':
            return initialState;
        default:
            return state;
    }
}

export default authReducer;