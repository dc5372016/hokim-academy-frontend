import React from "react";
import { Skeleton } from "react-skeleton-generator";
import { AllCoursesContainer, Container1 } from "./style";

const Skeleton1 = () => {
  return (
    <Skeleton.SkeletonThemeProvider animationDuration={2}>
        <Container1   style={{height:239,backgroundColor:'rgba(0,0,0,0)'}}>
            <Skeleton
                borderRadius="20px 20px 0px 0px"
                
                count={1}
                widthMultiple={["180px"]}
                heightMultiple={["120px"]}
            />
            <div
                style={{
                    width: '100%',
                    display: 'flex',
                    justifyContent:'space-between',
                    alignItems:'center'
                }}
            >
                <div style={{width:'70%',display:'flex',flexDirection:'column',justifyContent:'space-evenly',alignItems:'flex-start'}}>
                    <Skeleton width="90%" height="14px" borderRadius="10px" />
                     <Skeleton width="90%" height="15px" borderRadius="10px" />
                </div>
                <div >

                     <Skeleton width="50px" height="50px" borderRadius="50%" />
                </div>
            </div>
            <div style={{marginTop:20}}>
                <Skeleton width="100%" height="30px" borderRadius="10px" />
            </div>
        </Container1>
    </Skeleton.SkeletonThemeProvider>
  );
};

export default Skeleton1;
