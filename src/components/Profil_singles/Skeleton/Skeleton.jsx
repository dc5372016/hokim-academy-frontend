import React from "react";
import { Skeleton } from "react-skeleton-generator";

const Skeleton1 = () => {
  return (
    <Skeleton.SkeletonThemeProvider animationDuration={2}>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          marginTop:25
        }}
      >
        <Skeleton width="50px" height="50px" borderRadius="50%" />

        <div style={{ width: "95%", display: "flex", alignItems: "center" ,marginTop:-10}}>
          <Skeleton
            borderRadius="5px"
            count={1}
            widthMultiple={["100%"]}
            heightMultiple={["50px"]}
          />
        </div>
      </div>

      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          marginTop: "30px",
          flexWrap: "wrap",
        }}
      >
        <Skeleton width="48%" height="20px" borderRadius="30px" />

        <Skeleton width="48%" height="20px" borderRadius="30px" />
        <Skeleton width="48%" height="20px" borderRadius="30px" />

        <Skeleton width="48%" height="20px" borderRadius="30px" />
      </div>
    </Skeleton.SkeletonThemeProvider>
  );
};

export default Skeleton1;
