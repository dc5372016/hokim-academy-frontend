import React, { useEffect, useState,useCallback } from "react";
import axios from "axios";
// import Images
import arrow from "../../assets/icons/profil_arrow.svg";
import user from "../../assets/icons/user.png";
import {  useNavigate } from "react-router-dom";
import Profile_Img from "../../assets/images/prfile_im.png";
import { AvatarYuklang, BackDiv, BackImg, BackText, BacktoDashboard, ButtonContainer, CancelButton, Container, Div, EditImgDiv, FileYuklang, Form, ImgLabel, ImgPreview, Input, InputContainer, LabelForInp, MainImg, NextButton, ProfileHeader, ProfileImageEdit, SelectView, SubmitContainer, TestContainer, TestContainer2, Wrapper } from "./style";
import {options,TypesSelect} from './data'
import { Button } from "@mui/material";
import Swal from "sweetalert2";
import { baseURL } from "../../api/baseUrl";

const Taxrirlash = () => {

  const [open, setOpen] = useState(false);
  let Auth_Data =
    JSON.parse(localStorage.getItem("access_key")) !== null
      ? JSON.parse(localStorage.getItem("access_key"))
      : { user: { img: Profile_Img, full_name: "Unknown" } };

  const [selected, setSelected] = useState(
    Auth_Data.user.gender === null ? "Erkak" : Auth_Data.user.gender
  );
  const [login, setlogin] = useState(Auth_Data.user.email);
  const [render, setrender] = useState(false);
  const [fullname, setfullname] = useState(Auth_Data.user.full_name);
  const [password, setpassword] = useState("");
  const [passwordnew, setpasswordnew] = useState("");
  const [imgview, setimgview] = useState(Auth_Data.user.image);
  const [date, setdate] = useState(Auth_Data.user.birthday);
  const [tel, settel] = useState(Auth_Data.user.phone);
  const [img, setimg] = useState(
    Auth_Data.user.image === null
      ? Profile_Img
      : baseURL+Auth_Data.user.image
  );
  const [loading, setloading] = useState(false);
  const [types, settypes] = useState('');
  const navigate = useNavigate()
  const [workplace, setworkplace] = useState('');
  const [currentjob, setcurrentjob] = useState('');
  const [grade, setgrade] = useState('');

  const [viloyalarOption, setviloyalarOption] = useState([]);
  const [viloyat, setviloyat] = useState('');

  const [tumanlarOption, settumanlarOption] = useState([]);
  const [tuman, settuman] = useState({value:1,label:'Tumaningizni tanlang'});

  const [schoolsOption, setschoolsOption] = useState([]);
  const [school, setschool] = useState('');

  const [isImg, setisImg] = useState(false);

  //yangilash btn
  const [editbtn, seteditbtn] = useState('Yangilash');


  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: "auto"
    })
  }, []);

  
  useEffect(() => {
    try {
     axios.get(`${baseURL}/api/region/`)
     .then(res=>{
       console.log("regions",res);
       let viloyatlar = []
       res.data.data.filter((item)=>{
          viloyatlar.push({value:item.id,label:item.name})
       })
       setviloyalarOption(viloyatlar)
     })
    } catch (err) {
     
    }
 }, []);


 useEffect(() => {
  try {
   axios.get(`${baseURL}/api/school/`,{
    headers:{
      Authorization: `token ${Auth_Data.token}`,
    }
   })
   .then(res=>{
     console.log("schools",res);
     let schools = []
     res.data.data.filter((item)=>{
        schools.push({value:item.id,label:item.name})
     })
     setschoolsOption(schools)
   })
  } catch (err) {
   
  }
}, []);
 
  useEffect(() => {
    try {
    axios.get(`${baseURL}/api/city/`)
    .then(res=>{
      let filtered = res.data.data.filter((item)=>{
        return item.region === viloyat.value
      })
      console.log("cities",filtered);
      let tumanlar = []
      filtered.filter((item)=>{
            tumanlar.push({value:item.id,label:item.name})
      })
      settumanlarOption(tumanlar)
      
    })
    } catch (err) {
    
    }
  }, [viloyat,setviloyat]);




  
  console.log("Auth - Edit", Auth_Data);








  const SetImage = async () => {
    let formData = new FormData();
    formData.append("image", imgview);
    var config = {
      method: "post",
      url: `${baseURL}/api/account/set-image/${Auth_Data.user.id}/`,
      headers: {
        Authorization: `token ${Auth_Data.token}`,
        "Content-Type": "application/json",
      },
      data: formData,
    };
    try {
      const res = await axios(config);
      if (res) {
        console.log("api", res);
        localStorage.setItem(
          "access_key",
          JSON.stringify({ token: Auth_Data.token, user: res.data.data })
        );
        setimg(baseURL+res.data.data.image);
        console.log("imgg", res.data.data.image);
        setloading(false);
        setisImg(false)
      }
    } catch (err) {
      console.log("img error");
    }
  };








  let Data = {
    phone: tel,
    full_name: fullname,
    city: tuman.value,
    birthday: date,
    type: types.value,
    school: school.value,
    grade: grade,
    email: login,
    work_place: workplace,
    job: currentjob,
    gender: selected.value,
    is_active: true,
  };



  const SetAccount = async () => {
    seteditbtn('Yangilanmoqda...')
    let settings = {
      method: "post",
      url: `${baseURL}/api/account/edit/${Auth_Data.user.id}/`,
      headers: {
        Authorization: `token ${Auth_Data.token}`,
        "Content-Type": "application/json",
      },
      data: Data,
    };
    try {
      const res = await axios(settings);
      if (res) {
        console.log("Edited ", res);
        seteditbtn('Yangilash')
        if(!res.data.success){
          Swal.fire({
            icon: 'error',
            title: 'Error!',
            text: res.data.error,
            confirmButtonText:'Yangilash'
          })
        }
        if(res.data.success){
          Swal.fire({
            icon: 'success',
            title: 'Success!',
            text: 'Profilingiz muvaffaqiyatli yangilandi!',
            confirmButtonText:'Dashboardga qaytish'
          }).then((res)=>{
            
            navigate('/dashboard/allcourses')
            window.location.reload(true)
            console.log('====================================');
            console.log('taxrir',res.data.data);
            console.log('====================================');
            localStorage.setItem('access_key',JSON.stringify({token:Auth_Data.token,user:res.data.data}))
          })
        }
        // console.log('account',res.data.data);
        setloading(false);
      }
    } catch (err) {
      console.log("account err");
    }
  };






  const SetPassword = async () => {
    try {

    } catch (err) {
      console.log("password err");
    }
  };








  const [ip, setIP] = useState("");

  const getData = async () => {
    const res = await axios.get("https://geolocation-db.com/json/");
    console.log("IP", res.data);
    setIP(res.data.IPv4);
  };

  useEffect(() => {
    getData();
  }, []);

  const SettingImg = (e) => {
    setimgview(e);
    setloading(true);
    setisImg(true)

    setTimeout(() => {
      setloading(false);
      setimg(URL.createObjectURL(e));
    }, 1000);
  };

  const UpdateAll = async () => {
    try {
      if(isImg){
        SetImage();
      }
      SetAccount();
    } catch (err) {}
  };




  
  return (
    <React.Fragment>
        <Container>
          <Wrapper>
            <BacktoDashboard>
              <BackDiv onClick={()=>navigate('/dashboard/allcourses')}>
                <BackImg src={arrow} />
                <BackText>Kabinetga qaytish</BackText>
              </BackDiv>
            </BacktoDashboard>
            <TestContainer2>
              <ProfileHeader>
                 Profil malumotlarini tahrirlash
              </ProfileHeader>
              <ProfileImageEdit>
                  <EditImgDiv>
                    <ImgPreview>
                      {loading ? "Loading ..." : <MainImg src={img} alt="" />}
                    </ImgPreview>
                    <ImgLabel for="img_upload">
                      <FileYuklang>Fayl yuklang ...</FileYuklang>
                      <input
                        type="file"
                        onChange={(e) => SettingImg(e.target.files[0])}
                        name=""
                        hidden
                        id="img_upload"
                      />
                      <AvatarYuklang >Avatar yuklang</AvatarYuklang>
                    </ImgLabel>
                  </EditImgDiv>

              </ProfileImageEdit>
              <Form>
                  <InputContainer>
                    <LabelForInp>Telefon Raqam</LabelForInp>
                    <Input
                      value={tel}
                      onChange={(e)=>settel(e.target.value)}
                      placeholder="Telefon raqamingiz" 
                     />
                  </InputContainer>

                  <InputContainer>
                    <LabelForInp>F.I.Sh</LabelForInp>
                    <Input 
                      value={fullname}
                      onChange={(e)=>setfullname(e.target.value)}
                      placeholder="Ism familya" 
                    />
                  </InputContainer>

                  <InputContainer>
                    <LabelForInp>Email</LabelForInp>
                    <Input
                      type={'email'}
                      value={login}
                      onChange={(e)=>setlogin(e.target.value)}
                      placeholder="Elektron pochta"
                    />
                  </InputContainer>

                  <InputContainer>
                    <LabelForInp>Tugʻilgan sana</LabelForInp>
                    <Input
                      value={date}
                      onChange={(e)=>setdate(e.target.value)}
                      type={'date'} 
                      placeholder="Tugʻilgan sana"
                    />
                  </InputContainer>

                  <InputContainer>
                    <LabelForInp>Jinsi</LabelForInp>
                    <SelectView
                      placeholder="Jinsi"
                      defaultValue={selected}
                      onChange={setSelected}
                      options={options}
                    />
                  </InputContainer>

                  <InputContainer>
                    <LabelForInp>Ish yoki o'qish joyingiz</LabelForInp>
                    <Input 
                     value={workplace}
                     onChange={(e)=>setworkplace(e.target.value)}
                     placeholder="Ish yoki o'qish joyi nomi" />
                  </InputContainer>

                  <InputContainer>
                    <LabelForInp>Shaxringiz</LabelForInp>
                    <SelectView
                      placeholder="Shaxar"
                      defaultValue={viloyat}
                      onChange={setviloyat}
                      options={viloyalarOption}
                    />
                  </InputContainer>

                  <InputContainer>
                    <LabelForInp>O'qish kursingiz yoki maktab sinfi </LabelForInp>
                    <Input
                     value={grade}
                     onChange={(e)=>setgrade(e.target.value)}
                     placeholder="1-kurs" />
                  </InputContainer>

                  <InputContainer>
                    <LabelForInp>Tumaningiz</LabelForInp>
                    <SelectView
                      placeholder="Tuman"
                      defaultValue={tuman}
                      onChange={settuman}
                      options={tumanlarOption}
                    />
                  </InputContainer>

                  <InputContainer>
                    <LabelForInp>Kasbingiz</LabelForInp>
                    <Input
                      value={currentjob}
                      onChange={(e)=>setcurrentjob(e.target.value)}
                      placeholder="Nima ish qilishingiz"
                    />
                  </InputContainer>

                  <InputContainer>
                    <LabelForInp>Types</LabelForInp>
                    <SelectView
                      placeholder="Types"
                      defaultValue={types}
                      onChange={settypes}
                      options={TypesSelect}
                    />
                  </InputContainer>

                  <InputContainer>
                    <LabelForInp>Maktabingiz (Agar o'quvchi bo'lsangiz)</LabelForInp>
                    <SelectView
                      placeholder="Maktabingizni tanglang"
                      defaultValue={school}
                      onChange={setschool}
                      options={schoolsOption}
                    />
                  </InputContainer>
              </Form>
              <SubmitContainer>
                <CancelButton onClick={()=>navigate(-1)}>Bekor qilish</CancelButton>
                <NextButton onClick={UpdateAll}>{editbtn}</NextButton>
              </SubmitContainer>
            </TestContainer2>
          </Wrapper>
        </Container>
      </React.Fragment>
  );
};

export default Taxrirlash;
