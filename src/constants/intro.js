import Intro1 from '../assets/images/intro1.webp';
import Intro2 from '../assets/images/intro2.webp';
import Intro3 from '../assets/images/intro3.webp';
import Icon1 from '../assets/images/introicon1.png';
import Icon2 from '../assets/images/introicon2.png';
import Icon3 from '../assets/images/introicon3.png';

export const IntroData = [
    {
        id:1,
        title:'Har qanday joyda, hamma uchun jahon darajasidagi ta`lim. 100% bepul. Oʻqiyotgan narsangiz boʻyicha shaxsiy yordam olish yoki mutlaqo yangi narsalarni oʻrganish uchun Bizga qoʻshiling. Biz barcha yutuqlaringizni saqlaymiz3.',
        img:Intro1,
        icon:Icon1,
        category:'Ota onalarga',
    },
    {
        id:2,
        title:'Har qanday joyda, hamma uchun jahon darajasidagi ta`lim. 100% bepul. Oʻqiyotgan narsangiz boʻyicha shaxsiy yordam olish yoki mutlaqo yangi narsalarni oʻrganish uchun Bizga qoʻshiling. Biz barcha yutuqlaringizni saqlaymiz2.',
        img:Intro2,
        icon:Icon2,
        category:'O’qituvchilarga',
    },
    {
        id:3,
        title:'Har qanday joyda, hamma uchun jahon darajasidagi ta`lim. 100% bepul. Oʻqiyotgan narsangiz boʻyicha shaxsiy yordam olish yoki mutlaqo yangi narsalarni oʻrganish uchun Bizga qoʻshiling. Biz barcha yutuqlaringizni saqlaymiz1.',
        img:Intro3,
        icon:Icon3,
        category:'O’quvchilarga',
    },
]