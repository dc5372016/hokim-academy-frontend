import styled from 'styled-components';



export const Container = styled.div`
    width: 100%;
    box-sizing: border-box;
    display: flex;
    justify-content: center;
    align-items: flex-start;
    background-image: url(${props=>props.bg});
    background-repeat: no-repeat;
    background-size: contain;
    background-position: center center;

    
`
export const Wrapper = styled.div`
    width: 100%;
    box-sizing: border-box;
    max-width: 1400px;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: flex-start;
`

export const RegisterContainer = styled.div`
    max-width: 800px;
    width: 100%;
    margin-top: 170px;
    @media (max-width:500px){
        margin-top: 100px;
    }
`

export const CategoryContainer = styled.div`
    width: 100%;
    height: 100px;
    display: flex;
    justify-content: center;
    align-items: center;
    @media (max-width:650px){
        box-sizing: border-box;
    }
`
export const CategoryButton = styled.button`
    height: 73px;
    width: 183px;
    border-radius: 20px;
    padding: 21px, 32px, 21px, 32px;
    font-family: 'Lexend';
    font-size: 25px;
    font-weight: 400;
    line-height: 31px;
    letter-spacing: 0em;
    border: none;
    text-align: center;
    color: ${props=>props.bg ? 'white' : 'rgba(4, 189, 238, 1)'};
    margin: 14px;
    box-shadow: 0px 0px 25px 0px rgba(163, 203, 255, 0.35);
    box-sizing: border-box;
    &:hover{
        box-shadow: 0px 0px 15px 2px rgba(163, 203, 255, 1);
        cursor: pointer;
    }
    background: ${props=>props.bg ?
     'linear-gradient(180deg, #04F5FF 0%, #03B9EC 100%)' : 
     'white'};
    @media (max-width:650px){
        height: 63px;
        width: 163px;
        font-size: 20px;
    }
    @media (max-width:500px){
        height: 43px;
        width: 123px;
        font-size: 15px;
        margin: 5px;
    }
    @media (max-width:400px){
        height: 38px;
        width: 95px;
        font-size: 15px;
        margin: 4px;
    }

`
export const Title = styled.div`
    width: 100%;
    height: 70px;
    font-family: 'Lexend';
    font-size: 30px;
    font-weight: 400;
    line-height: 45px;
    letter-spacing: 0em;
    text-align: center;
    display: flex;
    justify-content: center;
    align-items: center;
    @media (max-width:500px){
        font-size: 23px;
    }
`