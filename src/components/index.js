export { default as Header } from './Header/Header';
export { default as Platform } from './Platform/Platform';
export { default as Widget } from './Card/Card';
export { default as Comments } from './Comments/Comments';
export { default as HomeCourse } from './HomeCourse/HomeCourse';
export { default as DarkAbout } from './DarkAbout/DarkAbout';