import styled from 'styled-components'
export const CardContainer = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    grid-gap: 40px;
    top: -150px;
    position: absolute;
    
`;

export const Container = styled.div`
    display: flex;
    position: relative;
    background-color: #F7FEFF;
    height: 100px;
    @media (max-width: 776px){
        height: 600px;
        
        ${CardContainer}{
            flex-direction: column; 
        }
    }
`;




export const Card = styled.div`
    background: #FFFFFF;
    box-shadow: 0px 8px 15px rgba(93, 95, 239, 0.2);
    border-radius: 20px;
    width: 213px;
    height: 213px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    transition: .3s;
    &:hover{
        cursor: pointer;
        box-shadow: 0px 0px 15px 4px rgba(93, 95, 239, 0.5);
    }
    &:active{
        cursor: pointer;
        background-color: aqua;
    }
`;

export const CardTitle = styled.span`
    /* font-family: 'Lexend'; */
    font-style: normal;
    font-weight: 400;
    font-size: 22px;
    line-height: 28px;
    text-align: center;
    color: #333333;
`;

export const IconContainer = styled.div`
    width: 105px;
    height: 105px;
    background: rgba(93, 95, 239, 0.1);
    border-radius: 52px;
    display: flex;
    justify-content: center;
    align-items: center;
`;

export const Icon = styled.img``;