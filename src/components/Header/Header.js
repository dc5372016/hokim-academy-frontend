import React from 'react';
import { useState } from 'react';
import Atropos from 'atropos/react';
import {useNavigate} from 'react-router-dom'
import ParticleEffectButton from 'react-particle-effect-button'
import { Button, ClipContainer, Container, Container1, Image, ImageBg, LeftContainer, LeftGradiant, LightTitle, MainContainer, RightContainer, RightGradiant, RightMainContainer, SvgContainer, Text, Title } from './styles'
import Header1 from '../../assets/images/header_img1.png'
import Header2 from '../../assets/images/header_img2.png'
import Header3 from '../../assets/images/header_img3.png'
import Header4 from '../../assets/images/header_img4.png'
const Header = () => {
    const navigate = useNavigate();
    const [state, setstate] = useState({
        hidden:false
    });
    return (
        <Container1>
        <LeftGradiant />
        <Container>
            <MainContainer>
                <LeftContainer data-aos="fade-right" data-aos-duration="2000">
                    <Title 
                        data-aos-anchor-placement="bottom-center">
                        <LightTitle onClick={()=>setstate({hidden:true})} >Hokim</LightTitle>
                    
                        
                         Akademiyasi<br />
                        bilimlar makoni!
                    </Title>
                    <Title style={{ fontSize: "36px", lineHeight: '50px', marginBottom: '20px' }} data-aos="fade-up"
                        data-aos-anchor-placement="fade-left">
                        O'qituvchilar va talabalar uchun <span style={{ color: '#02F5FF' }}>onlayn platforma</span>
                    </Title>
                    <Text>
                        Onlayn kurslarda siz ingliz tilini o'rganishingiz, programmist, dizayner, sotuvch menejeri bo’lishiz mumkin. Bularning barchasi uydan chiqmagan holda va hatto bepul. 2022-da eng ko'p talabgor kasblar bo'yicha eng yaxshi kurslarni o’rganishiz mumkin.
                    </Text>
                    <Button onClick={()=>navigate('/courses')}>Batafsil</Button>
                </LeftContainer>
                <RightContainer data-aos="fade-up" data-aos-duration="2000" >
                    {/* <ImageBg src={BgiHeader} /> */}
                    <RightMainContainer>
                        <Atropos
                            className="atropos-banner"
                            highlight={true}
                            shadow={false}
                        >
                            <Image src={Header1} data-aos="flip-left" data-aos-duration="2000" />
                        </Atropos>
                        <Atropos
                            className="atropos-banner"
                            highlight={true}
                            shadow={false}
                        >
                            <Image src={Header2} data-aos="flip-left" data-aos-duration="2000" />
                        </Atropos>
                        <Atropos
                            className="atropos-banner"
                            highlight={true}
                            shadow={false}
                        >
                            <Image src={Header3} data-aos="flip-left" data-aos-duration="2000" />
                        </Atropos>
                        <Atropos
                            className="atropos-banner"
                            highlight={true}
                            shadow={false}
                        >
                            <Image src={Header4} data-aos="flip-left" data-aos-duration="2000" />
                        </Atropos>
                    </RightMainContainer>
                </RightContainer>
            </MainContainer>
            <SvgContainer>
                <svg width="1920" height="200" viewBox="0 0 1920 200" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fillRule="evenodd" clipRule="evenodd" d="M25.7289 179.278C92.5668 151.405 345.811 45.7968 557 29C819.846 8.09471 974.139 54.7049 1192.93 120.8C1227.61 131.275 1263.9 142.238 1302.5 153.5C1572.03 232.134 1891.85 99.8843 1920 87.8638V870H0V207.978C0.224911 208.441 0.375153 208.698 0.375153 208.698V192.156C2.15652 189.692 5.12546 187.422 9.87578 185.752C11.9789 185.012 17.4101 182.747 25.7289 179.278ZM0.375153 0L0.375153 192.156C0.243716 192.338 0.118745 192.521 0 192.705V0H0.375153Z" fill="#F6FEFF" />
                </svg>
            </SvgContainer>
        </Container>
        <RightGradiant />
        </Container1>
    )
}

export default Header;