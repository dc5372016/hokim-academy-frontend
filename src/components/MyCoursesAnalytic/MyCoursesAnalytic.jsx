import React from "react";
import { useState } from "react";
import { CircularProgressbar } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";

import ParticleEffectButton from "react-particle-effect-button";
function MyCoursesAnalytic(props) {
  const [state, setstate] = useState(false);

  function Complete(params) {
    setTimeout(() => {
      setstate(false)
    }, 500);
  }
  
  return (
    <div className="my-courses-analytic" data-aos="fade-up" >
        <ParticleEffectButton 
        color="#8a5ef0" 
        hidden={state} 
        easing={'easeInOutCubic'}
        onComplete={Complete}
        type={'rectangle'} >
          <div className="btn-pro" onClick={()=>setstate(true)}>
            <img src={props.img} alt="avatar" />
            <p>{props.name}</p>
          </div>
          </ParticleEffectButton>
        <div className="progress-wrap">
          <CircularProgressbar
            value={props.progress}
            text={`${props.progress}% `}
          />
          ;
        </div>
      </div>
  );
}

export default MyCoursesAnalytic;
