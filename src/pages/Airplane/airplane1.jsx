import React, { Component } from 'react';
import Samalyot from "../../assets/icons/samalyot.svg"

class Airplane1 extends Component {
  state = { 
    scroll: 0,
    moveY: 0,
    moveX: 0,
    rotate:0,
    scale: 1,
    zindex: 1
   } 
  componentDidMount() {
    window.addEventListener('scroll', this.listenToScroll);
  }
  componentWillUnmount() {
    window.removeEventListener('scroll', this.listenToScroll)
  }
  listenToScroll = () => {
    const S =  document.body.scrollTop || document.documentElement.scrollTop

    this.setState({
      scroll: S,
    })
    if (S <= 200) {
      this.setState({ moveY: S * 0.2, moveX: S * -2, rotate: S * -0.125  })
    } else { 
      if (S > 200) {
        this.setState({ moveY: S * 0.5, moveX: S * -1.8, rotate: S * -0.3 })
      }
      if (S > 300) {
        this.setState({ moveY: S * 0.9, rotate: S * -0.3 })
      }
      if (S >= 400) {
        this.setState({ moveY: S * 0.8, moveX: S * -1.35, rotate: S * -0.33, })
      }
      if (S > 400) {
        this.setState({ moveY: 320, moveX: -540, rotate: -90, })
      }
      if (S > 500) {
        this.setState({ moveY: 550, })
      }
      if (S > 600) {
        this.setState({ moveY: 650, })
      }
      if (S > 700) {
        this.setState({ moveY: 750, })
      }
      if (S > 800) {
        this.setState({ moveY: 850, })
      }
      if (S < 800) {
        this.setState({ scale: 1 })
      }
      if (S > 900) {
        this.setState({ moveY: 950, scale: -1, rotate: 90 })
      }
      if (S > 1000) {
        this.setState({ moveY: 1050, })
      }
      if (S > 1100) {
        this.setState({ moveY: 1150, })
      }
      if (S >= 1200) {
        this.setState({ moveY: 1150, rotate: 20 })
      }
      if (S >= 1300) {
        this.setState({ moveY: 1200, moveX: -230, rotate: 20 })
      }
      if (S >= 1400) {
        this.setState({ moveY: 1250, moveX: 0, rotate: 20 })
      }
      if (S >= 1500) {
        this.setState({ moveY: 1290, moveX: 300, rotate: 30 })
      }
      if (S >= 1600) {
        this.setState({ moveY: 1350, moveX: 600, rotate: 40 })
      }
      if (S >= 1700) {
        this.setState({ moveY: 1400, moveX: 700, rotate: 80 })
      }
      if (S >= 1800) {
        this.setState({ moveY: 1400, moveX: 700, rotate: 100 })
      }
    }
  }
  render() { 
    // console.log(this.state.scroll)
    return (
      <React.Fragment>
        <div className='airplane1'>
          <img style={{
              transform: `
              translateX(${this.state.moveX}px) 
              translateY(${this.state.moveY}px) 
              rotate(${this.state.rotate}deg)
              scaleX(${this.state.scale})
              `,
              zIndex: `${this.state.zindex}`
            }} 
          src={Samalyot} alt="" />
        </div>

      </React.Fragment>
    );
  }
}
 
export default Airplane1;