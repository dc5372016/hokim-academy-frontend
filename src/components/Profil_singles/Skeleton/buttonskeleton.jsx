import React from "react";
import { Skeleton } from "react-skeleton-generator";

const Skeleton1 = () => {
  return (
    <Skeleton.SkeletonThemeProvider animationDuration={2}>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          marginTop:25,
        }}
      >
            <div style={{margin:40}}>
                 <Skeleton width="250px" height="55px" borderRadius="10px" />
            </div>
            <div style={{margin:40}}>
                 <Skeleton width="250px" height="55px" borderRadius="10px" />
            </div>
      </div>
    </Skeleton.SkeletonThemeProvider>
  );
};

export default Skeleton1;
