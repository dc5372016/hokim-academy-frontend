import styled from "styled-components";





export const SubContainer = styled.div`
    display: flex;
    flex-direction: column;
    grid-gap: 15px;
    max-height: 350px;
    overflow-y: auto;
    padding: 15px;
    &::-webkit-scrollbar-track{
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.1);
        background-color: #F5F5F5;
        border-radius: 10px;
    }
    &::-webkit-scrollbar
    {
        width: 4px;
        height: 4px;
        border-radius: 10px;
        background: linear-gradient(180deg, #03F5FF 0%, #02B6EB 100%);
    }
    &::-webkit-scrollbar-thumb
    {
        border-radius: 4px;
        background: linear-gradient(180deg, #03F5FF 0%, #02B6EB 100%);
    }
    
`;

export const FilterBtn = styled.button`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 237px;
    max-height: 77px;
    min-height: 77px;
    background: white;
    border-radius: 15px;
    cursor: pointer;
    font-family: 'Lexend';
    font-style: normal;
    font-weight: 400;
    font-size: 28px;
    line-height: 35px;
    text-align: center;
    color: #03F5FF;
    outline: none;
    border: 1px solid #03F5FF;
    
    &:hover{
        background: linear-gradient(180deg, #03F5FF 0%, #02B6EB 100%);
        color: white !important;
    }
    
    @media (max-width:1025px){
        max-height: 46px;
        min-height: 46px;
        font-size: 18px;
        line-height: 24px;
        width: 160px;
    }
`;

export const SlideFilter = styled.div`
    display: none;
    justify-content: center;
    position: relative;
    @media (max-width: 1025px){
            display: block;
    }
    
`;

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    padding: 100px;
    position: relative;
    align-self: flex-end;
    @media (max-width: 1400px){
        padding: 50px;
        ${FilterBtn}{
            max-height: 66px;
            min-height: 66px;
            font-size: 24px;
            line-height: 30px;
            width: 200px;
        }
    }
    @media (max-width: 1025px){
        align-self: auto;
        padding: 00px;
        ${SubContainer}{
            display: flex;
            flex-direction: row;
            float: right;
            padding: 20px;
            max-height: 50px;
            overflow-x: auto;
            overflow-y: hidden;
            max-width: 100%;
        }
    }
    @media (max-width: 1100px){
        ${FilterBtn}{
            max-height: 55px;
            min-height: 55px;
            font-size: 20px;
            line-height: 25px;
            min-width: 160px;
        }
    }
`;