import React, { useState } from 'react'
import MainBG  from '../../components/Info_Courses_Next/MainBG/MainBg';
import Category  from '../../components/Info_Courses_Next/Category/Category';
import Dasturlash  from '../../components/Info_Courses_Next/Dasturlash/Dasturlash';
import MyCourses  from '../../components/Info_Courses_Next/MyCourses/MyCourses';
import { useEffect } from 'react';


const InfoCourse = () => {
  const [render, setrender] = useState(false);
  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: 'auto'
    })
  }, [])
  
  return (
    <>
      <MainBG />
      <Category render={render}/>
      {/* <Dasturlash render={render} setrender={setrender} /> */}
      {/* <MyCourses /> */}
    </>
  )
}


export default InfoCourse