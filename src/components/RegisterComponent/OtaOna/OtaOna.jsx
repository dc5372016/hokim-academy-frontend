import React, { useState } from 'react'
import { Box, Button, Container, Input, InputNumber, Label, Option, Rows, Rows1, Select } from './style'
import axios from 'axios'
import CustomizedProgressBars from '../Circular/Progress'
import { useNavigate} from 'react-router-dom'
import Stack from '@mui/material/Stack';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert from '@mui/material/Alert';
import { useDispatch } from 'react-redux'
import Swal from 'sweetalert2'
import { baseURL } from '../../../api/baseUrl'

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

const OtaOna = ({bigRender,setbigRender}) => {
  const [gender, setgender] = useState('Erkak');
  const [phone, setphone] = useState('');
  const [password, setpassword] = useState('')
  const [full_name, setfull_name] = useState('')
  const [btnword, setbtnword] = useState('Akkaunt yaratish')
  const [open, setOpen] = React.useState(false);
  const [snackbar_word, setsnackbar_word] = useState('')
  const dispatch = useDispatch()
  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };
  const data = [
    {
      id: "1",
      value: "Male",
      name:"Erkak"
    },
    {
      id: "2",
      value: "Female",
      name:"Ayol"
    }
  ]
  const [open2 , setOpen2] = useState(false)
  const [selected, setSelected] = useState('Male')
  const inputClick = () =>{ setOpen2(true) }
  const itemClick = (e) =>{ 
    setSelected(e)
    setOpen2(false)
  }
  console.log(selected);
  const navigate = useNavigate()
  console.log(btnword);

  const handleChange = (event) => {
    setgender(event.target.value);
    setbtnword('Akkaunt yaratish')
  };

 
  let formData = new FormData();
  formData.append('phone',phone)
  formData.append('password',password)
  formData.append('full_name',full_name)
  formData.append('gender',selected)

  const SetmyTime = () => {
    setbtnword('Akkaunt yaratish')
  }
  const SuccessTrue = () => {
    window.location.href = '/'
  }

  const Register =  () => {
       if(password.length < 8){
        Swal.fire({
          icon: 'warning',
          title: 'Eslatma!',
          text: 'Parol 8ta belgidan ko`p bo`lishi kerak!',
        })
       }else{
        setbtnword('Yaratilmoqda')
        axios.post(`${baseURL}/api/register/`,formData)
        .then(res=>{
            console.log(res.data);
            if(res.data.error){
                 setbtnword('Error in filling')
                 setTimeout(SetmyTime, 3000);
                 setsnackbar_word(res.data.error)
                 handleClick()
            }
            if(res.data.data !==undefined){
                setfull_name('')
                setpassword('')
                setphone('')
                setbtnword('Yaratildi')
                setsnackbar_word('Succesfully created')
                localStorage.setItem('access_key',JSON.stringify(res.data.data))
                dispatch({type:'AUTH_SUCCESS',payload:{token:res.data.data.token,id:res.data.data.user_id}})
                setbigRender(!bigRender)
                handleClick()
                setTimeout(SetmyTime, 2000);
                setTimeout(SuccessTrue,2500);
            }
            
        })
        .catch(err=>{
            console.log(err);
            setbtnword('Xatolik')
        })
       }
  }

  
  

  return (
    <Container>
        <Rows>
            <Box>
                <Label>Telefon raqami</Label>
                <InputNumber
                    placeholder="+998 _ _  _ _"
                    format="998#########"
                    mask="_"
                    value={phone}
                    onChange={(e)=>{
                      setphone(e.target.value)
                      setbtnword('Akkaunt yaratish')
                    }}
                  />
            </Box>
            <Box>
                <Label>Parol</Label>
                <Input
                 placeholder='8 xonadan  dan iborat bo’lish kerak' 
                 value={password}
                 onChange={(e)=>{
                   setpassword(e.target.value)
                   setbtnword('Akkaunt yaratish')
                  }}
                 />
            </Box>
        </Rows>
        <Rows>
            <Box>
                <Label>F.I.Sh</Label>
                <Input
                  value={full_name}
                  onChange={(e)=>{
                    setfull_name(e.target.value)
                    setbtnword('Akkaunt yaratish')
                  }}
                 placeholder='Familya , Ism, ...' 
                 />
            </Box>
            <Box>
                <Label>Jinsi</Label>
                <Input
                  value={selected === "Male" ? "Erkak" : "Ayol"}
                  onClick={inputClick}
                 />
                <div onClick={() => setOpen2(false)} className={open2 === true ? "select__close__show" : ""}></div>
                <div className="edit__select">
                  <div className={open2 === true ? "options height_a" : "options"}>
                    <div>
                      {data.map((c) => {
                        return(
                          <label htmlFor={c.id}>
                              <input type="checkbox" value={c.name} id={c.id} />
                              <p onClick={()=>itemClick(c.value)} className="result">
                                  {c.name}
                              </p>
                          </label>
                        )
                      })}
                    </div>
                    
                  </div>
                </div>
                {/* <Select
                  value={gender}
                  onChange={handleChange}
                  
                >
                  <Option  value={'Erkak'}>Erkak</Option>
                  <Option  value={'Ayol'}>Ayol</Option>
                </Select> */}
            </Box>
        </Rows>
        {/* <Rows>
            <Box>
                <Label>Tugʻilgan sana</Label>
                <Input  placeholder='Oy-Kun-Yil' />
            </Box>
            <Box>
                <Label>Telefon raqami</Label>
                <InputNumber
                    placeholder="+998 _ _  _ _"
                    format="+998 ## ### ## ##"
                    mask="_"
                  />
            </Box>
        </Rows> */}
        <Rows1>

            <Box>
                <Label> &nbsp;</Label>
                <Button onClick={Register}>
                    {
                        btnword === 'Akkaunt yaratish' ?
                        btnword : btnword === 'Yaratilmoqda' ?
                        <CustomizedProgressBars /> : btnword === 'Xatolik' ? 
                        <p style={{color:'red'}}>Xatolik</p> : btnword === 'Yaratildi' ? 
                        <div>Yaratildi</div> : <div style={{color:'red'}}>Error in filling</div>
                    } 
                </Button>
            </Box>
        </Rows1>
        <Stack spacing={2} sx={{ width: '100%' }}>
            <Snackbar anchorOrigin={{horizontal:'center',vertical:'top'}} style={{zIndex:99999,marginTop:120}} open={open} autoHideDuration={6000} onClose={handleClose}>
                <Alert  onClose={handleClose} severity={snackbar_word === 'Succesfully created' ?  "success" : 'error'} sx={{ width: '100%',backgroundColor:snackbar_word === 'Succesfully created' ?  "blue" : 'red' }}>
                    {snackbar_word}
                </Alert>
            </Snackbar>
            
        </Stack>
    </Container>
  )
}

export default OtaOna