import React, { useEffect, useState } from 'react';
import OtaOna from './OtaOna/OtaOna';
import Oquvchi from './Oquvchi/Oquvchi';
import Oqituvchi from './Oqituvchi/Oqituvchi';
import { CategoryButton, CategoryContainer, Container, RegisterContainer, Title, Wrapper } from './style';
import Bg from '../../assets/images/registerbg.svg';
import Bg1 from '../../assets/images/otaonabg.svg';
import Bg2 from '../../assets/images/teacherbg.svg';


let Data = ['Ota - Ona','O’quvchi','O’qituvchi']

const Kirish = ({bigRender,setbigRender}) => {
  const [category, setcategory] = useState('Ota - Ona');
  const [mybg, setmybg] = useState(Bg2);

  useEffect(() => {
    window.scrollTo({
      top:0,
      behavior: 'auto'
    })
  }, [])

  const Ishla = (item) => {
    setcategory(item)
    if(item==='Ota - Ona'){
      setmybg(Bg2)
    }else if(item==='O’quvchi'){
      setmybg(Bg1)
    }else if(item==='O’qituvchi'){
      setmybg(Bg2)
    }
  }
  

  return (
    <Container bg={mybg}>
      <Wrapper>
        <RegisterContainer>
          {/* <CategoryContainer>
             {Data.map((item,index)=>(
               <CategoryButton bg={item===category ? true : false} key={index} onClick={()=>Ishla(item)}>{item}</CategoryButton>
             ))}
          </CategoryContainer> */}
          <br />
          <br />
          <br />
          <br />
          
          <Title>Roʻyxatdan oʻtish</Title>
          {
            category === 'Ota - Ona' ?
            <OtaOna  setbigRender={setbigRender} bigRender={bigRender} /> : category === 'O’quvchi' ? 
            <Oquvchi /> : <Oqituvchi />
          }
        </RegisterContainer>
      </Wrapper>
    </Container>
  )
}

export default Kirish