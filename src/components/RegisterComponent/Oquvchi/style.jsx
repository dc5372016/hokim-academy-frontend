import styled from 'styled-components';
import Select1 from '@mui/material/Select';
import { MenuItem } from '@mui/material';

export const Container = styled.div`
    width: 100%;
    height: 100%;
    box-sizing: border-box;
    @media (max-width:850px){
       padding: 10px;
    }
`
export const Rows = styled.div`
    width: 100%;
    height: 120px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    @media (max-width:785px){
       flex-direction: column;
       height: 230px;
    }
    @media (max-width:400px){
       height: 190px;
       justify-content: center;
    }

`

export const Rows2 = styled.div`
    width: 100%;
    height: 120px;
    display: none;
    
    
    @media (max-width:785px){
       flex-direction: column;
       height: 230px;
       display: flex;
    }
    @media (max-width:400px){
       height: 100px;
       justify-content: center;
    }
    justify-content: space-between;
    align-items: center;

`
export const Rows1 = styled.div`
    width: 100%;
    height: 250px;
    margin-top: 10px;
    display: flex;
    justify-content: space-between;
    align-items: flex-start;
    @media (max-width:785px){
       flex-direction: column;
       height: 340px;
       justify-content: center;
    }
    @media (max-width:400px){
       height: 190px;
       justify-content: center;
    }
`
export const Box = styled.div`
    @media (max-width:785px){
       width: 100%;
       display: flex;
       justify-content: center;
       align-items: center;
       flex-direction: column;
    }
`
export const Box1 = styled.div`
    @media (max-width:785px){
       display: none;
    }
`
export const MainBox = styled.div`
    @media (max-width:785px){
       width: 100%;
       display: flex;
       justify-content: center;
       align-items: center;
       flex-direction: column;
    }
`
export const Label = styled.div`
    font-family: 'Lexend';
    font-size: 18px;
    font-weight: 400;
    line-height: 23px;
    letter-spacing: 0em;
    text-align: left;
    margin: 15px 10px;
    @media (max-width:785px){
       width: 88%;
    }
    @media (max-width:400px){
        font-size: 16px;
        width: 300px;
        font-weight: 400;
        line-height: 18px;
        letter-spacing: 0em;
        text-align: left;
        margin: 10px 5px;
    }
`
export const Input = styled.input`
    height: 60.274658203125px;
    width: 360.6963195800781px;
    border-radius: 128.8201141357422px;
    border: 1.6px solid rgba(3, 215, 246, 1);
    outline:none;
    background: none;
    text-align: center;
    &::-webkit-input-placeholder {
        color: rgba(201, 201, 201, 1);
    }
    &:-ms-input-placeholder {
    color: rgba(201, 201, 201, 1);
    }
    &::placeholder {
        color: rgba(201, 201, 201, 1);
    }
    @media (max-width:785px){
       width: 90%;
       padding-left: 15px;
       padding-right: 15px;
       box-sizing: border-box;
    }
    @media (max-width:400px){
       width: 300px;
       height: 50px;
    }
`
export const Input1 = styled.textarea`
    height: 143.274658203125px;
    width: 360.6963195800781px;
    border-radius: 25.8201141357422px;
    border: 1.6px solid rgba(3, 215, 246, 1);
    outline:none;
    background: none;
    text-align: left;
    padding: 10px 20px;
    box-sizing: border-box;
    @media (max-width:785px){
       width: 90%;
       padding-left: 15px;
       padding-right: 15px;
       box-sizing: border-box;
    }
    @media (max-width:400px){
       width: 300px;
       height: 80px;
    }
    
`
export const Select = styled(Select1)`
    height: 60.274658203125px !important;
    width: 360.6963195800781px !important;
    border-radius: 128.8201141357422px !important;
    border:none !important;
    outline: 1px solid rgba(3, 215, 246, 1) !important;
    background: none !important;
    text-align: left !important;
    padding-left: 15px !important;
    box-sizing: border-box !important;
    @media (max-width:785px){
       width: 90% !important;
    }
    @media (max-width:400px){
       width: 300px !important;
       height: 50px !important;
    }

`
export const Option = styled(MenuItem)`
`

export const Button = styled.button`
    height: 68.274658203125px;
    width: 360.6963195800781px;
    border-radius: 128.8201141357422px;
    background: linear-gradient(180deg, 
        #03F5FF 0%, #02B6EB 100%);
    font-family: 'Lexend';
    font-size: 22px;
    font-weight: 500;
    line-height: 28px;
    letter-spacing: 0em;
    text-align: center;
    color: white;
    border: none;
    transition: 1s;
    &:hover{
        background: linear-gradient(180deg, 
        #03F5FF 0%, #3a7efc 100%);
        cursor: pointer;
    }
    @media (max-width:785px){
       width: 90%;
       padding-left: 15px;
       padding-right: 15px;
       box-sizing: border-box;
    }
    @media (max-width:400px){
       width: 300px;
       height: 50px;
       font-size: 16px;
    }

`