import React, { useEffect, useState } from 'react'
import { BgImg1, BgImg2, Container, IconImg, ImgContainer, InnerDiv, LeftWrapper, ReusableDiv, RightWrapper, Text, TextContainer, Wrapper } from './style'
import Introbg1 from '../../assets/images/introbg1.svg';
import Introbg2 from '../../assets/images/introbg2.svg';
import Qalamcha from '../../assets/images/qalamcha.png';
import {IntroData} from '../../constants/intro';
import aos from 'aos';

const CourseIntro = () => {
  const [data, setdata] = useState(IntroData[0])

  useEffect(() => {
    aos.init();
    aos.refresh()
  }, [data]);

  const Sorting = (item) => {
    setdata(item);
  }


  return (
    <Container>
      <BgImg1 src={Introbg1} />
      <BgImg2 src={Introbg2} />
      <Wrapper>
        <LeftWrapper>
          <ImgContainer data-aos-duration='1000' data-aos='zoom-in' bg={data.img}>
            <TextContainer>{data.title}</TextContainer>
          </ImgContainer>
        </LeftWrapper>
        <RightWrapper>
          {IntroData?.map((item,index)=>{
              return(
                <ReusableDiv 
                  data-aos='fade-up'
                  data-aos-duration='1000'
                  onClick={()=>Sorting(item)} bg={item.img}
                  key={index}
                >
                  <InnerDiv>
                    <IconImg src={item.icon} />
                    <Text>{item.category}</Text>
                  </InnerDiv>
                </ReusableDiv>
              )
            })}
        </RightWrapper>
      </Wrapper>
    </Container>
  )
}

export default CourseIntro