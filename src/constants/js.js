export const HtmlData = [
    {
        id:1,
        title:'js Intro',
        url:'https://youtu.be/2md4HQNRqJA'
    },
    {
        id:2,
        title:'js Variable',
        url:'https://youtu.be/papg2tsoFzg'
    },
    {
        id:3,
        title:'Js Atributlar',
        url:'https://youtu.be/qz0aGYrrlhU'
    },
    {
        id:4,
        title:'Js Var let cont',
        url:'https://youtu.be/BMKWdLX9w3M'
    },
    {
        id:5,
        title:'Js Dom',
        url:'https://youtu.be/Sb5_BIprKDs'
    },

    {
        id:6,
        title:'Js local Storage',
        url:'https://youtu.be/bCt2FnyY7AE'
    },
    {
        id:7,
        title:'Js Arrays',
        url:'https://youtu.be/nvKktNrJXcY'
    },
    {
        id:8,
        title:'Js Objects',
        url:'https://youtu.be/gx7N3QGrVWE'
    },
    {
        id:9,
        title:'Js Main Basics',
        url:'https://youtu.be/rO6_MZLIzCg'
    },
    {
        id:10,
        title:'React Js',
        url:'https://youtu.be/uT9n7iIxEw8'
    },
    {
        id:11,
        title:'Final JS',
        url:'https://youtu.be/09oErCBjVns'
    },
    
]