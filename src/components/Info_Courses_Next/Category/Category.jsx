import React, { useEffect, useState } from "react";
import {
  BacktoDiv,
  BigContainer,
  Container,
  Img1,
  ListContainer,
  MenuButton,
  MyIconButton,
  NavIcon,
  PlayBtn,
  QuizText,
  Text,
  TextContainer,
  VideoContainer,
  Wrapper,
  XIcon,
} from "./style";
import { Link, useLocation, useNavigate, useParams } from "react-router-dom";
import { HtmlData } from "../../../constants/exact-courses";
import Playing from "../../../assets/images/playing.png";
import Video from "../../CourseComponents/VideoPlayer/VideoPlayer";
import Loading from '../Loading/Loading';
import SnackBar from '../Snackbar/Snackbar'
import { IconButton } from "@mui/material";
import axios from "axios";
import arrow from "../../../assets/icons/profil_arrow.svg"
import { baseURL } from "../../../api/baseUrl";

const Category1 = ({render}) => {
  const {course_id} = useParams()
  console.log("dasdsadsd",course_id);
  const [loading, setloading] = useState(false);
  const [textLoading, settextLoading] = useState('Test topshirish');
  const location = useLocation();
  const [Videoopen, setVideoopen] = useState(false);
  const [data, setData] = useState([]);
  const [video_url, setvideo_url] = useState(HtmlData[0]);
  const [toright, settoright] = useState('-150%');
  const [open, setOpen] = useState(false);
  const [issnack, setissnack] = useState(true);
  const [quizes, setquizes] = useState([]);
  const navigate = useNavigate()
  const [isTested, setisTested] = useState(null);
  const [isdate, setisdate] = useState(null);
  const [javob, setJavob] = useState({});
  let Auth_Data =
  JSON.parse(localStorage.getItem("access_key")) !== null
    ? JSON.parse(localStorage.getItem("access_key"))
    : { user: { full_name: "Unknown" } };
  const Show = () => {
    setVideoopen(true);
  };

  const ForSnackbar = () => {
    if(issnack === true){
      setOpen(true)
    }
  }

  // setTimeout(() => {
  //   ForSnackbar()
  // }, 5000);

  const SetMyVideo = (item) => {
    setvideo_url(item)
    setVideoopen(true);
    OpenList()
  }
  const SetMyVideo1 = (item) => {
    setvideo_url(item)
    setVideoopen(true);
  }

  const OpenList = () => {
    if(toright==='-150%'){
      settoright('0%')
    }else{
      settoright('-150%')
    }
  }


  const ToTest = () => {
    settextLoading('Starting...')
    var config = {
      method: 'post',
      url: `${baseURL}/api/quiz/quiz/${quizes[0].id}/`,
      headers: { 
        Authorization: `token ${Auth_Data.token}`, 
        'Content-Type': 'application/json', 
      },
    };
   
    if(isTested === false || isdate === null){
      axios(config)
      .then((res)=>{
        console.log('myres',res);
        settextLoading('Test topshirish')
        navigate('/tests',{state:quizes})
      })
      .catch(function (error) {
        console.log(error);
      })
    }else if(isTested){
      navigate('/results',{state:javob})
    }
  }

  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: 'auto'
    })
  }, []);

  useEffect(() => {
    try {
      axios.get(`${baseURL}/api/quiz/result/${quizes[0].id}/`,{
        headers: { 
          Authorization: `token ${Auth_Data.token}`, 
          'Content-Type': 'application/json', 
        }
      })
      .then((res)=>{
        console.log('result of tttttt',res);
        setisTested(res.data.success)
        setisdate(res.data.data.end_date)
        setJavob(res.data.data)
      })
    } catch (err) {
      
    }
  }, [setquizes,quizes]);


  useEffect(() => {
      window.scrollTo({
        top: 0,
        behavior: 'auto'
      })
      async function GetLessons() {
        try {
          const res = await axios.get(`${baseURL}/api/lesson/${course_id}/`)
          if(res){
            console.log('resmyres = ',res);
            setData(res.data.data)
            setquizes(res.data.quiz)
            setvideo_url(res.data.data[0])
            setloading(true)
            setissnack(false)
          }
        } catch (err) {
          console.log(err.message);
        }
      }
      GetLessons()
  }, [render]);

  const mystyle ={
    color: 'blue',
    borderRadius:5
  }


  return (
    <>
   
      <SnackBar open={open} setOpen={setOpen} />
      
      <Container>
      {loading ? <Wrapper>
        <ListContainer style={{left:toright}}>
            <MyIconButton onClick={OpenList}>
              <XIcon  />
            </MyIconButton>
            {
              data?.map((item, index) => {
                return (
                  <Text
                    onClick={() => SetMyVideo(item)}
                    key={index}
                    style={video_url.id===item.id ? mystyle : {backgroundColor:'none'} }
                  >
                    {item.name.slice(0,25)}
                  </Text>
                )
              })
            }
            <QuizText onClick={ToTest}>{textLoading}</QuizText>

        </ListContainer>
        <MenuButton onClick={OpenList}><NavIcon /></MenuButton>
        <BacktoDiv>
              <Link to="/dashboard/allcourses" className='back__profile'>
                <img src={arrow} alt="" />
                Kabinetga qaytish
              </Link>
        </BacktoDiv>
        <BigContainer>
          <VideoContainer bg={baseURL+location.state.image} show={Videoopen}>
            <Video
              show={Videoopen}
              setshow={setVideoopen}
              video_url={video_url.video_or_link === 'link' ? video_url.link : baseURL+video_url.video}
            />
            <PlayBtn show={Videoopen} onClick={Show}>
              <Img1 src={Playing} />
            </PlayBtn>
          </VideoContainer>
          <TextContainer>
            { 
              data?.map((item, index) => {
                return (
                  <Text
                    onClick={() => SetMyVideo1(item)}
                    key={index}
                    style={video_url.id===item.id ? mystyle : {backgroundColor:'none'} }
                  >
                    {item.name.slice(0,25)}
                  </Text>
                )
              }) 
            }
            <QuizText onClick={ToTest}>{textLoading}</QuizText>
            
          </TextContainer>
        </BigContainer>
      </Wrapper> : <Loading />}
    </Container>
    </>
    
  )
}

export default Category1