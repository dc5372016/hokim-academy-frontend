import React, { useEffect, useState } from "react";
import {
  AllContainer,
  Block,
  Container,
  ExitIcon,
  ExitIcon1,
  FilterButton,
  FilterButton1,
  FilterButton2,
  MenuButtonCool,
  MenuItemStyled,
  Name,
  Number,
  ProfileContainer,
  ProfileImg,
  ProfileWrapper,
  ResponsiveMenu,
  RightContainer,
} from "./style";
import Profile_Img from "../../assets/images/prfile_im.png";
import { Navigate, Outlet, useNavigate } from "react-router-dom";
import Button from "@mui/material/Button";
import Menu from "@mui/material/Menu";
import 'sweetalert2/src/sweetalert2.scss'
import Swal from 'sweetalert2'
import MenuIcon from "@mui/icons-material/Menu";





//icons
import AlignHorizontalLeftSharpIcon from '@mui/icons-material/AlignHorizontalLeftSharp';
import FolderSharedSharpIcon from '@mui/icons-material/FolderSharedSharp';
import SettingsSharpIcon from '@mui/icons-material/SettingsSharp';
import LogoutSharpIcon from '@mui/icons-material/LogoutSharp';
import { baseURL } from "../../api/baseUrl";



const Dashboard = () => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const [choice, setChoice] = useState("all");
  let Auth_Data =
    JSON.parse(localStorage.getItem("access_key")) !== null
      ? JSON.parse(localStorage.getItem("access_key"))
      : { user: { img: Profile_Img, full_name: "Unknown" } };

  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: "auto",
    });
  }, []);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const navigate = useNavigate();

  const Navigating = (route,active) => {
    navigate(route);
    setAnchorEl(null);
    setChoice(active)
  };

  const Chiqish = () => {
    Swal.fire({
      title: 'Siz rostdan ham chiqmoqchimisiz?',
      text: "Buni orqaga qaytarishni iloji yo`q!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText:'Yo`q',
      confirmButtonText: 'Ha , Chiqaman!'
    }).then((result) => {
      if (result.isConfirmed) {
        localStorage.removeItem("access_key");
        navigate("/login");
        setChoice('log')
        window.location.reload(true);
        
      }
    })
    
  };

  return (
    <AllContainer>
      <Container>
        <ProfileContainer>
          <ProfileWrapper>
            <Block>
              <ProfileImg
                src={
                  Auth_Data.user.image === null
                    ? Profile_Img
                    : baseURL+Auth_Data.user.image
                }
              />
              <Name>{Auth_Data.user.full_name}</Name>
              <Number>+{Auth_Data.user.phone}</Number>
              <FilterButton
                style={{ color: choice === "all" ? "#7F74FC" : "#555454" }}
                onClick={() => Navigating("/dashboard/allcourses",'all')}
              >
                <AlignHorizontalLeftSharpIcon style={{ color: choice === "all" ? "#7F74FC" : "#555454" }}  className="mui-icons" />
                 Barcha kurslar
              </FilterButton>
              <FilterButton
                style={{ color: choice === "my" ? "#7F74FC" : "" }}
                onClick={() => Navigating("/dashboard/mycourses",'my')}
              >
                <FolderSharedSharpIcon style={{ color: choice === "my" ? "#7F74FC" : "#555454" }} className="mui-icons"  />
                Mening kurslarim
              </FilterButton>
              
              <FilterButton2
                style={{ color: choice === "edit" ? "#7F74FC" : "" }}
                onClick={() => Navigating("/taxtitlash",'edit')}
              >
                <SettingsSharpIcon  style={{ color: choice === "edit" ? "#7F74FC" : "#555454" }} className="mui-icons" />
                Tahrirlash
              </FilterButton2>
              <FilterButton
                style={{ color: choice === "log" ? "#7F74FC" : "" }}
                onClick={Chiqish}
              >
                <LogoutSharpIcon style={{ color: choice === "log" ? "#7F74FC" : "#555454" }} className="mui-icons"  />
                Chiqish
              </FilterButton>
              <ResponsiveMenu>
                <MenuButtonCool
                  id="basic-button"
                  aria-controls={open ? "basic-menu" : undefined}
                  aria-haspopup="true"
                  aria-expanded={open ? "true" : undefined}
                  onClick={handleClick}
                >
                  <MenuIcon style={{ fontSize: 25 }} />
                </MenuButtonCool>
                <Menu
                  id="basic-menu"
                  anchorEl={anchorEl}
                  open={open}
                  onClose={handleClose}
                  MenuListProps={{
                    "aria-labelledby": "basic-button",
                  }}
                  style={{ marginLeft: -20 }}
                >
                  <MenuItemStyled
                    style={{ color: choice === "all" ? "#7F74FC" : "rgba(33,33,33,1)" }}
                    onClick={() => Navigating("/dashboard/allcourses")}
                  >
                    <AlignHorizontalLeftSharpIcon style={{ color: choice === "all" ? "#7F74FC" : "#555454" }}   className="mui-icons" />
                    Barcha kurslar
                  </MenuItemStyled>
                  <MenuItemStyled
                    style={{ color: choice === "my" ? "#7F74FC" : "rgba(33,33,33,1)" }}
                    onClick={() => Navigating("/dashboard/mycourses")}
                  >
                    <FolderSharedSharpIcon style={{ color: choice === "my" ? "#7F74FC" : "#555454" }}   className="mui-icons" />
                    Mening kurslarim
                  </MenuItemStyled>
                  
                  <MenuItemStyled
                    style={{ color: choice === "edit" ? "#7F74FC" : "rgba(33,33,33,1)" }}
                    onClick={() => Navigating("/taxtitlash")}
                  >
                    <SettingsSharpIcon style={{ color: choice === "edit" ? "#7F74FC" : "#555454" }}   className="mui-icons" />
                    Tahrirlash
                  </MenuItemStyled>
                  <MenuItemStyled
                    style={{ color: choice === "log" ? "#7F74FC" : "rgba(33,33,33,1)" }}
                    onClick={Chiqish}
                  >
                    <LogoutSharpIcon style={{ color: choice === "log" ? "#7F74FC" : "rgba(33,33,33,1)" }}   className="mui-icons" />
                    Chiqish 
                  </MenuItemStyled>
                </Menu>
              </ResponsiveMenu>
            </Block>
          </ProfileWrapper>
        </ProfileContainer>
        <RightContainer>
          <Outlet />
        </RightContainer>
      </Container>
    </AllContainer>
  );
};

export default Dashboard;
