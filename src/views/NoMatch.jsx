import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import error from "../assets/images/404.svg"

class NoMatch extends Component {
  state = {  } 
  render() { 
    return (
      <React.Fragment>
        <div className='noMatch'>
          <img src={error} alt="" />
          <p>Sahifa topilmadi</p>
          <Link to="/">Orqaga qaytish</Link>
        </div>
      </React.Fragment>
    );
  }
}
 
export default NoMatch;