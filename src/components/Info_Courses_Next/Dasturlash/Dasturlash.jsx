import React, { useEffect, useState } from "react";
import {
  AuthorContainer,
  AvatarContainer,
  Button1,
  Category,
  Container,
  Container1,
  Content,
  Image,
  Image1,
  LessonCount,
  Name,
  ReusableTitle,
  Text1,
  VidosWrap,
  Wrapper,
} from "./style";
import { Courses } from "../../../constants/courses";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import axios from "axios";
import { baseURL } from "../../../api/baseUrl";

const Dasturlash = ({render,setrender}) => {
  const location = useLocation();
  const [data, setdata] = useState([]);
  const {course_id} = useParams()
  const [loading, setloading] = useState(false);
  const navigate = useNavigate();
  const [rasm, setrasm] = useState('');

  const Ishla = (item) => {
    navigate(`/course/${item.id}`,{state:{...item,word:location.state.word}})
    window.scrollTo({
      top:250,
      behavior: "smooth"
    })
    setrender(!render)
  }

  useEffect(() => {
    async function GetAllCourses() {
      try {
        const res = await axios.get(`${baseURL}/api/courses-with-category/`);
        if(res){
          let filtered = res.data.data.filter((item)=>{
            if(item.category.name === location.state.word){
              return item.courses
            }
          })
          
          console.log("filtered",filtered[0].courses);
          setdata(filtered[0].courses)
        }
        setloading(true)
      } catch (err) {
        console.log(err.message);
      }
    }
    GetAllCourses()
  }, [render,setrender]);

  return (
    <Container>
      { loading ? <Wrapper>
        <ReusableTitle>{location.state.word}</ReusableTitle>
        <VidosWrap>
          {data?.map((item, index) => {
            return (
              <Container1
                // data-aos="flip-left"
                // data-aos-duration="500"
                // data-aos-delay={index * 150}
                // data-aos-offset="100"
                // data-aos-easing="ease-in-sine"
                key={index}>
                <Image src={baseURL+item.image} />
                <LessonCount>{item.lesson_count+1} dars</LessonCount>
                <Content>
                  <AuthorContainer>
                    <Name>{item.author.name}</Name>
                    <AvatarContainer>
                      <Image1
                        src={baseURL+item.author.image}
                        style={{ borderRadius: "25px" }}
                      />
                    </AvatarContainer>
                  </AuthorContainer>
                  <Category>{item.name}</Category>
                  <Text1>
                    {item.views} ko'rildi - {item.day} kun oldin
                  </Text1>
                  <Button1 onClick={()=>Ishla(item)}>Boshlash</Button1>
                </Content>
              </Container1>
            );
          })}
        </VidosWrap>
      </Wrapper> : ''}
    </Container>
  );
};

export default Dasturlash;
