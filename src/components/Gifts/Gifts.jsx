import React, { Component  } from 'react';
import { Carousel } from 'react-responsive-carousel';
import Atropos from 'atropos/react';
import "atropos/atropos.css";
// import Images 
import G1 from "../../assets/images/G1.webp";
import G2 from "../../assets/images/G2.webp";
import G3 from "../../assets/images/G3.webp";
import G4 from "../../assets/images/G4.webp";
import G5 from "../../assets/images/G5.webp";
import G6 from "../../assets/images/G6.webp";
import G7 from "../../assets/images/G7.webp";
import G8 from "../../assets/images/G8.webp";

// Import Swiper styles
import "react-responsive-carousel/lib/styles/carousel.min.css";

class Gifts extends Component {
  state = { 
    data: [
      {id: 1 ,img : G1},
      {id: 2 ,img : G2},
      {id: 3 ,img : G3},
      {id: 4 ,img : G4},
      {id: 5 ,img : G5},
      {id: 6 ,img : G6},
      {id: 7 ,img : G7},
      {id: 8 ,img : G8},
    ]
  }
  render() { 
    return (
      <React.Fragment>
        <div className="gifts">
          <p className="gifts__title">Aktiv o'quvchilar uchun bizning sovg'alar</p>
          <Carousel 
            showArrows={true} 
            infiniteLoop={true}
            
            // centerMode={true} 
            // centerSlidePercentage={100}
            thumbWidth={100}
          >
            {this.state.data.map((i, index) =>{
              return(
                
                <div className='gift__swiper' key={index}>
                  <img src={i.img} alt="" />
                </div>
              )
            })}
          </Carousel>
        </div>
      </React.Fragment>
    );
  }
}
 
export default Gifts;

// const Gifts = () => {
//   const [activeThumb, setActiveThumb] = useState(null);

//   const data = [
    
//   ]
//   return ( 
    
//    );
// }
 
// export default Gifts;