import styled from 'styled-components';
import SearchIcon from '@mui/icons-material/Search';
import Select from 'react-select';





export const SearchBanner = styled.div`
    width: 100%;
    height: 86px;
    border-radius: 6px;
    box-shadow: 0px 4px 10px 0px #9CA1A440;
    margin-bottom: 30px;
    margin: auto;
    background-color: #EBFEFF;
    /* background-color: red; */
    display: flex;
    align-items: center;
    justify-content: space-around;
    max-width: 1550px;
    @media (max-width:1111px){
        flex-direction: column;
        height: auto;
        padding: 20px;
        padding-top: 40px;
        padding-bottom: 40px;
        box-sizing: border-box;
    }
`
export const Space = styled.div`
    height: 50px;
    width: 100%;
    /* border: 1px solid red; */
`

export const SelectView = styled(Select)`
    height: 50px !important;
    width: 225px !important;
    border-radius: 10px;
    background-color: white;
    border: 1px solid red;
    font-size: 18px;
    padding: 10px !important;
    scroll-behavior: smooth;
    border: none !important;
    @media (max-width:1111px){
        width: 100% !important;
        margin-top: 10px;
    }
`

export const Option = styled.option`
    border: none !important;
    height: 40px !important;
    &:hover{
        background-color: red !important;
    }
`


export const SearchInpContainer = styled.div`
    width: 230px;
    height: 40px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 5px;
    padding-right: 10px;
    background-color: white;
    border-radius: 10px;
    @media (max-width:1111px){
        width: 100% ;
        margin-top: 10px;
    }
`
export const SearchRasm = styled(SearchIcon)`
   
`

export const SearchInp = styled.input`
    width: 90%;
    border: none;
    outline: none;
    height: 40px;
    font-size: 18px;
    @media (max-width:1111px){
       padding-left: 20px;
    }
`