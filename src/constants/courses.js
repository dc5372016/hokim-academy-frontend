import React, { useState, useEffect } from "react";
import axios from "axios";
import Cr1 from "../assets/images/course/cr1.png";
import Cr2 from "../assets/images/course/cr2.png";
import Cr3 from "../assets/images/course/cr3.png";
import Cr4 from "../assets/images/course/cr4.png";
import Cr5 from "../assets/images/course/cr5.png";
import Cr6 from "../assets/images/course/cr6.png";
import Cr7 from "../assets/images/course/cr7.png";
import Cr8 from "../assets/images/course/cr8.png";
import Cr9 from "../assets/images/course/cr9.png";
import Cr10 from "../assets/images/course/cr10.png";
import Cr11 from "../assets/images/course/cr11.png";
import Cr12 from "../assets/images/course/cr12.png";
import Avatar1 from "../assets/images/avatar/avatar1.png";
import Avatar2 from "../assets/images/avatar/avatar2.png";
import Avatar3 from "../assets/images/avatar/avatar3.png";
import Avatar4 from "../assets/images/avatar/avatar4.png";


export const Courses = [
    {
        id: 1,
        name: "Rano Aliyeva",
        avatar: Avatar1,
        image: Cr1,
        category: "Ingliz tili ",
        lessons_count: 12,
        views_count: "5.3k",
        day: 2,
    },
    {
        id: 2,
        name: "Baxodir Jo’rayev",
        avatar: Avatar2,
        image: Cr2,
        category: "Matematika misollar toplami",
        lessons_count: 12,
        views_count: "5.7k",
        day: 3,
    },
    {
        id: 3,
        name: "Rayhon Isayeva",
        avatar: Avatar3,
        image: Cr3,
        category: "Fizika boshlang’ich",
        lessons_count: 12,
        views_count: "6.4k",
        day: 1,
    },
    {
        id: 4,
        name: "Davron Umarov",
        avatar: Avatar4,
        image: Cr4,
        category: "Kimyo formulalar bilan ishlash",
        lessons_count: 12,
        views_count: "9.1k",
        day: 3,
    },
    {
        id: 5,
        name: "Rayhon Isayeva",
        avatar: Avatar3,
        image: Cr5,
        category: "Fizika boshlang’ich",
        lessons_count: 7,
        views_count: "8.2k",
        day: 1,
    },
    {
        id: 6,
        name: "Baxodir Jo’rayev",
        avatar: Avatar2,
        image: Cr6,
        category: "Matematika misollar toplami",
        lessons_count: 13,
        views_count: "7k",
        day: 2,
    },
    {
        id: 7,
        name: "Davron Umarov",
        avatar: Avatar4,
        image: Cr7,
        category: "Kimyo formulalar bilan ishlash",
        lessons_count: 9,
        views_count: "3.5k",
        day: 1,
    },
    {
        id: 8,
        name: "Rano Aliyeva",
        avatar: Avatar1,
        image: Cr8,
        category: "Ingliz tili ",
        lessons_count: 8,
        views_count: "15k",
        day: 2,
    },
    {
        id: 9,
        name: "Davron Umarov",
        avatar: Avatar4,
        image: Cr9,
        category: "Kimyo formulalar bilan ishlash",
        lessons_count: 11,
        views_count: "5.5k",
        day: 3,
    },
    {
        id: 10,
        name: "Rayhon Isayeva",
        avatar: Avatar3,
        image: Cr10,
        category: "Fizika boshlang’ich",
        lessons_count: 15,
        views_count: "5.4k",
        day: 2,
    },
    {
        id: 11,
        name: "Rano Aliyeva",
        avatar: Avatar1,
        image: Cr11,
        category: "Ingliz tili ",
        lessons_count: 12,
        views_count: "1.8k",
        day: 4,
    },
    {
        id: 12,
        name: "Baxodir Jo’rayev",
        avatar: Avatar2,
        image: Cr12,
        category: "Matematika misollar toplami",
        lessons_count: 14,
        views_count: "12.3k",
        day: 1,
    }
]