import React from 'react'
import { useNavigate } from 'react-router-dom'
import { AuthorContainer, AvatarContainer, Button, Category, Container, Content, Image, Image1, LessonCount, Name, Text } from './styles'

import 'sweetalert2/src/sweetalert2.scss'
import Swal from 'sweetalert2'
import { baseURL } from '../../../api/baseUrl'


const Course = ({ item }) => {
    const navigate = useNavigate()
    let token = localStorage.getItem('access_key');

    const ToMore = (item) =>{
        if(token===null){
            Swal.fire({
                title: 'Eslatma !',
                icon: 'info',
                text: 'Videolarni ko`rish uchun iltimos ro`yhatdan o`ting',
                imageWidth: 400,
                imageHeight: 200,
                imageAlt: 'Custom image',
                confirmButtonColor: '#3085d6',
                confirmButtonText:'Ro`yhatdan o`tish',
                showCloseButton: true,
                footer: '<a href="/login">Yoki Login orqali kiring</a>'
            })
            .then((result) => {
                /* Read more about handling dismissals below */
                if (result.isConfirmed) {
                  navigate('/register')
                }
              })
        }else{
            navigate(`/course/${item.id}`,{state:item})
        }
    }
    
    return (
        <Container data-aos-duration="2000" >
            <Image src={baseURL+item.image} />
            <LessonCount>{item.lesson_count}ta dars</LessonCount>
            <Content>
                <AuthorContainer>
                    <Name>{item.author.name}</Name>
                    <AvatarContainer>
                        <Image1 src={baseURL+item.author.image} style={{ borderRadius: "25px" }} />
                    </AvatarContainer>
                </AuthorContainer>
                <Category>{item.name}</Category>
                <Text>{item.rating}{'K'} ko'rildi - {item.day} kun oldin</Text>
                <Button onClick={()=>ToMore(item)}>Boshlash</Button>
            </Content>
        </Container>
    )
}

export default Course;