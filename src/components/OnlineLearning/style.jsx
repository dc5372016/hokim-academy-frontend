import styled from 'styled-components';
import bg from "../../assets/images/online_bg.png"


export const Container = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    background: url(${bg});
    position: relative;
`