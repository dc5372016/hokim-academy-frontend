import styled from "styled-components"
import Select from 'react-select'
import { Button, Checkbox, Radio } from "@mui/material"




export const Container = styled.div`
    width: 100%;
    height: auto;
    display: flex;
    justify-content: center;
    box-sizing: border-box;
`
export const Wrapper = styled.div`
    width: 97%;
    max-width: 1350px;
`

export const BacktoDashboard = styled.div`
    width: 100%;
    height: 120px;
    display: flex;
    align-items: center;
    margin-top: 200px;
    @media (max-width:600px){
       margin-top: 100px;
    }
`
export const BackDiv = styled.div`
    display: flex;
    width: auto;
    align-items: center;
    transition: 0.3s;
    &:hover{
        cursor: pointer;
    }
`
export const BackImg = styled.img`
    
`
export const BackText = styled.div`
    color: rgba(2, 182, 235, 1);
    font-family: 'Lexend';
    font-size: 22px;
    font-weight: 400;
    line-height: 28px;
    letter-spacing: 0px;
    text-align: left;
    margin-left: 25px;

`

export const TestContainer = styled.div`
    width: 100%;
    height: auto;
    box-shadow: 0px 4px 20px 0px rgba(0, 0, 0, 0.25);
    @media (max-width:700px){
        padding: 10px;
        box-sizing: border-box;
        padding-left: 15px;
    }
`
export const TestContainer2 = styled.div`
    width: 100%;
    height: auto;
    padding: 50px;
    padding-top: 20px;
    box-sizing: border-box;
    box-shadow: 0px 4px 20px 0px rgba(0, 0, 0, 0.25);
    @media (max-width:700px){
        padding: 10px;
        box-sizing: border-box;
        padding-left: 15px;
    }
`

export const ProgressBarHere = styled.div`
    width: 100%;
    height: 80px;
    padding-top: 30px;
    padding-left: 50px;
    padding-right: 50px;
    box-sizing: border-box;
`
export const TestsWrapper = styled.div`
    width: 100%;
    padding-left: 50px;
    padding-right: 50px;
    box-sizing: border-box;
    height: 100%;
    box-sizing: border-box;
    @media (max-width:700px){
        
        width: 100%;
        box-sizing: border-box;
        padding: 0px;
    }
`
export const TestTitle = styled.h1`
    font-family: 'Lexend';
    font-size: 32px;
    font-weight: 500;
    line-height: 40px;
    letter-spacing: 0px;
    text-align: center;
    margin-top: 20px;

`
export const TestsHere = styled.div`
    margin-top: 30px;
    box-sizing: border-box;
`
export const Block = styled.div`
    margin-top: 15px;
`

export const QuestionWrapper = styled.div`
    font-family: 'Lexend';
    font-size: 22px;
    font-weight: 400;
    line-height: 28px;
    letter-spacing: 0px;
    text-align: left;

`
export const AnswerWrapper = styled.div`
    height: 100px;
    display: flex;
    padding: 13px;
    padding-left: 0px;
    justify-content: space-between;
    align-items: center;
    flex-wrap: wrap;
    @media (max-width:700px){
        flex-direction: column;
        align-items: flex-start;
        flex-wrap: initial;
        height: auto;
        width: 100%;
        box-sizing: border-box;
    }
`

export const ChoicesWrapper = styled.div`
    display: flex;
    width: 40%;
    margin-top: 10px;
    align-items: center;
    @media (max-width:700px){
        
        width: 100%;
        box-sizing: border-box;
    }
    
`

export const CheckBox = styled(Checkbox)`
    width: 20px;
    height: 20px;
    border: 1px solid rgba(3, 245, 255, 1) !important;
`

export const Options = styled.div`
    font-family: 'Lexend';
    font-size: 16px;
    margin-left: -20px;
    font-weight: 300;
    line-height: 20px;
    letter-spacing: 0px;
    text-align: left;
    margin-left: 10px;
`

export const SubmitContainer = styled.div`
    height: 150px;
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
`
export const NextButton = styled(Button)`
    background: linear-gradient(180deg, #03F5FF 0%, #02B6EB 100%);
    box-shadow: 0px 5.937713623046875px 11.87542724609375px 0px rgba(93, 95, 239, 0.2);
    height: 56px !important;
    width: 170px !important;
    border-radius: 5.937713623046875px !important;
    border: none !important;
    color: white !important;
    font-weight: 400 !important;
    line-height: 24px !important;
    letter-spacing: 0em !important;
    margin:20px !important;

`

export const CancelButton = styled(Button)`
    border-radius: 5.937713623046875px;
    font-weight: 400 !important;
    line-height: 24px !important;
    letter-spacing: 0em !important;
    color: rgba(2, 182, 235, 1) !important;
    box-shadow: 0px 5.937713623046875px 11.87542724609375px 0px rgba(93, 95, 239, 0.2) !important;
    height: 56px !important;
    width: 170px !important;
    border: 1px solid  rgba(2, 182, 235, 1) !important; 
    margin:20px !important;
`

export const NoTEST = styled.div`
    text-align: center;
    padding: 50px;
    box-sizing: border-box;
    font-size: 20px;
    color: #070707;
`

export const ProfileHeader = styled.div`
    width: 100%;
    height: 50px;
    font-family: 'Lexend';
    font-size: 29px;
    font-weight: 500;
    line-height: 40px;
    letter-spacing: 0px;
    text-align: left;
    color: #000000;
    @media (max-width:550px){
       font-size: 20px;
    }


`

export const ProfileImageEdit = styled.div`
    width: 100%;
    height: 200px;
    margin-top: 30px;
    @media (max-width:550px){
        height: auto;
        margin: 0;
    }
`

export const EditImgDiv = styled.div`
    width: 100%;
    display: flex;
    justify-content: space-between;
    height: 134px;
    @media (max-width:550px){
       flex-direction: column;
       justify-content: space-around;
       align-items: center;
       height: auto;
       padding-top: 15px;
       padding-bottom: 20px;
       margin: 0;
       box-sizing: border-box;

    }
`
export const ImgPreview = styled.div`
    height: 137px;
    width: 137px;
    border-radius: 6.353383541107178px;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color:#9CFBFF ;
    box-sizing: border-box;

`

export const MainImg = styled.img`
    height: 99px;
    width: 99px;
    border-radius: 100%;

`
export const ImgLabel = styled.label`
    width:85%;
    border: 3px dashed #78e0ff;
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding-left: 100px;
    padding-right: 100px;
    box-sizing: border-box;
    @media (max-width:850px){
        padding-right: 10px;
        padding-left: 10px;
        width: 70%;
    }
    @media (max-width:600px){
       justify-content: center;
       width: 70%;
       align-items: center;
       margin-top: 20px;
    }
    @media (max-width:550px){
       width: 100%;
       height: 90px;
    }
    
`

export const FileYuklang = styled.p`
    font-family: 'Lexend';
    font-size: 24px;
    font-weight: 300;
    line-height: 30px;
    letter-spacing: 0px;
    text-align: left;
    color: #808191;
    @media (max-width:600px){
       display: none;
    }

`
export const AvatarYuklang = styled.p`
    background: linear-gradient(180deg, #03F5FF 0%, #02B6EB 100%);
    height: 42px;
    width: 192.9375px;
    border-radius: 5.268062114715576px;
    border: none;
    font-family: Lexend;
    font-size: 17px;
    font-weight: 400;
    line-height: 21px;
    letter-spacing: 0em;
    text-align: center;
    color: white;
    display: flex;
    justify-content: center;
    align-items: center;
`

export const Form = styled.div`
    width: 100%;
    height: auto;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    @media (max-width:666px){
       flex-direction: column;
    }
`

export const InputContainer = styled.div`
    width: 46%;
    height: 120px;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    @media (max-width:666px){
       width: 100%;
       box-sizing: border-box;
    }
`

export const LabelForInp = styled.h1`
    font-family: 'Lexend';
    font-size: 18px;
    font-weight: 500;
    line-height: 23px;
    letter-spacing: 0em;
    text-align: left;
    @media (max-width:900px){
       font-size: 16px;
    }

`

export const Input = styled.input`
    border: none;
    outline: none;
    border-bottom: 2px solid #03D7F6;
    font-family: Lexend;
    font-size: 16px;
    font-weight: 300;
    line-height: 16px;
    letter-spacing: 0em;
    padding-bottom: 10px;
    box-sizing: border-box;
    &::placeholder{
        color: #C9C9C9; 

    }
`
export const SelectView = styled(Select)`
    width: 100% !important;
    background-color: white;
    font-size: 18px;
    scroll-behavior: smooth;
    padding-top: 10px !important;
    padding-bottom: 0px !important;
    border-radius: 0px !important;
    margin-top: -20px;
    border-bottom:2px solid #03D7F6;
    .css-1pahdxg-control::before{
        border: none !important;
        outline: none !important;
        box-shadow: none !important;
        background: none !important;
    }
`

export const ButtonContainer = styled.div`
    width: 100%;
    height: 100px;

`

export const TimeTable = styled.div`
    position: absolute;
    top: 0;
    right: 0;
    width: 150px;
    height: 35px;
    background-color: black;
    color: yellow;
    font-size: 17px;
    font-weight: 500;
    display: ${props=>props.view};
`
export const NewRadio = styled(Radio)`
    background-color: red !important;
    border-radius: 2px !important;
    width: 25px;
    height: 25px;
    
`