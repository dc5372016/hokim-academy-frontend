import React, { useEffect, useState } from "react";
import {
  AuthorContainer,
  AvatarContainer,
  BigContainer,
  Button,
  Button1,
  ButtonWrapper,
  Category,
  Container,
  Container1,
  Content,
  HeartWrapper,
  Image,
  Image1,
  Img,
  Img1,
  LessonCount,
  LoadMoreView,
  MoreButton,
  Name,
  NewContainer,
  PlayBtn,
  ProfileContainer,
  ReusableTitle,
  Text,
  Text1,
  TextContainer,
  Title,
  VideoContainer,
  VidosWrap,
  Wrapper,
  Yozuvlar,
} from "./style";
import { Courses } from "../../../constants/courses";
import YurakIcon from "../../../assets/images/yurak.png";
import Playing from "../../../assets/images/playing.png";
import Video from "../VideoPlayer/VideoPlayer";
import Loading from '../../Info_Courses_Next/Loading/Loading'
import { MyHeart } from "./style";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import 'sweetalert2/src/sweetalert2.scss'
import Swal from 'sweetalert2'


import CircularProgress from './../../RegisterComponent/Circular/Progress'
import { baseURL } from "../../../api/baseUrl";
const AniqFanlar = () => {
  const [Videoopen, setVideoopen] = useState(false);
  const [video_url, setvideo_url] = useState("https://youtu.be/KGMf314LUc0");
  const [isClick, setClick] = useState(false);
  const [data, setData] = useState([]);
  const [BGimg, setBGimg] = useState('');
  const [birinchi, setbirinchi] = useState({id:0});
  const [rasm, setrasm] = useState('');
  const navigate = useNavigate();
  const [loading, setloading] = useState(true);
  const [slicedData, setSlicedData] = useState([]);
  const [sliceValue, setSliceValue] = useState(0);
  const [btnword, setbtnword] = useState('More');
  let token = localStorage.getItem('access_key');

  const Ishla = (item) => {
      if(token===null){
          Swal.fire({
              title: 'Eslatma !',
              icon: 'info',
              text: 'Videolarni ko`rish uchun iltimos ro`yhatdan o`ting',
              imageWidth: 400,
              imageHeight: 200,
              imageAlt: 'Custom image',
              confirmButtonColor: '#3085d6',
              confirmButtonText:'Ro`yhatdan o`tish',
              showCloseButton: true,
              footer: '<a href="/login">Yoki Login orqali kiring</a>'
          })
            .then((result) => {
              /* Read more about handling dismissals below */
              if (result.isConfirmed) {
                navigate('/register')
              }
            })
        }else{
          navigate(`/course/${item.id}`,{state:{...item,word:'Aniq Fanlar'}})
        }
  }
  
  const Ishla1 = (item) => {
      if(token===null){
        Swal.fire({
            title: 'Eslatma !',
            icon: 'info',
            text: 'Videolarni ko`rish uchun iltimos ro`yhatdan o`ting',
            imageWidth: 400,
            imageHeight: 200,
            imageAlt: 'Custom image',
            confirmButtonColor: '#3085d6',
            confirmButtonText:'Ro`yhatdan o`tish',
            showCloseButton: true,
            scrollbarPadding:true,
            footer: '<a href="/login">Yoki Login orqali kiring</a>'
        })
        .then((result) => {
            /* Read more about handling dismissals below */
            if (result.isConfirmed) {
              navigate('/register')
            }
          })
    }else{
      navigate(`/course/${item.id}`,{state:{image:rasm,word:'Aniq Fanlar'}})
    }
  }
  
  const Show = () => {
    setVideoopen(true);
  };

  const LoadMoreData = () => {
    setbtnword('Loading')
    setTimeout(() => {
      setSliceValue(sliceValue+5)
      setbtnword('More')
    }, 1000);
  }

  useEffect(() => {
    setSlicedData(data?.slice(0,sliceValue+10))
    console.log('====================================');
    console.log('sliced data',slicedData);
    console.log('====================================');
  }, [setData,loading,setSliceValue,sliceValue]);


  useEffect(() => {
    async function fetchData() {
      try {
        const res = await axios.get(`${baseURL}/api/courses-with-category/`)
        if(res){
          console.log(res.data.data[0].courses);
          setData([...res.data.data[0].courses,...res.data.data[0].courses])
          setBGimg(`${baseURL}${res.data.data[0].courses[0].image}`)
          setbirinchi({id:res.data.data[0].courses[0].id})
          setrasm(res.data.data[0].courses[0].image);
          setloading(false)
        }else{
          console.log('error');
        }
      } catch (error) {
        console.log(error.message);
      }
    }
    fetchData()
    window.scrollTo({
      top:100,
      behavior: 'auto'
    })
  }, []);



  

  return (
    <Container>
      {loading ===false ? 
      
      
       <Wrapper>
        <ReusableTitle>Aniq fanlar</ReusableTitle>
        <BigContainer>
          <VideoContainer bg={BGimg} show={Videoopen}>
            <Video
              show={Videoopen}
              setshow={setVideoopen}
              video_url={video_url}
            />
            <PlayBtn show={Videoopen} onClick={Show}>
              <Img1 src={Playing} />
            </PlayBtn>
          </VideoContainer>
          <TextContainer>
            <Yozuvlar>
              <Title>
                Matematika misollar <br /> toplami
              </Title>
              <Text>
                Matritsalarni koʻpaytirishning xossalari: Matritsalar
                Koʻchishlar misolida matritsalar: Matritsalar 2x2 oʻlchamli
                matritsa determinanti: Matritsalar Teskari matritsalar haqida
                maʼlumot: Matritsalar Matritsa determinantidan foydalanib uning
                teskarisini topish: Matritsalar 2x2 oʻlchamli matritsalarning
                teskari matritsalarini topishga harakat qilib koʻring.:
                Matritsalar Tenglamalarni yechishda teskari matritsalardan
                foydalanish
              </Text>
            </Yozuvlar>
            <ButtonWrapper>
              <Button  onClick={()=>Ishla1(birinchi)}>Batafsil</Button>
              {/* <Img src={YurakIcon} /> */}
              <HeartWrapper>
                <MyHeart isClick={isClick} onClick={() => setClick(!isClick)} />
              </HeartWrapper>
            </ButtonWrapper>
          </TextContainer>
        </BigContainer>
        <VidosWrap>
          {slicedData?.map((item, index) => {
            // data-aos="zoom-in-up" data-aos-duration="2000"
            return (
              <Container1
                // data-aos="flip-left"
                data-aos-duration="500"
                data-aos-delay={index * 150}
                data-aos-offset="100"
                data-aos-easing="ease-in-sine"
                key={index}
              >
                <Image src={baseURL+item.image} />
                <LessonCount>{item.lesson_count}ta dars</LessonCount>
                <Content>
                  <AuthorContainer>
                    <Name>{item.author.name}</Name>
                    <AvatarContainer>
                      <Image1
                        src={baseURL+item.author.image}
                        style={{ borderRadius: "25px" }}
                      />
                    </AvatarContainer>
                  </AuthorContainer>
                  <Category>{item.name}</Category>
                  <Text1>
                    {item.views} ko'rildi - {item.day} kun oldin
                  </Text1>
                  <Button1 onClick={()=>Ishla(item)}>Boshlash</Button1>
                </Content>
              </Container1>
            );
          })}
        </VidosWrap>
        <LoadMoreView>
            <MoreButton onClick={LoadMoreData}>
              {btnword === 'More' ? btnword : <CircularProgress />}
            </MoreButton>
        </LoadMoreView>
      </Wrapper>
      
       : <div style={{marginTop:200}}>
           <Loading />
        </div>}
    </Container>
  );
};

export default AniqFanlar;
