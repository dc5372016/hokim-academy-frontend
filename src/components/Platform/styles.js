import styled from 'styled-components';




export const Title = styled.span`
    font-family: 'Lexend';
    font-style: normal;
    font-weight: 500;
    font-size: 36px;
    line-height: 45px;
    color: #333333;
`;

export const LeftContainer = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
`;

export const RightContainer = styled.div`
    display: flex;
    flex-direction: column;
`;

export const CardContainer = styled.div`
    display: grid;
    grid-template-columns: 50% 50%;
    grid-gap: 30px;
    margin-top: 35px;
    
`;

export const Card = styled.div`
    display: flex;
    flex-direction: column;
    grid-gap: 15px;
`;

export const Icon = styled.img``;

export const IconContainer = styled.div`
display: flex;
justify-content: center;
align-items: center;
    width: 77px;
    height: 77px;
    background: #FFFFFF;
    box-shadow: 0px 4px 10px rgba(93, 95, 239, 0.3);
    border-radius: 38px;
`;

export const CardTitle = styled.span`
   font-family: 'Lexend';
    font-style: normal;
    font-weight: 500;
    font-size: 26px;
    line-height: 32px;
    color: #333333; 
`;

export const CardText = styled.span`
    font-family: 'Lexend';
    font-style: normal;
    font-weight: 300;
    font-size: 20px;
    line-height: 25px;

    color: #333333;
`;

export const Image = styled.img`
    z-index: 2;
`;

export const Container = styled.div`
    display: grid;
    grid-template-columns: auto auto;
    max-width: 1920px;
    margin: auto;
    @media (max-width:1025px){
        grid-template-columns: auto;
        ${Title}{
        text-align: center;
       }
       ${Image}{
        max-width: 600px;
       }
       ${LeftContainer}{
           align-self: flex-end;
       }
    }
    @media (max-width:674px){
        ${CardContainer}{
            grid-template-columns: 100%;
        }
    }
    padding: 100px;
    @media (max-width:1400px){
        padding: 50px;
       ${Image}{
           width: 100%;
       }
       ${CardTitle}{
        font-size: 22px;
        line-height: 25px;
       }
       ${CardText}{
        font-size: 17px;
        line-height: 20px; 
       }
       ${Title}{
        font-size: 32px;
        line-height: 38px;
       }
       ${CardContainer}{
           grid-gap: 20px;
       }
    }
`;